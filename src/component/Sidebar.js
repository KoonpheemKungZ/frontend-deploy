import React from 'react';
import { Link } from 'react-router-dom';

class Sidebar extends React.Component {

    render() {
        return (
        <nav className="col-md-2 d-none d-md-block bg-light sidebar">
            <div className="sidebar-sticky">
                <ul className="nav flex-column mb-2">
                    <li className="nav-item">
                        <Link className="nav-link active" to="/home">All News<span className="sr-only">(current)</span></Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">Publish</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" to="/home">Unpublish</Link>
                    </li>
                </ul>
            </div>
        </nav>
        )
    }
}

export default Home;