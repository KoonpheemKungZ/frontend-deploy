import React from 'react';
import { Link } from 'react-router-dom';
import './Home.css';
import axios from 'axios'
import { Pagination } from 'react-bootstrap'
import { API_URL } from '../utils/constants'
import iconcreatenews from '../images/icon-create-news.svg';
import highlight from '../images/ic_pin_article_active.svg';
import unHighlight from '../images/ic_pin_article_inactive.svg';
import { GroupService } from '../services/GroupService';

class Home extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            datasource: [],
            current_page: 1,
            per_page: 20,
            start_item_index: 0,
            max_page: 0,
            all_items: 0,
            status: "",
            status_id: "",
            categories: []
        };
    }

    componentDidMount() {
        let status = this.props.match.params.status;
        this.getNewsCategories()
        var state_id = "";
        if (this.state.status !== status) {
            this.setState({ current_page: 1 });
            this.setState({ start_item_index: 0 });
        }
        if (status === "All") {
            status = "";
        } else if (status === "Publish") {
            status = "Publish";
            state_id = "5d453e2a1e79ab8688d1bd1b";
        } else if (status === "Unpublish") {
            status = "Unpublish";
            state_id = "5d453e2f1e79ab8688d1bd1c";
        } else if (status === "Hidden") {
            status = "Hidden";
            state_id = "5d453e351e79ab8688d1bd1d";
        }
        this.setState({ status: status })
        this.setState({ status_id: state_id }, () => this.loadNews());
    }

    componentWillReceiveProps(nextProps) {
        let status = nextProps.match.params.status;
        var state_id = "";
        if (this.state.status !== status) {
            this.setState({ current_page: 1 });
            this.setState({ start_item_index: 0 });
        }
        if (status === "All") {
            status = "";
        } else if (status === "Publish") {
            status = "Publish";
            state_id = "5d453e2a1e79ab8688d1bd1b";
        } else if (status === "Unpublish") {
            status = "Unpublish";
            state_id = "5d453e2f1e79ab8688d1bd1c";
        } else if (status === "Hidden") {
            status = "Hidden";
            state_id = "5d453e351e79ab8688d1bd1d";
        }
        this.setState({ status: status })
        this.setState({ status_id: state_id }, () => this.loadNews());
    }

    async loadNews() {

        var params = {
            "work": "CMS_GET_NEWS",
            "item_amount": this.state.start_item_index,
            "item_per_request": this.state.per_page,
            "status": this.state.status_id,
            "reference_fields": ["file", "description", "title", "image"]
        }

        await axios({
            method: "post",
            url: API_URL,
            data: params
        })
            .then(response => {
                if (response.data.status === true) {

                    this.setState({ all_items: response.data.more_fields.total_amount_of_news });
                    this.setState({ datasource: response.data.result }, () => { this.calPageination() })
                    this.calPageination()
                }
                console.log('before waitingggg')

            }).catch(error => {
                console.log(error);
            });
    }

    calPageination() {
        const { per_page, all_items } = this.state
        var checkMax = parseInt((all_items / per_page) + (all_items % per_page > 0 ? 1 : 0));
        this.setState({ max_page: checkMax });
    }

    calDate(time) {
        var date = new Date(time * 1000);
        return (
            date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
        );
    }

    displayStatus(status) {
        if (status === "Publish") {
            return <td className="text-success" style={{ textAlign: "right" }}>Publish</td>
        } else {
            return <td className="text-muted" style={{ textAlign: "right", color: "#494E52" }}>Unpublish</td>
        }
    }

    onClickFirstPage = () => {
        this.setState({ current_page: 1 })
        this.setState({ start_item_index: 0 }, () => this.loadNews())
    }

    onClickPrePage = () => {
        this.setState({ current_page: this.state.current_page - 1 }, () =>
            this.setState({ start_item_index: parseInt(((this.state.current_page - 1) * this.state.per_page)) }, () => this.loadNews()));
    }

    onClickNextPage = () => {
        this.setState({ current_page: this.state.current_page + 1 }, () =>
            this.setState({ start_item_index: parseInt(((this.state.current_page - 1) * this.state.per_page)) }, () => this.loadNews()));
    }

    onClickLastPage = () => {
        this.setState({ current_page: this.state.max_page })
        this.setState({ start_item_index: parseInt(((this.state.max_page - 1) * this.state.per_page)) }, () => this.loadNews())
    }

    addImagePlaceholderSrc(e) {
        e.target.src = 'https://via.placeholder.com/60x50/494E52'
    }

    getImageCoverURL = (item) => {
        if (item.length > 0) {
            return (item === undefined ? 'https://via.placeholder.com/60x50/494E52' : item);
        } else {
            return 'https://via.placeholder.com/60x50/494E52';
        }

    }

    getNewsCategories = () => {
        var params = {
            "work": "GET_NEWS_CATEGORIES"
        }
        axios({
            method: "post",
            url: API_URL,
            data: params
        }).then(response => {
            if (response.data.status === true) {
                this.setState({ categories: response.data.result })
            }

        }).catch(error => {
            console.log(error);
        });
    }

    filterCategory = async (event) => {
        event.persist();
        await this.loadNews();
        if (event.target.value === "") return
        const arrayCopy = [...this.state.datasource];
        const arrayFilter = await arrayCopy.filter(news => news.category._id === event.target.value)
        this.setState({ datasource: arrayFilter });
    }

    toggleHighlight = async (newID, highlight) => {
        console.log('click toggle hightlishgt')
        const param = {
            work: 'HIGHLIGHT_NEWS',
            news_id: newID,
            highlight_status: highlight ? false : true
        }
        await GroupService.highlight(param)
        await this.loadNews();
    }

    render() {
        return (
            <div class="row justify-content-md-center">
                <div role="main" className="col-md-9 ml-sm-10 col-lg-10">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <div>
                        <h4 className="h4">{(this.state.status === undefined || this.state.status === null || this.state.status === "") ? "ThaiBev Members" : this.state.status}</h4>
                        <p>App Permission Setting</p>
                        </div>
                        
                        <div className="btn-toolbar mb-2 mb-md-0">
                            <div className="btn-group mr-2">
                                <Link to="/createnews"><button type="button" className="btn btn-success"><img className="mb-1" src={iconcreatenews} alt="" /> Create News</button></Link>

                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header bg-white">
                            <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                                <div className="btn-toolbar mb-2 mb-md-0 d-flex align-items-center">
                                    <p className="mr-2 mt-3">{(this.state.status === undefined || this.state.status === null || this.state.status === "") ? "All" : this.state.status}</p>
                                    <div className="btn-group">
                                        <select className="form-control" id="exampleFormControlSelect1" onChange={this.filterCategory}>
                                            <option value="">All Categories</option>
                                            {
                                                this.state.categories.map(category => {
                                                    return <option value={category._id} key={category._id}>{category.name}</option>
                                                })
                                            }
                                        </select>
                                    </div>
                                </div>
                                <div hidden>
                                    <form className="form-inline">
                                        <div className="form-group mx-sm-3 mb-2">
                                            <label htmlFor="inputPassword2" className="sr-only">Search</label>
                                            <input type="text" className="form-control" id="inputPassword2" placeholder="Search" />
                                        </div>
                                        <button type="submit" className="btn btn-primary mb-2">Search</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="card-body">
                            <table className="table">
                                <thead>
                                    <tr>
                                        <th style={{ borderTop: '0' }}></th>
                                        <th style={{ borderTop: '0' }}></th>
                                        <th style={{ borderTop: '0', whiteSpace: "nowrap" }}>Category</th>
                                        <th style={{ borderTop: '0', whiteSpace: "nowrap" }}>Create Date</th>
                                        {/* <th style={{ borderTop: '0', whiteSpace: "nowrap" }}>Publish Date</th> */}
                                        <th style={{ borderTop: '0', whiteSpace: "nowrap" }}></th>
                                        <th style={{ borderTop: '0', whiteSpace: "nowrap", width: "1px" }}>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.datasource.map((item, index) => (
                                        <tr key={index}>
                                            <td style={{ width: "16px", whiteSpace: "nowrap" }}><img src={this.getImageCoverURL(item.contents.file)} onError={this.addImagePlaceholderSrc} alt="" width="60" height="50" /></td>
                                            <td><Link to={"/editnews/" + item.news_id}>{item.contents.title}</Link></td>
                                            <td style={{ textAlign: "right", width: "32px", whiteSpace: "pre-wrap" }}>{item.category.name}</td>
                                            <td style={{ textAlign: "right", width: "32px", whiteSpace: "pre-wrap" }}>{this.calDate(item.created_timestamp)}</td>
                                            {/* <td style={{ textAlign: "right", width: "32px", whiteSpace: "pre-wrap" }}>{this.calDate(item.created_timestamp)}</td> */}
                                            <td style={{ whiteSpace: "pre" }}><span style={{ cursor: 'pointer' }} onClick={() => this.toggleHighlight(item.news_id, item.highlight)}>{item.highlight ? <img src={highlight} alt=''/> : <img src={unHighlight} alt=''/>} Highlight</span></td>
                                            {this.displayStatus(item.status.desc)}
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                        <Pagination style={{ marginLeft: 16 }}>
                            <Pagination.First onClick={this.onClickFirstPage} disabled={this.state.current_page <= 1 ? true : false} />
                            <Pagination.Prev onClick={this.onClickPrePage} disabled={this.state.current_page <= 1 ? true : false} />
                            <Pagination.Ellipsis disabled />
                            <Pagination.Item disabled>{this.state.current_page}</Pagination.Item>
                            <Pagination.Ellipsis disabled />
                            <Pagination.Next onClick={this.onClickNextPage} disabled={this.state.current_page >= this.state.max_page ? true : false} />
                            <Pagination.Last onClick={this.onClickLastPage} disabled={this.state.current_page >= this.state.max_page ? true : false} />
                        </Pagination>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;

