import React from 'react';
import '../style/login.css';
import logo from '../images/logo-thaibev.svg';
import iconuser from '../images/icon-user.svg';
import iconpassword from '../images/icon-password.svg';
import auth from '../firebase'
import firebase from 'firebase'
class Login extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      message: '',
      currentUser: null,
      loading:true
    }
  }

  componentWillUnmount() {
    this.setState({loading:false})
 }

  componentDidMount() {
    //this.setState({loading:true})
    auth.onAuthStateChanged(user => {
      if (user) {
        this.setState({
          currentUser: user
        }, ()=> {
          localStorage.setItem("uid",user.uid);
          this.setAuthen();
          this.setState({loading:false})
        });
        
      }else{
        this.setState({loading:false})
      }
    })
  }

  setAuthen=()=>{
    this.props.userHasAuthenticated(true);
    //console.log(this.props.history.location.pathname)
    if (this.props.history.location.pathname === "/login"){
      this.props.history.push("/groups");
    }else{
      this.props.history.push(this.props.history.location.pathname);
    }
    //this.props.history.push(this.props.history)
    
  }
  onChange = e => {
    const { name, value } = e.target

    this.setState({
      [name]: value
    })
  }

  onSubmit = e => {
    e.preventDefault()
    //console.log(this.props.isAuthenticated)
    const { email, password } = this.state
    auth.setPersistence(firebase.auth.Auth.Persistence.SESSION)
      .then(function () {
        // Existing and future Auth states are now persisted in the current
        // session only. Closing the window would clear any existing state even
        // if a user forgets to sign out.
        // ...
        // New sign-in will be persisted with session persistence.
        return auth.signInWithEmailAndPassword(email, password);
      }).catch(function (error) {
        // Handle Errors here.
        // var errorCode = error.code;
        // var errorMessage = error.message;
      })
      // auth.signInWithEmailAndPassword(email, password)
      // .then(response => {
      //   this.setState({
      //     currentUser: response.user
      //   })
      //   this.props.userHasAuthenticated(true);
      //   this.props.history.push("/home");
      // })
      // .catch(error => {
      //   console.log(error.message)
      //   this.setState({
      //     message: error.message
      //   })
      // })
  }

  render() {
    return (
      <div className="container" hidden={this.state.loading}>
        <div className="row align-items-center">
          <div className="col">
          </div>
          <div className="col">
            <div className="card card-signin">
				<div className="card-header text-center pt-4">
					<img className="card-logo mb-2" src={logo} alt="" /> 
					<p className="mb-1">Admin Tool</p>
				</div>
				<div className="card-body ">
				<h5 className="card-title mb-3">Login</h5>
                <form className="form-signin">
                  <div className="form-label-group mb-4">
                    <div className="input-group mb-2">
                      <div className="input-group-prepend">
                        <div className="input-group-text bg-white"><img className="" src={iconuser} alt="" /></div>
                      </div>
                      <input type="email" name="email" className="form-control border-left-0" id="email" placeholder="Username" onChange={this.onChange} />

                    </div>
                  </div>
                  <div className="form-label-group mb-4">
                    <div className="input-group mb-2">
                      <div className="input-group-prepend">
                        <div className="input-group-text bg-white"><img className="" src={iconpassword} alt="" /></div>
                      </div>
                      <input type="password" name="password" className="form-control border-left-0" id="password" placeholder="Password" onChange={this.onChange} />

                    </div>
                  </div>
                  <button className="btn btn-success btn-block text-uppercase" type="submit" onClick={this.onSubmit}>log in</button>
                </form>
              </div>
            </div>
          </div>
          <div className="col">
          </div>
        </div>
        <div className="row align-items-center mt-2">
          <div className="col">
            <p className="text-center">CMS Power by Think</p>
          </div>
        </div>
      </div>
    )
  }
}

export default Login;