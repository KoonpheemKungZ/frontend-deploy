import Loader from 'react-loader-spinner';

import { usePromiseTracker } from "react-promise-tracker";

const LoadingIndicator = props => {
    const { promiseInProgress } = usePromiseTracker();

    return promiseInProgress &&
        (
            <>
                {/* <i class="fa fa-home"></i> */}
                <div className="float-right ml-2"
                //     style={{
                //         width: "100%",
                //         height: "100",
                //         display: "flex",
                //         justifyContent: "center",
                //         alignItems: "center"
                //     }}
                >
                    <Loader type="ThreeDots" color="#fff" width="24" height="24" />
                </div>
            </>

        )

};
export default LoadingIndicator