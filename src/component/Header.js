import React from 'react';
import { withRouter } from 'react-router-dom';
import { Navbar } from 'react-bootstrap';
import { Nav } from 'react-bootstrap';
import '../style/header.css';
import logo from '../images/ic_admin_tools_44px.png';
import iconavatar from '../images/icon-avatar.svg';
import icondragindicator from '../images/format_list_bulleted-24px.svg';
import { history } from '../store/history';

class Header extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            create_new: false
        };
    }

    onSaveBtn = () => {

    }

    render() {
        var isShowSave = false;
        if (this.props.location.pathname === "/createnews" || this.props.location.pathname.includes("/editnews")) {
            isShowSave = true;
        } else {
            isShowSave = false;
        }

        return (
            <Navbar className="shadow-sm" bg="white" fixed="top" variant="white">
                <div className="d-flex align-items-center">
                    <div className="flex-shrink-0">
                        <img alt="" className="align-self-center mr-1 text-green" width="32" height="32" aria-hidden="true" src={logo} />
                    </div>
                    <div className="flex-grow-1 ml-2">
                        <h5 className="mb-0 fw-bolder text-dark">Admin Tool</h5>
                        <p className="mb-0 text-dark">Web Tool</p>
                    </div>
                </div>
                <Nav className="mr-auto">

                </Nav>

                <Nav>
                    <button type="button" className="discard-btn" style={{ marginRight: 10 }} hidden={!isShowSave}>Discard</button>
                    <button className="save-btn" hidden={!isShowSave} form='my-form' type="submit">Save</button>
                    <Nav.Link className="text-dark" onClick={() => history.push({
                        pathname: '/manage-app',
                        state: {
                            apps: this.state.default_apps
                        }
                    })} hidden={isShowSave}><img className="mr-1 mb-1" src={icondragindicator} alt="" />Manage App</Nav.Link>
                    <Nav.Link href="#deets" className="text-dark" hidden={isShowSave}><img className="mr-1 mb-1" src={iconavatar} alt="" />Admin</Nav.Link>
                </Nav>
            </Navbar>
        )
    }
}

export default withRouter(Header);