import React from 'react';
import ic_apps from '../images/icons8-squared-menu-24.png'


class Footer extends React.Component {

    render() {
        return (
            <footer className="footer py-3 border-top">
                <div className="container-fluid d-flex flex-wrap justify-content-between align-items-center">

                    <div className="col-md-4 mb-0 d-flex align-items-center">
                        <a href={process.env.REACT_APP_AUTH_URL} className="d-flex align-items-center text-dark" style={{ fontSize: "14px" }} >
                            <img
                                src={ic_apps}
                                width={18}
                                height={18}
                                alt=""
                                className='mr-2'
                            />
                            <span className='flex-grow-1 text-left mb-0 font-weight-bold'>All Service</span>
                        </a>
                    </div>
                    <p className="text-muted col-md-4 d-flex align-items-center justify-content-center mb-3 mb-md-0 me-md-auto ">
                        © สงวนลิขสิทธิ์ 2564 บริษัท ไทยเบฟเวอเรจ จำกัด (มหาชน)
                    </p>
                    <div className="col-md-4"></div>
                </div>
            </footer>
        )
    }
}

export default Footer;