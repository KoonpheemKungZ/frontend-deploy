import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { Button, ListGroup, Media, Form, InputGroup } from 'react-bootstrap'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import { GroupService } from '../../services/GroupService'

export default class SearchMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search_member: '',
            persons: []
        };
    }
    componentDidMount() {
        this.getMember()
        this.search_member.focus();
    }
    getMember() {
        let value = this.props.match.params.value;
        this.setState({ search_member: value });
        const param = {
            work: "ADMIN_DYNAMIC_SEARCH_USER",
            user_id: "5cff95c2ab4eee1afc097367",
            language: "en",
            without_group: {
                full_name: value,
                item_amount: 0,
                item_per_request: 100
            }
        }
        GroupService.requestAPI(param).then(res => {
            console.log(res)
            if (res.status === 200) {
                this.setState({ persons: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })

    }
    serchChangeHandler = (event) => {
        const search_member = event.target.value
        // console.log(event.target.value)
        const param = {
            work: "ADMIN_DYNAMIC_SEARCH_USER",
            user_id: "5cff95c2ab4eee1afc097367",
            language: "en",
            without_group: {
                full_name: search_member,
                item_amount: 0,
                item_per_request: 100
            }
        }
        GroupService.requestAPI(param).then(res => {
            if (res.status === 200) {
                if(res.data.status === true) {
                    this.setState({ persons: res.data.result })
                }else{
                    this.setState({ persons: [] })
                }
                
            }
        }).catch(err => {
            console.log(err)
        })
        this.setState({ search_member: event.target.value });
    }

    renderPerson() {
        const { persons } = this.state
        var arr = []
        for (let index = 0; index < persons.length; index++) {
            const element = persons[index];
            arr.push(
                <ListGroup.Item className="d-flex justify-content-between align-items-center">
                    <Media>
                        <img
                            width={40}
                            height={40}
                            className="mr-2 rounded-circle"
                            src={element.user.profile_url}
                            alt=""
                            onError={(e) => { e.target.onerror = null; e.target.src = "https://yt3.ggpht.com/a/AGF-l7_KcMkAm641_17l3LYRN9jbx8kA6SyZONwang=s48-c-k-c0xffffffff-no-rj-mo" }}
                        />
                        <Media.Body>
                            <h6 className="mb-0">{element.user.first_name} {element.user.last_name}  ,{element.user.ref_id}</h6>
                            <Icon className="fa fa-users float-left" style={{ "fontSize": "1rem", color: "#888888", "width": "30px" }} />
                            <p className="mb-0">Group : {element.drawer.title}</p>
                        </Media.Body>
                    </Media>
                    <Button variant="outline-secondary" onClick={() => this.props.history.push({ pathname : '/managegroup/' + element.user._id, state : {
                        user : element.user
                    }})}>Manage</Button>
                </ListGroup.Item>
            )
        }
        return arr
    }

    render() {
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Link to="/groups"><p className="text-muted" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">Search Members</h4>

                        </div>
                        <div>
                            {/* <Button variant="outline-secondary">Add Manual</Button> */}
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-body" >
                            <Row>
                                <Col sm={12}>
                                    <Form>
                                        <Form.Group as={Col} md="12" className="p-0" controlId="validationCustomUsername">
                                            {/* <Form.Label>Username</Form.Label> */}
                                            <InputGroup>
                                                <InputGroup.Prepend>
                                                    <InputGroup.Text id="inputGroupPrepend"><Icon>search</Icon></InputGroup.Text>
                                                </InputGroup.Prepend>
                                                <input type="text" ref={(input) => { this.search_member = input; }} className="form-control" id="inlineFormInputGroup" placeholder="Search Members" defaultValue={this.state.search_member} onChange={this.serchChangeHandler} />
                                                {/* <Form.Control.Feedback type="invalid">
                                    Please choose a username.
            </Form.Control.Feedback> */}
                                            </InputGroup>
                                        </Form.Group>
                                    </Form>
                                </Col>
                            </Row>
                            <Row>
                                <Col sm={12}>
                                    <ListGroup>
                                        {this.renderPerson()}
                                    </ListGroup>
                                </Col>
                            </Row>
                        </div>
                    </div>
                    {/* <div class="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                       <button type="button" className="btn btn-success"> Confirm</button> 
                    </div> */}
                </div>
            </div>
        )
    }
}

