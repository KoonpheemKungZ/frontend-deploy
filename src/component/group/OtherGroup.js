/* eslint-disable array-callback-return */
import React from 'react';
import '../Home.css';
import { Link } from 'react-router-dom'
import { Card, Media, Form } from 'react-bootstrap'
import iconcreatenews from '../../images/icon-create-news.svg';
import Icon from '@material-ui/core/Icon';
import { Cookies } from "react-cookie"
import './group.css';
import { GroupService, authSign } from '../../services/GroupService';

import { history } from '../../store/history';
import Swal from 'sweetalert2';

import defaultimage from '../../images/default-image.jpg';
// import { CardFooter } from 'react-bootstrap/Card';
// import { NewsService } from '../services/NewsService';

class OtherGroup extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            default_apps: [],
            datasource: [],
            current_page: 1,
            per_page: 20,
            start_item_index: 0,
            max_page: 0,
            all_items: 0,
            status: "",
            status_id: "",
            categories: []
        };
    }

    componentDidMount() {
        this.requestGroups()
    }

    requestGroups() {
        const param = {
            work: 'drawer-group:CREATE-FORM.GET',
            // user_id: '5cff95c2ab4eee1afc097367'
        }
        authSign().then(res => {
            console.log(res)
            if (res.token !== undefined) {
                const cookies = new Cookies()
                cookies.set('token', res.token)
                cookies.set('tmp', res.tmp)

                GroupService.getGroups(param).then(res => {
                    console.log(res)
                    if (res.status === 200) {
                        this.setState({ datasource: res.data.result.groups })
                        res.data.result.groups.map(data => {
                            if (data.default === true) {
                                this.setState({ default_apps: data.apps })
                                sessionStorage.setItem('default_apps', JSON.stringify(data.apps))

                            }
                        })
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
        }).catch(err => {

        })



    }

    deleteGroup = (id) => {
        Swal.fire({
            title: 'ยืนยันการลบ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                const param = {
                    work: 'ADMIN_DELETE_DRAWER_GROUP',
                    user_id: '5cff95c2ab4eee1afc097367',
                    drawer_group_id: id
                }
                GroupService.requestAPI(param).then(res => {
                    if (res.status === 200) {
                        Swal.fire(
                            '',
                            'ลบกลุ่มสำเร็จ',
                            'success'
                        )
                        this.requestGroups()
                    }
                }).catch(err => {
                    console.log(err)
                })

            }
        })

    }
    serchChangeHandler = (event) => {
        const search_member = event.target.value
        // console.log(event.target.value)
        // this.setState({ search_member: event.target.value });
        this.props.history.push('/searchmember/' + search_member)
    }
    onError = () => {
        if (!this.state.errored) {
            this.setState({
                src: this.props.fallbackSrc,
                errored: true,
            });
        }
    }

    renderApps(apps, index) {
        var arr = []
        apps.map((app, j) => {
            if (j < 7) {
                arr.push(
                    <div className="col-md-3" id="p1" style={{ marginBlock: 5 }} key={(index * 100) + j}>
                        {/* <Image src={app.icon.active} fluid/> */}
                        <Card style={{ border: 'none' }} className='position-relative rounded-circle'>
                            <Card.Img
                                variant="top"
                                src={app.icon.active}
                                fluid='true'
                                onError={(e) =>
                                    e.target.src = defaultimage
                                }
                                className='rounded-circle'
                            />

                        </Card>
                        <br />
                    </div>
                )
            } else if (j === 12) {
                arr.push(
                    <div className="col-md-3" id="p1" style={{ marginBlock: 5 }} key={(index * 100) + j}>
                        {/* <Image src={app.icon.active} fluid/> */}
                        <Card style={{ border: 'none' }} className='position-relative rounded-circle bg-dark'>

                            <Card.Img
                                variant="top"
                                src={app.icon.active}
                                fluid='true'
                                style={{ 'opacity': '50%' }}
                                onError={(e) =>
                                    e.target.src = defaultimage
                                }
                                className='rounded-circle'
                                width={'52'}
                                height={'52'}
                            />
                            <div className='position-absolute  text-white text-center' style={{ "height": "52px", "width": "52px", paddingTop: "8px" }}>
                                <span className='align-middle'>More</span>
                            </div>
                            {/* <span className='position-absolute text-white' style={{
                                'top': '30%',
                                'left': '20%'
                            }}>More</span> */}
                        </Card>
                        <br />
                    </div>
                )
            }

        })
        return arr
    }

    renderGroups() {
        var arr = []
        const { datasource } = this.state
        datasource.map((data, index) => {
            arr.push(
                <div className="col-md-3" key={index}>
                    <Card className='shadow-sm'>
                        <Card.Header >
                            <div className="float-left">{data.title}</div>
                            {/* <div className="float-right"><img className="mb-1" src={iconpassword} alt="" /></div> */}
                            <div className="float-right" style={{ cursor: 'pointer' }} hidden={data.member.amount !== 0}><Icon style={{ height: 20 }} className="fa fa-trash" fluid='true' onClick={this.deleteGroup.bind(this, data._id)} /></div>
                            {/* <button className="float-right btn"><Icon className="fa fa-trash" /></button> */}
                        </Card.Header>
                        <Card.Body style={{ cursor: 'pointer', height: 225 }} className="bg-white clearfix py-0" onClick={() => history.push({
                            pathname: '/groupdetail/' + data._id,
                            state: {
                                group: data
                            }
                        })}>
                            <Card.Text>App Access</Card.Text>
                            <div className="row">
                                {this.renderApps(data.apps, index)}
                            </div>
                        </Card.Body>
                        <Card.Footer className="bg-white border-0">
                            <Media style={{ cursor: 'pointer' }} onClick={() => this.props.history.push('/addmember/' + data._id)}>
                                <Icon className="fa fa-users mr-1" style={{ "fontSize": "1.25rem", color: "#888888", "width": "30px" }} />
                                <Media.Body>
                                    <p>{data.member.amount} Members</p>
                                </Media.Body>
                            </Media>

                        </Card.Footer>
                    </Card>
                    <br />
                </div>
            )
        })
        return arr
    }

    render() {
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-9 ml-sm-10 col-lg-10">
                    <div className="mr-auto mt-3 p-0 bd-highlight">
                        <Link to="/"><p className="text-muted mb-1" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <div>
                            <h4 className="h4">{(this.state.status === undefined || this.state.status === null || this.state.status === "") ? "ThaiBev Affiliates" : this.state.status}</h4>
                            <p>ระบบบริหารจัดการการเข้าถึง Application  Beverest Life ของบริษัทในเครือไทยเบฟเวอเรจ</p>
                        </div>

                        <div className="btn-toolbar mb-2 mb-md-0">
                            <Form inline>
                                <div className="btn-group mr-2">
                                    <div className="input-group mr-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-white"><Icon>search</Icon></div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Search Members" onChange={this.serchChangeHandler} />
                                    </div>
                                </div>
                                <button type="button" className="btn btn-primary" onClick={() => history.push({
                                    pathname: '/create-group',
                                    state: {
                                        apps: this.state.default_apps
                                    }
                                })}><img className="mb-1" src={iconcreatenews} alt="" /> Create Group</button>
                            </Form>
                        </div>

                    </div>
                    <div className="row">
                        {this.renderGroups()}
                    </div>
                </div>
            </div>
        )
    }
}

export default OtherGroup;

