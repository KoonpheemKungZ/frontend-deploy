import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { Card, Accordion, Media, Button } from 'react-bootstrap'
import Row from 'react-bootstrap/Row'
import { GroupService } from '../../services/GroupService'
// import Msucces from './MoveEmployeeSucces'
import Swal from 'sweetalert2'
// import DoneIcon from '@material-ui/icons/Done';
// import Icon from '@material-ui/core/Icon'

export default class ManageGroup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search_member: 'a',
            groups: [],
            apps: [],
            mygroup: {},
            active_key: '',
        };
    }
    componentDidMount() {
        this.getMember()
        //this.search_member.focus();
    }

    getMember() {
        const { id } = this.props.match.params

        console.log(this.props)
        //this.setState({ search_member: value });
        const param = {
            "work": "GET_DRAWER_APP_MENU_BY_USER_ID",
            "user_id": id,
            "group_detail": true,
            "app_version_number": 1000
        }
        GroupService.requestAPI(param).then(res => {
            console.log(res)
            if (res.status === 200) {
                this.setState({ apps: res.data.result.menu })
                this.setState({ mygroup: res.data.result.drawer })
            }
        }).catch(err => {
            console.log(err)
        })

        const params = {
            "work": "ADMIN_GET_DRAWER_GROUPS",
            "user_id": id
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                this.setState({ groups: res.data.result.groups })
            }
        }).catch(err => {
            console.log(err)
        })
    }
    serchChangeHandler = (event) => {
        //const search_member = event.target.value
        // console.log(event.target.value)
        this.setState({ search_member: event.target.value });
        // IsDone: false
    }

    requestChangeGroup(group_id) {
        const { id } = this.props.match.params
        const params = {
            "work": "ADMIN_UPDATE_USER_DRAWER_GROUP",
            "user_id": id,
            "drawer_group_id": group_id
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                this.getMember()
                Swal.fire({
                    position: 'top',
                    icon: 'success',
                    title: 'การย้ายกลุ่มสมาชิกสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                  })
            }
        }).catch(err => {
            console.log(err)
        })
    }

    onSelectedGroup = (group_id) => {
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันการย้ายกลุ่มสมาชิก?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                this.requestChangeGroup(group_id)
            }
        })
    }

    renderGroup() {
        const { groups, mygroup } = this.state
        var arr = []
        // eslint-disable-next-line array-callback-return
        groups.map((item, index) => {
            arr.push(
                <Card key={index}>
                    <Accordion.Toggle className="bg-white" as={Card.Header} eventKey={item._id}>
                        <div className="d-flex justify-content-between">
                            <Media>
                                <Media.Body>
                                    <h6 className="h6 mb-0">Group Name : {item.title}</h6>
                                    <p className="p mb-0" style={{ "fontWeight": "400", fontSize: ".875rem" }}>{item.apps.lenght} App Access</p>
                                </Media.Body>
                            </Media>
                            <Button variant="outline-primary" hidden={item._id === mygroup._id} onClick={this.onSelectedGroup.bind(this, item._id)}>Select</Button>
                        </div>
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey={item._id}>
                        <Card.Body>
                            <Row>
                                {item.apps.map((app, index) => {
                                    return (
                                        <div className="col-md-1 p-1" id="p1" style={{ marginBlock: 10 }} key={`${app.id}${index * 10}`}>
                                            <Card style={{ border: 'none' }}>
                                                <Card.Img variant="top" src={app.icon.active} fluid='true' />
                                            </Card>
                                        </div>
                                    )
                                }
                                )}
                            </Row>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            )
        }
        )
        return arr
    }

    render() {
        const { active_key, mygroup } = this.state
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Link to="/groups"><p className="text-muted" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">Manage Groups</h4>

                        </div>
                        <div>
                            {/* <Button variant="outline-secondary">Add Manual</Button> */}
                        </div>
                    </div>
                    <Card className="border-0 mb-4">
                        <Card.Header className="bg-white py-4 px-5">
                            <Media>
                                <img
                                    width={45}
                                    height={45}
                                    className="mr-2 rounded-circle"
                                    alt=''
                                    src="https://yt3.ggpht.com/a/AGF-l7_KcMkAm641_17l3LYRN9jbx8kA6SyZONwang=s48-c-k-c0xffffffff-no-rj-mo"
                                />
                                <Media.Body>
                                    <h6 className="mb-2">{this.props.location.state ? this.props.location.state.user.first_name +' ' + this.props.location.state.user.last_name  : ''}</h6>
                                    <p className="mb-0 d-flex align-items-start"><Icon className="fa fa-users float-left" style={{ "fontSize": "1.125rem", color: "#888888", "width": "30px" }} />Current Group : {mygroup.title}</p>
                                </Media.Body>
                            </Media>
                        </Card.Header>
                        <Card.Body className="py-4 px-5">
                            <Card.Title className="h4">Move users to groups</Card.Title>
                            <Accordion defaultActiveKey={active_key}>
                                {this.renderGroup()}
                            </Accordion>
                        </Card.Body>
                    </Card>
                    {/*  modal */}
                    {/* <Msucces /> */}
                    {/*  modal */}

                </div>
            </div>
        )
    }
}

