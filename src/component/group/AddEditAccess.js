import React, { Component } from 'react'
import { Modal, Image, Button } from 'react-bootstrap'
import ListGroup from 'react-bootstrap/ListGroup'
import Form from 'react-bootstrap/Form'
import Swal from 'sweetalert2'
import { GroupService } from '../../services/GroupService';

export default class AddEditAccess extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ModalAccess: false,
            apps_all:[]
        }
    }

    componentDidMount(){
        // console.log('GET_APPS')
        let params = {
            "work": "GET_APPS",
            "app_version_number": 1000
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                this.setState({ apps_all: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
    }

    ModalClose = () => {
        this.setState({ ModalAccess: false },()=>{
            this.props.reloadDetail(this.props.group_id)
        })
        
    }
    ModalShow = () => {
        this.setState({ ModalAccess: true })
    }

    handeSwitchClick = (e) => {
        const item = e.target.id;
        const isChecked = e.target.checked;
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันการเปลี่ยนข้อมูลแอพ?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                // console.log(item)
          
                if (!isChecked) {
                    const param = {
                        work: "ADMIN_REMOVE_APPLICATION_FROM_DRAWER_GROUP",
                        user_id:"5cff95c2ab4eee1afc097367",
                        drawer_group_id:this.props.group_id,
                        app_id:item
                    }
                    GroupService.removeAppToGroup(param).then(res => {
                        if (res.status === 200) {
                            this.setState({ group: res.data.result })
                        }
                    }).catch(err => {
                        console.log(err)
                    })
                } else {
                    const param = {
                        work: "ADMIN_ADD_APPLICATION_TO_DRAWER_GROUP",
                        user_id:"5cff95c2ab4eee1afc097367",
                        drawer_group_id:this.props.group_id,
                        app_id:item
                    }
                    GroupService.addAppToGroup(param).then(res => {
                        if (res.status === 200) {
                            this.setState({ group: res.data.result })
                        }
                    }).catch(err => {
                        console.log(err)
                    })
                }
            }
        })
        

        // console.log(apps)
    }

    renderApps(apps) {
        var arr = []
        // eslint-disable-next-line array-callback-return
        this.state.apps_all.map((item)=> {
            var isCheck = false
            // eslint-disable-next-line array-callback-return
            apps.map((app) => {
                if (app.app_id === item._id) {
                    isCheck = true
                } 
            })
            if(isCheck){
                arr.push(
                    <ListGroup.Item className="d-flex justify-content-between align-items-center" key={item._id}>
                        <div><Image style={{ width: 32, height: 32, float: 'left', marginRight: 10 }} src={item.icon_url} fluid='true'></Image>{item.name}</div>

                        <Form>
                            <Form.Check
                                type="switch"
                                id={item._id}
                                label=""
                                defaultChecked
                                onChange={this.handeSwitchClick}
                            />
                        </Form>
                    </ListGroup.Item>
                )
            }else {
                arr.push(
                    <ListGroup.Item className="d-flex justify-content-between align-items-center" key={item._id}>
                        <div><Image style={{ width: 32, height: 32, float: 'left', marginRight: 10 }} src={item.icon_url} fluid='true'></Image>{item.name}</div>

                        <Form>
                            <Form.Check
                                type="switch"
                                id={item._id}
                                label=""
                                onChange={this.handeSwitchClick}
                            />
                        </Form>
                    </ListGroup.Item>
                )
            }
        })
        return arr
    }
    render() {
        // console.log(this.props)
        return (
            <>
                {/* <Button variant="outline-primary">Add/Edit Access</Button> */}
                <Button variant="outline-primary" onClick={this.ModalShow}>
                    Add/Edit Access
                </Button>
                <Modal show={this.state.ModalAccess} onHide={this.ModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title className="h5">App Access Setting</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ListGroup variant="flush">
                            {this.props.apps ? this.renderApps(this.props.apps) : ''}
                        </ListGroup>


                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outline-primary" onClick={this.ModalClose}>
                            Close
                    </Button>
                        {/* <Button variant="primary" onClick={this.ModalClose}>
                            Save Changes
                    </Button> */}
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}
