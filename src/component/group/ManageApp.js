/* eslint-disable array-callback-return */
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { Button, ListGroup, Media } from 'react-bootstrap'
import { history } from '../../store/history'
import { GroupService } from '../../services/GroupService'
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import update_icon from '../../images/update-icon.svg';
import Swal from 'sweetalert2'
import { trackPromise } from 'react-promise-tracker'

const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

// const getItemStyle = (isDragging, draggableStyle) => ({
//     // some basic styles to make the items look a bit nicer
//     userSelect: "none",
//     // change background colour if dragging
//     background: isDragging ? "lightgreen" : "light",

//     // styles we need to apply on draggables
//     ...draggableStyle
// });

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? "lightblue" : "light",
    // padding: grid,
    //   width: 250
});

export default class ManageApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selected_apps: [],
            items: [],
            newItems: [],
            isChange: false
        }
        this.onDragEnd = this.onDragEnd.bind(this);
    }
    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const items = reorder(
            this.state.items,
            result.source.index,
            result.destination.index
        );

        this.setState({
            items
        });
        this.setState({
            isChange: true
        });
    }
    renderNewApps() {
        return (
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                        <div
                            {...provided.droppableProps}
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}
                        >
                            {this.state.items.map((item, index) => (
                                <Draggable key={item._id} draggableId={item._id} index={index}>
                                    {(provided, snapshot) => (
                                        <ListGroup.Item className="d-flex justify-content-between align-items-center"
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            // style={getItemStyle(
                                            //     snapshot.isDragging,
                                            //     provided.draggableProps.style
                                            // )}
                                        >
                                            {/* {item.name} */}
                                            <Media>
                                                <img className="mr-3" width="30" src={item.icon_url} alt='' />
                                                <Media.Body>
                                                    <h5>{item.name}</h5>
                                                </Media.Body>
                                            </Media>
                                            <Button onClick={() => history.push({
                                                pathname: '/manage-setting-app/' + item._id,
                                                state: {
                                                    apps: this.state.default_apps
                                                }
                                            })} variant="outline-primary">Edit</Button>
                                        </ListGroup.Item>

                                        // </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
            </DragDropContext>
        );
    }
    renderApps() {
        var arr = []
        const { apps } = this.state
        if (apps !== undefined) {
            apps.map(app => {
                arr.push(
                    <ListGroup.Item className="d-flex justify-content-between align-items-center" key={app._id}>
                        {/* <Form>
                            <Form.Check
                                type="switch"
                                id={app.id}
                                label={app.name}
                                defaultChecked
                                onChange={this.handeSwitchClick}
                            />
                        </Form> */}
                        <Media>
                            <img className="mr-3" width="30" src={app.icon_url} alt='' />
                            <Media.Body>
                                <h5>{app.name}</h5>
                            </Media.Body>
                        </Media>
                        <Button onClick={() => history.push({
                            pathname: '/manage-setting-app/' + app._id,
                            state: {
                                apps: this.state.default_apps
                            }
                        })} variant="outline-primary">Edit</Button>

                    </ListGroup.Item>
                )
            })
        }

        return arr
    }
    componentDidMount() {
        let params = {
            "work": "GET_APPS",
            "app_version_number": 1000
        }
        
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {                
                this.setState({ items: res.data.result })
                // this.setState({ apps: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
        // this.setState({ apps: JSON.parse(sessionStorage.getItem('default_apps')) }, () => {

        // })
        //this.setState({selected_apps:JSON.parse(sessionStorage.getItem('default_apps'))})
    }
    onUpdate = async () => {
        let tempex = []
        this.state.items.map((item, index) => {
            var obj = {
                "index": index,
                "application_id": item._id               
            }
            tempex.push(obj);
        })
        const param =
        {
            "work": "ADMIN_SORTING_APPLICATION",
            "apps": tempex
        }
        console.log(param)
        this.setState({
            isChange: false
        });
        trackPromise(
            GroupService.requestAPI(param).then(res => {
                console.log(res)
                if (res.status === 200) {
                    if (res.data.status === true) {
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'จัดเรียงแอพเรียบร้อย',
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }
            })
        )
    }
    render() {
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className="d-flex bd-highlight mt-2">
                        <div className="mr-auto p-2 bd-highlight">
                            <Link to="/groups"><p className="text-muted mb-1" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                            <h4 className="h4">Manage App</h4>
                        </div>
                        <div className="p-0 flex-shrink-1 bd-highlight pt-4">
                            <Button
                                onClick={this.onUpdate} variant="outline-primary" className="clearfix mr-2 " disabled={!this.state.isChange}>
                                {/* <span className="fas fa-plus float-left mr-2 pt-1"></span> */}
                                <img className="fas fa-plus float-left mr-1" src={update_icon} alt="" />
                                <span className="float-right">Update</span>
                            </Button>
                            <Button
                                onClick={() => history.push({
                                    pathname: '/manage-create-app/',
                                    state: {
                                        apps: this.state.default_apps
                                    }
                                })} variant="primary" className="clearfix"><span className="fas fa-plus float-left mr-2 pt-1"></span><span className="float-right">New App</span>
                            </Button>
                        </div>
                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-1 pb-2 mb-3">

                        {/* <div>
                            <h4 className="h4">Manage App</h4>
                        </div> */}
                    </div>
                    <div className="card border-0">
                        <div className="card-header bg-white border-0">
                            <h5>List Apps</h5>
                        </div>
                        <div className="card-body" >

                            <ListGroup variant="flush">
                                {this.renderNewApps()}
                                {/* {this.renderApps()} */}
                            </ListGroup>
                        </div>
                    </div>
                    {/* <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                        <button type="button" className="btn btn-primary"> Create Group</button>
                    </div> */}
                </div>
            </div>
        )
    }
}
