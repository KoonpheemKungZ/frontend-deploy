import Modal from 'react-bootstrap/Modal'
import React, { Component } from 'react'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import { Cookies } from "react-cookie"
import { GroupService, authSign } from '../../services/GroupService';

export default class SelectApiName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ModalSelect: false,
            names_all: [],
            api_name: undefined
        }
    }
    ModalClose = () => {
        this.setState({ ModalSelect: false })
    }
    ModalShow = () => {
        this.setState({ ModalSelect: true })
        
    }
    handeClickSelect = (item) => {
        this.props.selected_api(item)
        this.setState({ api_name: item.role_name })
        this.ModalClose()
        
    }
    componentDidMount() {
        let params = {
            "work": "DRAWER-GROUP:CREATE-FORM.GET",
        }
        authSign().then(res => {
            if (res.data.token !== undefined) {
                const cookies = new Cookies()
                cookies.set('token', res.data.token)
                cookies.set('tmp', res.data.tmp)

                GroupService.getGroups(params).then(res => {
                    console.log(res.data)
                    if (res.status === 200) {
                        if (res.status === 200) {
                            this.setState({ names_all: res.data.result.user_group_roles })
                        }
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
        }).catch(err => {
            console.log(err)
        })
    }

    renderApps() {
        var arr = []
        this.state.names_all.map((item) => {
            return arr.push(
                <ListGroup.Item action 
                    className="d-flex justify-content-between align-items-center" key={item._id}
                    onClick={()=>this.handeClickSelect(item)}
                >
                    {item.role_name}
                </ListGroup.Item>
            )
        })
        return arr
    }

    render() {
        return (
            <>
                <Button variant="outline-light" className="border text-muted" onClick={this.ModalShow} block>
                    <div class="d-flex justify-content-between">
                        <span>{this.state.api_name===undefined?'Select Group Name':this.state.api_name}</span>
                    </div>
                </Button>

                <Modal show={this.state.ModalSelect} onHide={this.ModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Select API Name</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ListGroup variant="flush">
                            {this.renderApps()}
                        </ListGroup>
                    </Modal.Body>
                    {/* <Modal.Footer>
                        <Button variant="secondary" onClick={this.ModalClose}>
                            Close
                        </Button>
                        <Button variant="primary" onClick={this.ModalClose}>
                            Save Changes
                        </Button>
                    </Modal.Footer> */}
                </Modal>
            </>
        );
    }

}