import GroupList from './Group';
import GroupDetail from './GroupDetail';
import ManageGroup from './ManageGroup';
import OtherGroup from './OtherGroup';

export {
    GroupList,
    GroupDetail,
    ManageGroup,
    OtherGroup
};