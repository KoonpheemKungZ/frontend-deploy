import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { Button, ListGroup, Media } from 'react-bootstrap'
import { GroupService } from '../../services/GroupService'
import Swal from 'sweetalert2'
import XLSX from 'xlsx'
import { make_cols } from './MakeColumns';


export default class AddMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ModalAccess: false,
            persons: [],
            search_key: '',
            file: {},
            data: [],
            cols: [],
            msg: "พิมพ์ชี่อที่ต้องการค้นหา"
        }
        this.handleFile = this.handleFile.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    componentDidMount() {
        this.searchChangeHandler('')
    }
    searchChangeHandler = (search_member) => {

        const { id } = this.props.match.params
        const param = {
            "work": "ADMIN_DYNAMIC_SEARCH_USER",
            "language": "en",
            "drawer_group_id": id,
            "excepted_group": {
                "drawer_group_id": id,
                "full_name": search_member,
                "item_amount": 0,
                "item_per_request": 100
            }
        }
        // console.log(param)
        GroupService.requestAPI(param).then(res => {
            // console.log(res)
            if (res.status === 200) {
                if (res.data.result !== undefined)
                    this.setState({ persons: res.data.result })
                if (res.data.result.length === 0)
                    this.setState({ msg: "user not found" })
            }
        }).catch(err => {
            console.log(err)
        })
        this.setState({ search_member: search_member });
    }
    handleChange(e) {
        const files = e.target.files;
        if (files && files[0]) this.setState({ file: files[0] },()=>this.handleFile());
    };
    handleFile() {
        /* Boilerplate to set up FileReader */
        const reader = new FileReader();
        const rABS = !!reader.readAsBinaryString;

        reader.onload = (e) => {
            /* Parse data */
            const bstr = e.target.result;
            const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA: true });
            /* Get first worksheet */
            const wsname = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];
            /* Convert array of arrays */
            const data = XLSX.utils.sheet_to_json(ws,{raw: false});
            /* Update state */
            this.setState({ data: data, cols: make_cols(ws['!ref']) }, () => {
                const params = {
                    "work": "ADMIN_MANUAL_ADD_USERS",
                    "users": this.state.data,
                    "drawer_group_id": this.props.group_id
                }
                GroupService.requestAPI(params).then(res => {
                    console.log(params)
                    if (res.status === 200) {
                        // console.log(res)
                        Swal.fire({
                            position: 'top',
                            icon: 'success',
                            title: 'การเพิ่มสมาชิกสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        this.props.addMemberSuccess()
                        this.ModalClose()
                    }
                }).catch(err => {
                    console.log(err)
                })
                //console.log(JSON.stringify(params, null, 2));
            });

        };

        if (rABS) {
            reader.readAsBinaryString(this.state.file);
        } else {
            reader.readAsArrayBuffer(this.state.file);
        };
    }

    onSelectedGroup = (user_id) => {
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันการเพิ่มสมาชิกเข้ากลุ่ม?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                this.requestChangeGroup(user_id)
            }
        })
    }
    requestChangeGroup(user_id) {
        const params = {
            "work": "ADMIN_UPDATE_USER_DRAWER_GROUP",
            "user_id": user_id,
            "drawer_group_id": this.props.group_id

        }

        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'การเพิ่มสมาชิกสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                })
                this.searchChangeHandler(this.state.search_member)
                this.props.addMemberSuccess()
            }
        }).catch(err => {
            console.log(err)
        })
    }
    renderPerson() {
        const { persons } = this.state
        // console.log(persons)
        var arr = []
        if (persons.length === 0) {
            arr.push(
                <ListGroup.Item className="py-4 d-flex justify-content-between align-items-center" key={0}>
                    {/* User not found */}
                    {this.state.msg}
                </ListGroup.Item>
            )
        } else {
            for (let index = 0; index < persons.length; index++) {
                const element = persons[index];
                arr.push(
                    <ListGroup.Item className="d-flex justify-content-between align-items-center" key={index + "-listgroup"}>
                        <Media className='w-50'>
                            <img
                                width={40}
                                height={40}
                                className="mr-2 rounded-circle"
                                src={element.user.profile_url}
                                alt=""
                                onError={(e) => { e.target.onerror = null; e.target.src = "https://yt3.ggpht.com/a/AGF-l7_KcMkAm641_17l3LYRN9jbx8kA6SyZONwang=s48-c-k-c0xffffffff-no-rj-mo" }}
                            />
                            <Media.Body >
                                <h6 className="mb-0">{element.user.first_name} {element.user.last_name}</h6>
                                <Icon className="fa fa-users float-left" style={{ "fontSize": "1rem", color: "#888888", "width": "30px" }} />
                                <p className="mb-0">Group : {element.drawer.title}</p>
                            </Media.Body>
                        </Media>
                        <Button variant="outline-primary" onClick={this.onSelectedGroup.bind(this, element.user._id)}>Add</Button>
                    </ListGroup.Item>
                )
            }
        }

        return arr
    }
    render() {
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Link to="/groups"><p className="text-muted" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">Add Members</h4>

                        </div>
                        <div>
                            <div className="upload-btn-wrapper float-right">
                                <Button style={{ marginBottom: 10 }} className="btn float-right delete-news" variant="outline-secondary" onClick={this.uploadFileManual}>Add Manual</Button>
                                <input type="file" id="myfile" name="myfile" accept={".xlsx, .csv"} onChange={this.handleChange} />
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        {/* <div className="card-header bg-white">
                            
                        </div> */}
                        <div className="card-body" >
                            <div className="input-group mb-2 mr-3" style={{ marginTop: 20, marginBottom: 30 }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text bg-white"><Icon>search</Icon></div>
                                </div>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="inlineFormInputGroup"
                                    placeholder="Search Members"
                                    onChange={e => this.setState({ search_key: e.target.value }, this.searchChangeHandler(e.target.value))}
                                />
                            </div>
                        </div>
                        <div className="card-body" >
                            {/* <Row>
                            <Col sm={12}> */}
                            <ListGroup>
                                {this.renderPerson()}

                            </ListGroup>
                            {/* </Col>
                        </Row> */}
                        </div>
                    </div>
                    <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                        {/* <button type="button" className="btn btn-primary"> Confirm</button> */}
                    </div>
                </div>
            </div>
        )
    }
}

