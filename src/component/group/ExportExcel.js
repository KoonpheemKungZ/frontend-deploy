import React from "react";
import ReactExport from "react-export-excel";
// import iconexcel from '../../images/icons8-microsoft-excel-48.png';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2'
import { GroupService } from "../../services/GroupService";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

// const dataSet1 = [
//     {
//         name: "Johson",
//         amount: 30000,
//         sex: 'M',
//         is_married: true
//     },
//     {
//         name: "Monika",
//         amount: 355000,
//         sex: 'F',
//         is_married: false
//     },
//     {
//         name: "John",
//         amount: 250000,
//         sex: 'M',
//         is_married: false
//     },
//     {
//         name: "Josef",
//         amount: 450500,
//         sex: 'M',
//         is_married: true
//     }
// ];


// obj['updated_at'] = user.updated_at;  
//             obj['register_number'] = user.register_number;  
//             obj['reg_date'] = user.reg_date; 
//             obj['user_id'] = user.user_id; 
//             obj['total_price'] = user.total_price; 
//             obj['status'] = user.status; 
//             obj['name'] = user.name; 
//             obj['distance'] = user.tickets[0].distance; 
//             obj['t-shirt'] = user.name; 
//             obj['scarf'] = user.name;


class ExportExcel extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            datas: [],
            reguers:[],
        }

        this.initialPage() 
    }

    async initialPage() {
        await this.loadData();
    }

    async loadData() {
        const param = {
            "work": "GET_USERS_IN_DRAWER_GROUP",
            "drawer_group_id":this.props.group_id
        }
        Swal.fire({
            title: 'กำลังโหลดข้อมูล',
            showConfirmButton: false,
            onBeforeOpen: () => {
                Swal.showLoading()
                GroupService.requestAPI(param).then(res => {
                    Swal.close()
                    // console.log(res)
                    if (res.data && res.status === 200) {
                        const { reguers } = this.state
                        // console.log(res.data.result)
                        if (res.data.result !== undefined && res.data.result !== null) {
                            this.setState({
                                datas: res.data.result || []
                            },()=>
                            // eslint-disable-next-line array-callback-return
                            this.state.datas.map((user, i) => {
                                var obj = {};
                                obj['employee_id'] = `${user.employee_id}`
                                obj['first_name'] = `${user.first_name}`
                                obj['last_name'] = user.last_name
                                reguers.push(obj);
                            })
                            )
                        }
                    }
                })
            }
        })
        
    }

    render() {
        const reguers = this.state.reguers
        // console.log(dataSet1)
        return (
            <ExcelFile element={<Button variant="outline-success">Export Data <i className="fa fa-file-download"></i></Button>}>
                <ExcelSheet  data={reguers} name="Employees">
                    <ExcelColumn label="Employee ID" value="employee_id" />
                    <ExcelColumn label="Firstname" value="first_name" />
                    <ExcelColumn label="Lastname" value="last_name"/>
                </ExcelSheet>
                
            </ExcelFile>
        );
    }
}
export default ExportExcel;