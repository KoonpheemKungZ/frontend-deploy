import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

import { GroupService } from '../../services/GroupService'

export default class EditGroupName extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ModalAccess: false,
            group_name:''
        }
    }

    // componentDidMount(){
    //     const { group_name } = this.props
    //     if (group_name !== undefined) {
    //         console.log(group_name)
    //         this.setState({group_name:group_name})
    //     }
    // }

    componentWillReceiveProps(nextProps){
        const { group_name } = this.props
        if (group_name !== undefined) {
            this.setState({group_name:group_name})
        }
    }

    requestChageName=()=>{
        const { group_name } = this.state
        const param = {
            "work":"ADMIN_EDIT_DRAWER_GROUP_TITLE",
            "title": group_name,
            user_id: "5cff95c2ab4eee1afc097367",
            "drawer_group_id":this.props.group_id
        }
        GroupService.removeAppToGroup(param).then(res => {
            if (res.status === 200) {
                this.props.editSuccess()
                this.ModalClose()
            }
        }).catch(err => {
            console.log(err)
        })
    }
    ModalClose = () => {
        this.setState({ ModalAccess: false })
    }
    ModalShow = () => {
        this.setState({ ModalAccess: true })
    }
    render() {
        return (
            <>
                {/* <Button variant="outline-primary">Add/Edit Access</Button> */}
                <Button variant="link" className="p-0" onClick={this.ModalShow}>
                    Edit
                </Button>
                <Modal show={this.state.ModalAccess} onHide={this.ModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title className="h5">Edit Group Name</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Row>
                            <Col sm={12}>
                                <Form>
                                    <Form.Group controlId="fromEditName">
                                        <Form.Label>Group Name</Form.Label>
                                        <Form.Control type="text" value={this.state.group_name} onChange={(e)=>this.setState({group_name:e.target.value})} />
                                    </Form.Group>                                  
                                </Form>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="outline-primary" onClick={this.ModalClose}>
                            Cancle
                    </Button>
                        <Button variant="primary" onClick={this.requestChageName} >
                            Done
                    </Button>
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}
