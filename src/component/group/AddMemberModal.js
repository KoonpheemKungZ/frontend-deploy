import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import ListGroup from 'react-bootstrap/ListGroup'
import Media from 'react-bootstrap/Media'
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'

import { make_cols } from './MakeColumns';
import Icon from '@material-ui/core/Icon';
import { GroupService } from '../../services/GroupService'
import Swal from 'sweetalert2'
import XLSX from 'xlsx'

export default class AddMember extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ModalAccess: false,
            persons: [],
            search_key: '',
            file: {},
            data: [],
            cols: []
        }
        this.handleFile = this.handleFile.bind(this);
        this.handleChange = this.handleChange.bind(this);

      
    }

    componentDidMount() {
        this.searchChangeHandler('')
    }
    // getMember = () => {
    //     const { search_key } = this.state
    //     const param = {
    //         work: "SEARCH_MEMBER_IN_DRAWER_GROUP",
    //         user_id: "5cff95c2ab4eee1afc097367",
    //         search_key: search_key,
    //         current_item_amount: 50,
    //         item_per_request: 10,
    //         language: "en"
    //     }
    //     GroupService.requestAPI(param).then(res => {
    //         console.log(res)
    //         if (res.status === 200) {
    //             this.setState({ persons: res.data.result.member.persons })
    //         }
    //     }).catch(err => {
    //         console.log(err)
    //     })

    // }

    searchChangeHandler = (search_member) => {
        const param = {
            "work": "ADMIN_DYNAMIC_SEARCH_USER",
            "language": "en",
            "drawer_group_id": this.props.group_id,
            "excepted_group": {
                "drawer_group_id": this.props.group_id,
                "full_name": search_member,
                "item_amount": 0,
                "item_per_request": 100
            }
        }
        GroupService.requestAPI(param).then(res => {
            if (res.status === 200) {
                if (res.data.result !== undefined)
                    this.setState({ persons: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
        this.setState({ search_member: search_member });
    }

    requestChangeGroup( user_id) {
        const params = {
            "work": "ADMIN_UPDATE_USER_DRAWER_GROUP",
            "user_id": user_id,
            "drawer_group_id": this.props.group_id

        }
        
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                Swal.fire({
                    position: 'top',
                    type: 'success',
                    title: 'การเพิ่มสมาชิกสำเร็จ',
                    showConfirmButton: false,
                    timer: 1500
                })
                this.searchChangeHandler(this.state.search_member)
                this.props.addMemberSuccess()
            }
        }).catch(err => {
            console.log(err)
        })
    }

    handleChange(e) {
        const files = e.target.files;
        if (files && files[0]) this.setState({ file: files[0] },()=>this.handleFile());
    };

    handleFile() {
        /* Boilerplate to set up FileReader */
        const reader = new FileReader();
        const rABS = !!reader.readAsBinaryString;

        reader.onload = (e) => {
            /* Parse data */
            const bstr = e.target.result;
            const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA: true });
            /* Get first worksheet */
            const wsname = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];
            /* Convert array of arrays */
            const data = XLSX.utils.sheet_to_json(ws,{raw: false});
            /* Update state */
            this.setState({ data: data, cols: make_cols(ws['!ref']) }, () => {
                const params = {
                    "work": "ADMIN_MANUAL_ADD_USERS",
                    "users": this.state.data,
                    "drawer_group_id": this.props.group_id
                }
                GroupService.requestAPI(params).then(res => {
                    console.log(params)
                    if (res.status === 200) {
                        // console.log(res)
                        Swal.fire({
                            position: 'top',
                            icon: 'success',
                            title: 'การเพิ่มสมาชิกสำเร็จ',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        this.props.addMemberSuccess()
                        this.ModalClose()
                    }
                }).catch(err => {
                    console.log(err)
                })
                //console.log(JSON.stringify(params, null, 2));
            });

        };

        if (rABS) {
            reader.readAsBinaryString(this.state.file);
        } else {
            reader.readAsArrayBuffer(this.state.file);
        };
    }

    onSelectedGroup = ( user_id) => {
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันการเพิ่มสมาชิกเข้ากลุ่ม?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                this.requestChangeGroup( user_id)
            }
        })
    }

    renderPerson() {
        const { persons } = this.state
        var arr = []
        for (let index = 0; index < persons.length; index++) {
            const element = persons[index];
            arr.push(
                <ListGroup.Item className="d-flex justify-content-between align-items-center" key={index+"-listgroup"}>
                    <Media className='w-50'>
                        <img
                            width={40}
                            height={40}
                            className="mr-2 rounded-circle"
                            src={element.user.profile_url}
                            alt=""
                            onError={(e) => { e.target.onerror = null; e.target.src = "https://yt3.ggpht.com/a/AGF-l7_KcMkAm641_17l3LYRN9jbx8kA6SyZONwang=s48-c-k-c0xffffffff-no-rj-mo" }}
                        />
                        <Media.Body>
                            <h6 className="mb-0">{element.user.first_name} {element.user.last_name}</h6>
                            <Icon className="fa fa-users float-left" style={{ "fontSize": "1rem", color: "#888888", "width": "30px" }} />
                            <p className="mb-0">Group : {element.drawer.title}</p>
                        </Media.Body>
                    </Media>
                    <Button variant="outline-primary" onClick={this.onSelectedGroup.bind(this, element.user._id)}>Add</Button>
                </ListGroup.Item>
            )
        }
        return arr
    }

    ModalClose = () => {
        this.setState({ ModalAccess: false })
    }
    ModalShow = () => {
        this.setState({ ModalAccess: true })
    }
    uploadFileManual = () => {
        document.getElementById("myfile").click()
    }
    render() {
        return (
            <>
                {/* <Button variant="outline-primary">Add/Edit Access</Button> */}
                <Button variant="outline-primary" onClick={this.ModalShow}>
                    Add Member
                </Button>
                <Modal show={this.state.ModalAccess} onHide={this.ModalClose}>
                    <Modal.Header closeButton>
                        <Modal.Title className="h5">Add Member</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="float-right">
                        <Row className="d-flex">
                            <Col sm={12} className="float-right">
                                <div className="upload-btn-wrapper float-right">
                                    <Button style={{ marginBottom: 10 }} className="btn float-right delete-news" variant="outline-secondary" onClick={this.uploadFileManual}>Add Manual</Button>
                                    <input type="file" id="myfile" name="myfile" accept={".xlsx, .csv"} onChange={this.handleChange} />
                                </div>


                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12}>
                                <Form>
                                    <Form.Group as={Col} md="12" className="p-0" controlId="validationCustomUsername">
                                        {/* <Form.Label>Username</Form.Label> */}
                                        <InputGroup>
                                            <InputGroup.Prepend>
                                                <InputGroup.Text id="inputGroupPrepend"><Icon>search</Icon></InputGroup.Text>
                                            </InputGroup.Prepend>
                                            <Form.Control
                                                type="text"
                                                placeholder="Username"
                                                aria-describedby="inputGroupPrepend"
                                                onChange={e => this.setState({ search_key: e.target.value }, this.searchChangeHandler(e.target.value))}
                                            />
                                            {/* <Form.Control.Feedback type="invalid">
                                    Please choose a username.
            </Form.Control.Feedback> */}
                                        </InputGroup>
                                    </Form.Group>
                                </Form>
                            </Col>
                        </Row>
                        <Row>
                            <Col sm={12}>
                                <ListGroup>
                                    {this.renderPerson()}

                                </ListGroup>
                            </Col>
                        </Row>
                    </Modal.Body>
                    <Modal.Footer>
                        {/* <Button variant="outline-primary" onClick={this.ModalClose}>
                            Close
                    </Button>
                        <Button variant="primary" onClick={this.ModalClose}>
                            Confirm
                    </Button> */}
                    </Modal.Footer>
                </Modal>
            </>
        )
    }
}