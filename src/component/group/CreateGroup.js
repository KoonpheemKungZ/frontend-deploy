import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { ListGroup, Form } from 'react-bootstrap'
import { history } from '../../store/history'
import { withRouter } from 'react-router-dom'
import Swal from 'sweetalert2'
import { GroupService } from '../../services/GroupService'
import SelectApiName from './SelectApiName'

class CreateGroup extends Component {

    constructor(props) {
        super(props)
        this.state = {
            apps: [],
            selected_apps: [],
            name: '',
            description: '',
            type_Group: '1',
            gtitle: undefined,
            isOptionName: false
        }
    }
    

    componentDidMount() {
        const { selected_apps } = this.state
        let params = {
            "work": "GET_APPS",
            "app_version_number": 1000
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                this.setState({ apps: res.data.result }, () => {
                    // eslint-disable-next-line array-callback-return
                    this.state.apps.map((item) => {
                        // var arr = []
                        selected_apps.push(item._id)
                    })
                })
            }
        }).catch(err => {
            console.log(err)
        })
        // this.requestGroups()
        // this.setState({apps:JSON.parse(sessionStorage.getItem('default_apps'))},()=>{
        //     this.state.apps.map(item=>{
        //         // var arr = []
        //         selected_apps.push(item.id)
        //     })
        // })
        //this.setState({selected_apps:JSON.parse(sessionStorage.getItem('default_apps'))})
    }
    handleSelectShow = () => {
        this.setState({ isOptionName : true})
    }
    onClickCreate = () => {
        const { name, selected_apps, description, gtitle } = this.state
        console.log(gtitle)
        if (name === '' && gtitle._id===undefined) {
            Swal.fire(
                '',
                'กรุณาระบุชื่อกลุ่ม',
                'warning'
            )
        } else if (description === '') {
            Swal.fire(
                '',
                'กรุณาระบุคำอธิบายของกลุ่ม',
                'warning'
            )
        } else {
            // const data = {
            //     "work": 'ADMIN_CREATE_DRAWER_GROUP',
            //     "user_id": '5cff95c2ab4eee1afc097367',
            //     "title": name,
            //     "description": description,
            //     "app_ids": selected_apps
            // }

            const data = {
                "work": "DRAWER-GROUP.CREATE",
                "user_id": "5cff95c2ab4eee1afc097367",
                "title": gtitle === undefined ? name : gtitle.role_name,
                "description": description,
                "user_group_role:id": ( gtitle === null || gtitle === undefined)  ? null : gtitle._id, 
                "app_ids": selected_apps
            }
            GroupService.createGroup(data).then(res => {
                console.log(res)
                if (res.status === 200) {
                    if(res.data.status){
                        history.push('/groups')
                    }else{
                        Swal.fire(
                            '',
                            'สร้างกลุ่มไม่สำเร็จ',
                            'warning'
                        )
                    }
                } else {
                    Swal.fire(
                        '',
                        'สร้างกลุ่มไม่สำเร็จ',
                        'warning'
                    )
                }
            }).catch(err => {
                console.log(err)
            })

        }
    }

    handeSwitchClick = (e) => {
        const { selected_apps } = this.state
        const item = e.target.id;
        const isChecked = e.target.checked;
        if (!isChecked) {
            for (var i = 0; i < selected_apps.length; i++) {
                if (selected_apps[i] === item) {
                    selected_apps.splice(i, 1);
                }
            }

        } else {
            selected_apps.push(item)
        }
    }

    renderApps() {
        var arr = []
        const { apps } = this.state
        if (apps !== undefined) {
            // eslint-disable-next-line array-callback-return
            apps.map(app => {
                arr.push(
                    <ListGroup.Item className="d-flex justify-content-between align-items-center" key={app._id}>
                        <Form>
                            <Form.Check
                                type="switch"
                                id={app._id}
                                label={app.name}
                                defaultChecked
                                onChange={this.handeSwitchClick}
                            />
                        </Form>
                        <img width="30" src={app.icon_url} alt='' />
                    </ListGroup.Item>
                )
            })
        }

        return arr
    }

    onChangeTypeGroup = (e) => {
        this.setState({ type_Group: e.target.value })
       
    }
    onSelectName = (item) => {
        this.setState({ gtitle: item })
       
    }
    // requestGroups() {
    //     const param = {
    //         work: 'ADMIN_GET_DRAWER_GROUPS',
    //         user_id: '5cff95c2ab4eee1afc097367'
    //     }
    //     GroupService.getGroups(param).then(res => {
    //         console.log(res.data.result)
    //         if (res.status === 200) {
    //             this.setState({ gtitle: res.data.result.groups })

    //         }
    //     }).catch(err => {
    //         console.log(err)
    //     })
    // }
    // renderOption() {
    //     var arr = []
    //     const { gtitle } = this.state
    //     if (gtitle !== undefined) {
    //         // eslint-disable-next-line array-callback-return
    //         gtitle.map(g => {
    //             arr.push(
    //                 <option>{g.title}</option>
    //             )
    //         })
    //     }

    //     return arr
    // }

    render() {
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Link to="/groups"><p className="text-muted" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">Create Group</h4>
                        </div>
                    </div>
                    <div className="card">
                        {/* <div className="card-header bg-white">
                            
                        </div> */}
                        <div className="card-body" >
                            <div>
                                <p>Group Name</p>
                            </div>
                            {/* <div class="form-check form-check-inline mb-3"> */}
                            <div className="custom-control custom-radio custom-control-inline">
                                <input
                                    type="radio"
                                    id="radio1"
                                    name={'type_Group'}
                                    className="custom-control-input"
                                    onChange={this.onChangeTypeGroup}
                                    value={'1'}
                                    defaultChecked
                                // checked={(this.state.type_Group === '1') ? true : false}
                                />
                                <label className="custom-control-label" htmlFor={'radio1'}>Custom Group Name</label>
                            </div>
                            <div className="custom-control custom-radio custom-control-inline mb-3">
                                <input
                                    type="radio"
                                    id="radio2"
                                    name={'type_Group'}
                                    className="custom-control-input"
                                    onChange={this.onChangeTypeGroup}
                                    value={'2'}
                                // checked={(this.state.type_Group === '2') ? true : false}
                                />
                                <label className="custom-control-label" htmlFor={'radio2'}>Create Group Name By API</label>
                            </div>
                            {/* </div> */}
                            <div className="input-group mb-2 mr-3">
                                {this.state.type_Group === '1' ? <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Name" value={this.state.name} onChange={e => this.setState({ name: e.target.value })} /> : <>
                                    {/* <Form.Control as="select">
                                        {this.renderOption()} 
                                    </Form.Control> */}
                                    <SelectApiName  selected_api={this.onSelectName.bind(this)} />
                                     {/* <Form.Control type="text" placeholder="Select Group Name" readOnly onClick={this.handleSelectShow} /> */}
                                </>}

                            </div>
                            <div className="input-group mb-2 mr-3">
                                
                                <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="description" value={this.state.description} onChange={e => this.setState({ description: e.target.value })} />
                            </div>
                            <div>
                                <p>App Access</p>
                            </div>
                            <ListGroup variant="flush">
                                {this.renderApps()}
                            </ListGroup>
                        </div>
                    </div>
                    {/* <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                        <ListGroup variant="flush">
                            { this.renderApps() }
                        </ListGroup>
                    </div> */}
                    <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                        <button type="button" className="btn btn-primary" onClick={this.onClickCreate}> Create Group</button>
                    </div>
                </div>
            </div>
        )
    }
}

export default withRouter(CreateGroup)

