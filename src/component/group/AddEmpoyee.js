import React, { Component } from 'react'
import Icon from '@material-ui/core/Icon'
import { Button } from 'react-bootstrap'
// import readXlsxFile from 'read-excel-file'
// import { NULL } from 'node-sass'
import usergroup from '../../images/user-group.svg';
import { GroupService } from '../../services/GroupService'

import Swal from 'sweetalert2'
import ProgressBar from 'react-bootstrap/ProgressBar'
import iconchecked from '../../images/ic_checked.svg';
import { Link } from 'react-router-dom'
import XLSX from 'xlsx'
import { history } from '../../store/history';

// const schema = {
//     'employee_id': {
//         prop: 'employee_id',
//         type: String
//     },
//     'first_name': {
//         prop: 'employee_id',
//         type: String
//     },
//     'last_name': {
//         prop: 'employee_id',
//         type: String
//     }
// }
export default class AddMember extends Component {
    constructor(props) {
        super(props)
        this.state = {
            isShow: false,
            persons: [],
            search_key: '',
            file: false,
            data: [],
            cols: [],
            row_emp: [],
            apps: [],
            reference_role_resource_id: undefined,
            num_row_emp: 0,
            role: undefined,
            linkBack: '',
            name:'',
            display_url:'',
        };
    }

    componentDidMount() {

        this.searchChangeHandler('')
        if (this.props.location.state !== undefined) {
            this.setState({
                apps: this.props.location.state.app,
                linkBack: this.props.location.state.back,
                reference_role_resource_id: this.props.location.state.reference_role_resource_id,
                role: this.props.location.state.role,
                display_url: this.props.location.state.display_url,
                name: this.props.location.state.name,
            });
            if (this.props.location.state.app !== undefined) {
                this.getSearchUser()
                this.getNumberUser()
            }
        }


    }
    getSearchUser() {
        const { app, reference_role_resource_id } = this.props.location.state
        const params = {
            "work": "ADMIN_SEARCH_USER_IN_ROLE",
            "search_value": "",
            "application_id": app._id,
            "reference_role_resource_id": reference_role_resource_id,
            "item_per_request": 100,
            "current_item_amount": 0
        }

        GroupService.requestAPI(params).then(res => {
            if (res.data.status === true) {
                if (res.data.result !== undefined) {
                    this.setState({ row_emp: res.data.result })
                }

            }
        }).catch(err => {
            console.log(err)
        })
    }
    getNumberUser() {
        const { app, reference_role_resource_id } = this.props.location.state
        let params = {
            "work": "ADMIN_GET_NUMBER_OF_USERS_IN_ROLE",
            "application_id": app._id,
            "reference_role_resource_id": reference_role_resource_id,
        }
        GroupService.requestAPI(params).then(res => {
            if (res.data.status === true) {
                this.setState({ num_row_emp: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
    }


    uploadFileemp = () => {
        document.getElementById("fileemp").click()
    }

    handleChange(e) {
        const files = e.target.files;
        if (files && files[0]) this.setState({ file: files[0] }, () => this.handleFile());
    }
    fileHandler = (event) => {
        // const { num_row_emp } = this.state
        this.setState({ progress_count: 10 })
        this.setState({ isShow: true })
        let fileObj = event.target.files[0];

        const reader = new FileReader(fileObj);
        const rABS = !!reader.readAsBinaryString;

        // setTimeout(() => {

        // }, 3000);
        this.setState({ progress_count: 10 })
        reader.onload = (e) => {
            /* Parse data */
            const bstr = e.target.result;
            const wb = XLSX.read(bstr, { type: rABS ? 'binary' : 'array', bookVBA: true });
            /* Get first worksheet */
            const wsname = wb.SheetNames[0];
            const ws = wb.Sheets[wsname];
            /* Convert array of arrays */
            var data = XLSX.utils.sheet_to_json(ws, { raw: false });
            data = data.map(function (el) {
                
                return {employee_id: el.employee_id.trim()}
              });
            console.log(data)
            this.setState({ data: data })
            this.setState({ file: true })
            this.setState({ progress_count: 100 })
        };

        if (rABS) {
            reader.readAsBinaryString(fileObj);
        } else {
            reader.readAsArrayBuffer(fileObj);
        };

        // setTimeout(() => {
        //     this.setState({ progress_count: 40 })
        //     readXlsxFile(fileObj, { schema }).then(({ rows }) => {
        //         var arr = []
        //         try {
        //             this.setState({ progress_count: 70 })
        //             arr = rows.reduce((acc, val) => [...acc, { "employee_id": (val.employee_id).toString() }], [])
        //             console.log('s')

        //         } catch (e) {
        //             console.log(e)
        //             this.setState({ progress_status: 'danger' })
        //             this.setState({ progress_count: 100 })
        //         }
        //         this.setState({ data: arr })
        //         this.setState({ file: true })
        //         this.setState({ progress_count: 100 })
        //         console.log(arr)

        //         // this.setState({ file: true })
        //         // this.setState({ num_row_emp: num_row_emp + arr.length })
        //     })
        // }, 3000);
    }

    searchChangeHandler = (search_member) => {
        const param = {
            work: "ADMIN_DYNAMIC_SEARCH_USER",
            user_id: "5cff95c2ab4eee1afc097367",
            language: "en",
            without_group: {
                full_name: search_member,
                item_amount: 0,
                item_per_request: 100
            }
        }

        GroupService.requestAPI(param).then(res => {
            if (res.status === 200) {
                if (res.data.result !== undefined)
                    this.setState({ persons: res.data.result })
                else
                    this.setState({ persons: [] })
            }
        }).catch(err => {
            console.log(err)
            this.setState({ persons: [] })
        })
        this.setState({ search_key: search_member });
    }

    onConfirm = () => {

    }

    renderPerson() {
        const { persons, data, row_emp } = this.state
        // const { row_emp } = this.props.params
        var arr = []
        for (let index = 0; index < persons.length; index++) {
            const element = persons[index];
            // console.log(row_emp)
            const { user } = element
            // console.log(row_emp.find(element => element.employee_id === user.ref_id))        
            arr.push(
                <li className="list-group-item border-0 px-0" key={index}>
                    <div className="d-flex justify-content-between border-bottom">
                        <div>
                            <h5 className="mb-4">{user.first_name} {user.last_name}</h5>
                            {/* <p className="mb-1">140004050696</p> */}
                        </div>
                        <div>
                            {/* ic_checked.svg */}
                            {/* {console.log(user)}
                            {console.log(data.find(function (element) {
                                return element.employee_id === user.ref_id;
                            }))} */}
                            {(row_emp.find(element => element.employee_id === user.ref_id) === undefined) && (data.find(element => element.employee_id === user.ref_id) === undefined) ? <Button
                                variant="outline-primary"
                                onClick={this.onSelectedGroup.bind(this, user)}
                                // size="sm" 
                                className="border float-right"
                                ref={btn => { this.btn = btn; }}
                            >Add
                            </Button> : <Button disabled
                                variant="outline-primary"
                                onClick={this.onSelectedGroup.bind(this, user)}
                                // size="sm" 
                                className="border float-right"
                            ><img className="mx-1" src={iconchecked} alt="" />
                            </Button>
                            }
                        </div>
                    </div>
                </li >
            )
        }
        return arr
    }

    onSelectedGroup = (user) => {
        this.btn.setAttribute("disabled", "disabled");
        const { apps } = this.state
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันการเพิ่ม?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                // const arr = data;
                // if (arr.length > 0) {
                // arr.push(
                //     {
                //         "employee_id": user.ref_id,
                //         "first_name": user.first_name,
                //         "last_name": user.last_name
                //     }
                // )
                if (apps) {
                    this.addUserByOne({
                        "employee_id": user.ref_id,
                        "first_name": user.first_name,
                        "last_name": user.last_name
                    })
                } else {

                    // this.setState({ data: arr })
                    // this.setState({ num_row_emp: num_row_emp + 1 })
                }



                // this.requestChangeGroup(user_id)
            }
        })
    }
    addUserByOne(user) {
        const { apps, reference_role_resource_id } = this.state
        if(apps === null || apps === undefined){
            history.replace(
                {
                    pathname:this.state.linkBack,
                    state: {
                        name: this.state.name,
                        display_url: this.state.display_url,
                        app: this.state.apps,
                        reference_role_resource_id: [reference_role_resource_id],
                        role: this.state.role,
                    }
                }
            )
            return
        }
        let params = {
            "work": "ADMIN_ADD_USER_TO_ROLE",
            "employee_id": user.employee_id,
            "first_name": user.first_name,
            "last_name": user.last_name,
            "application_id": apps._id,
            "reference_role_resource_id": reference_role_resource_id
        }
        GroupService.requestAPI(params).then(res => {
            console.log(res)
            if (res.status === 200) {
                if (res.data.status === true) {
                    this.setState({ num_row_emp: res.data.result })
                    // history.push('/manage-app')
                    Swal.fire({
                        position: 'top',
                        type: 'success',
                        title: 'เพิ่มข้อมูลเรียบร้อย',
                        showConfirmButton: false,
                        timer: 2000
                    })
                } else {
                    // this.btn.removeAttribute("disabled");
                    Swal.fire({
                        position: 'top',
                        type: 'warning',
                        title: res.data.message,
                        showConfirmButton: false,
                        timer: 2000
                    })
                }

            }
        }).catch(err => {
            console.log(err)
        })
    }

    addUserByMultiple = () => {
        const { apps, reference_role_resource_id, data } = this.state
        if(apps === null || apps === undefined){
            history.replace(
                {
                    pathname:this.state.linkBack,
                    state: {
                        name: this.state.name,
                        display_url: this.state.display_url,
                        app: this.state.apps,
                        reference_role_resource_id: data,
                        role: this.state.role,
                    }
                }
            )
            return
        }
        let params = {
            "work": "ADMIN_ADD_USERS_TO_ROLE",
            "application_id": apps._id,
            "reference_role_resource_id": reference_role_resource_id,
            "resources": data,
        }
        // console.log(params)
        GroupService.requestAPI(params).then(res => {
            // console.log(res)
            if (res.status === 200) {
                if (res.data.status === true) {
                    this.setState({ num_row_emp: res.data.result })
                    // history.push('/manage-app')
                    history.goBack()
                    this.setState({ data: [] })
                    Swal.fire({
                        position: 'top',
                        type: 'success',
                        title: 'เพิ่มข้อมูลเรียบร้อย',
                        showConfirmButton: false,
                        timer: 2000
                    })
                } else {
                    // this.btn.removeAttribute("disabled");
                    Swal.fire({
                        position: 'top',
                        type: 'warning',
                        title: res.data.message,
                        showConfirmButton: false,
                        timer: 2000
                    })
                }

            }
        }).catch(err => {
            console.log(err)
        })
    }

    render() {
        const { data, role } = this.state
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Button variant="link" className="pl-0">
                                <Link
                                    to={
                                        {
                                            pathname:this.state.linkBack,
                                            state: {
                                                name: this.state.name,
                                                display_url: this.state.display_url,
                                                app: this.state.apps,
                                                reference_role_resource_id: this.state.reference_role_resource_id,
                                                role: this.state.role,
                                            }
                                        }
                                    }
                                    
                                    // onClick={e=>history.goBack()}
                                >
                                    <p className="text-muted pl-0 m-0" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p>
                                </Link>
                            </Button >
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">Add Member To Role</h4>

                        </div>
                        <div>
                            {/* <Button variant="outline-secondary">Add Manual</Button> */}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-lg-8">
                            <div className="card mb-2">
                                <div className="card-body" >
                                    <div className="input-group mb-2 mr-3" style={{ marginTop: 20, marginBottom: 30 }}>
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-white"><Icon>search</Icon></div>
                                        </div>
                                        <input
                                            type="text"
                                            className="form-control"
                                            id="inlineFormInputGroup"
                                            placeholder="Search Members"
                                            onChange={e => this.setState({ search_key: e.target.value }, this.searchChangeHandler(e.target.value))}
                                        />
                                    </div>
                                    <ul className="list-group">
                                        {this.renderPerson()}
                                    </ul>
                                </div>
                            </div>
                            <div className="card">
                                <div className="card-body" >
                                    <div className="d-flex justify-content-between">
                                        <div className="media">
                                            <svg className="mt-2 mr-3" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm2 16H8v-2h8v2zm0-4H8v-2h8v2zm-3-5V3.5L18.5 9H13z" /></svg>
                                            <div className="media-body">
                                                <h5 className="my-0">Import Data</h5>
                                                <p className="mb-0">{this.state.data.length} Employees ID</p>
                                            </div>
                                        </div>
                                        <div className="mt-2 ml-1 upload-btn-wrapper my-2">
                                            <Button variant="outline-primary" onClick={this.uploadFileemp} size="sm" className="border float-right">
                                                Import file
                                            </Button>
                                            <input type="file" name="myfile" id="fileemp" onChange={this.fileHandler.bind(this)} accept={".xlsx, .csv"} />
                                        </div>
                                    </div>
                                    {
                                        this.state.isShow ? (
                                            <>
                                                <div className="mb-4">
                                                    <ProgressBar animated variant={this.state.progress_status} now={this.state.progress_count} label={`${this.state.progress_count}%`} />
                                                </div>

                                            </>
                                        ) : null
                                    }
                                </div>
                            </div>
                            <div className="d-flex flex-row-reverse" style={{ marginTop: 20, marginBottom: 20 }}>
                                {/* <Link to="/manage-create-app">
                                    
                                    <button type="button" className="btn btn-primary"> Confirm</button>
                                </Link> */}
                                {data.length > 0 ?
                                    // this.props.location.state.back === '/manage-create-app/'
                                    // <Link to={{
                                    //     pathname: this.state.linkBack,
                                    //     state: {
                                    //         emp: (this.state.linkBack === '/manage-create-app/') ? data : (this.state.file === true) ? data : [],
                                    //         role: this.props.location.state.role,
                                    //         name: this.props.location.state.name,
                                    //         display_url: this.props.location.state.display_url,
                                    //         num_row_emp: this.state.num_row_emp,
                                    //     }
                                    // }}>
                                    <Button
                                        // variant="outline-primary"
                                        // onClick={this.gotoEdit.bind(this,)}
                                        // size="sm"
                                        className="border float-right btn btn-primary"
                                        onClick={this.addUserByMultiple}
                                    >
                                        Confirm
                                    </Button>
                                    // </Link>
                                    : ""}

                            </div>
                        </div>
                        <div className="col-md-4 col-lg-4">
                            <div className="card">
                                <div className="card-body">
                                    API
                                    <h5>{role}</h5>
                                    <p className="text-secondary">Total Employees</p>
                                    <div className="media">
                                        <img src={usergroup} className="mt-1 mr-1" width="18" height="18" alt="..." />
                                        {/* <svg className="mt-1" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M6 8c1.11 0 2-.9 2-2s-.89-2-2-2c-1.1 0-2 .9-2 2s.9 2 2 2zm6 0c1.11 0 2-.9 2-2s-.89-2-2-2c-1.11 0-2 .9-2 2s.9 2 2 2zM6 9.2c-1.67 0-5 .83-5 2.5V13h10v-1.3c0-1.67-3.33-2.5-5-2.5zm6 0c-.25 0-.54.02-.84.06.79.6 1.34 1.4 1.34 2.44V13H17v-1.3c0-1.67-3.33-2.5-5-2.5z" /></svg> */}
                                        <div className="media-body">
                                            <p className="mt-0 text-secondary">{this.state.num_row_emp + this.state.data.length} Employees</p>
                                        </div>
                                    </div>
                                    {/* <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                                        <button type="button" className="btn btn-primary" onClick={this.onConfirm}> Confirm</button>
                                    </div> */}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

