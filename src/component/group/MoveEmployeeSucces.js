import React, { Component } from 'react'
import { Col, Row, Button, Modal } from 'react-bootstrap'
import Icon from '@material-ui/core/Icon'

export default class MoveEmployeeSucces extends Component {
    constructor(props) {
        super(props);
        this.state = {
            IsDone: false
        };
    }
    
    handleDoneShow = () => {
        this.setState({ IsDone: true });
    }
    handleClose = () => {
        this.setState({ IsDone: false });
    }
    render() {
        return (
            <>
            <div className="d-flex justify-content-center" onClick={this.handleDoneShow}  >
                <Button variant="primary">Confirm</Button>
            </div>
            <Modal show={this.state.IsDone} onHide={this.handleClose}>
                {/* <Modal.Header closeButton>
                <Modal.Title>Modal heading</Modal.Title>
            </Modal.Header> */}
                <Modal.Body className="p-5">
                    <Row>
                        <Col className="d-flex justify-content-center mt-3 mb-2">
                            <div className="p-2 bd-highlight bg-primary rounded-circle">
                                <Icon style={{ "fontWeight": "700", fontSize: "3.875rem", color: "#fff" }}>done</Icon>
                            </div>

                            {/* <Button variant="primary">Done</Button> */}
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-flex justify-content-center">
                            <p>Move Pattraporn to groups <span className="text-primary">Developer</span> Success</p>
                            {/* <Button variant="primary">Done</Button> */}
                        </Col>
                    </Row>
                    <Row>
                        <Col className="d-flex justify-content-center">
                            {/* <p>Move Pattraporn to groups <span className="text-primary">Developer</span> Success</p> */}
                            <Button variant="primary" onClick={this.handleClose}>Done</Button>
                        </Col>
                    </Row>

                </Modal.Body>
            </Modal>
            </>
        )
    }
}
