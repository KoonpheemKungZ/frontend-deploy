import React, { Component } from 'react'

import Icon from '@material-ui/core/Icon'
import { Button } from 'react-bootstrap'
// import { NULL } from 'node-sass'
import usergroup from '../../images/user-group.svg';

import ReactExport from "react-export-excel";

// import ConfirmEmp from './ConfirmRemove'

import { GroupService } from '../../services/GroupService'
import Swal from 'sweetalert2'
import { Link } from 'react-router-dom'


const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
export default class AddMember extends Component {
    constructor(props) {
        super(props)
        this.state = {
            row_emp: [],
            tmp_emp: [],
            num_row_emp: 0,
            apps: [],
            reference_role_resource_id: undefined,
            role: undefined,
            backPath:'',
            search_key:'',
        }
        this.handleConfirmEmpClick = this.handleConfirmEmpClick.bind(this);
    }
    componentDidMount() {
        // const { params } = this.props
        // this.setState({
        //     row_emp: params.row_emp
        // });
        // this.setState({
        //     num_row_emp: params.num_row_emp
        // });
        if (this.props.location.state !== undefined) {
            this.setState({backPath: this.props.location.state.back})
            if (this.props.location.state.back === '/manage-create-app/') {
                this.setState({
                    apps: this.props.location.state.app,
                    reference_role_resource_id: this.props.location.state.reference_role_resource_id,
                    row_emp: this.props.location.state.row_emp,
                    num_row_emp: this.props.location.state.num_row_emp
                });
            }else{
                this.getSearchUser()
                this.getNumberUser()
            }
            
            this.setState({
                apps: this.props.location.state.app,
                reference_role_resource_id: this.props.location.state.reference_role_resource_id,
                role: this.props.location.state.role,
            });
        }

    }
    getSearchUser() {
        const { app, reference_role_resource_id } = this.props.location.state
        // const params = {
        //     "work": "ADMIN_SEARCH_USER_IN_ROLE",
        //     "search_value": "",
        //     "application_id": app._id,
        //     "reference_role_resource_id": reference_role_resource_id,
        //     "item_per_request": 100,
        //     "current_item_amount": 0
        // }

        const params = {
            "work": "ADMIN_GET_ALL_USERS_IN_ROLE_BY_ROLE_RESOURCE_ID",
            "application_id": app._id,
            "reference_role_resource_id": reference_role_resource_id,
        }

        GroupService.requestAPI(params).then(res => {
            if (res.data.status === true) {
                if (res.data.result !== undefined) {
                    this.setState({ row_emp: res.data.result })
                }

            }
        }).catch(err => {
            console.log(err)
        })
    }
    getNumberUser() {
        const { app, reference_role_resource_id } = this.props.location.state
        let params = {
            "work": "ADMIN_GET_NUMBER_OF_USERS_IN_ROLE",
            "application_id": app._id,
            "reference_role_resource_id": reference_role_resource_id,
        }
        GroupService.requestAPI(params).then(res => {
            if (res.data.status === true) {
                this.setState({ num_row_emp: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
    }
    handleConfirmEmpClick(page) {
        const { row_emp } = this.state
        const array = row_emp
        const index = array.indexOf(page);
        if (index > -1) {
            array.splice(index, 1);
        }
        this.setState({
            row_emp: array
        });
        this.setState({
            num_row_emp: array.length
        });
        this.handleRemoveEmpClick(page);
        // this.props.handleConfirmEmpClick(array) 
        // this.props.goBack(0)   
    }
    handleRemoveEmpClick(param) {
        let params = {
            "work": "ADMIN_REMOVE_USER_FROM_ROLE",
            "employee_id": param.employee_id,
            "resource_id": param.resource_id,
            "application_id": param.application_id,
            "reference_role_resource_id": param.reference_role_resource_id
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                // this.getNUMBER_OF_USERS_IN_ROLE(param.application_id,param.reference_role_resource_id)
            }
        }).catch(err => {
            console.log(err)
        })

    }
    handleConfirmAllClick = () => {
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันลบข้อมูลพนักงานทั้งหมด?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                const { apps, reference_role_resource_id } = this.state
                let param = {
                    "work": "ADMIN_REMOVE_ALL_USERS_FROM_ROLE",
                    "application_id": apps._id,
                    "reference_role_resource_id": reference_role_resource_id

                }
                GroupService.requestAPI(param).then(res => {

                    if (res.status === 200) {
                        if (res.data.status === true) {
                            this.getNumberUser()
                            this.setState({
                                row_emp: []
                            });
                        }
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
        })
    }

    searchChangeHandler = (search_member) => {
        // const param = {
        //     work: "ADMIN_DYNAMIC_SEARCH_USER",
        //     user_id: "5cff95c2ab4eee1afc097367",
        //     language: "en",
        //     without_group: {
        //         full_name: search_member,
        //         item_amount: 0,
        //         item_per_request: 100
        //     }
        // }

        // GroupService.requestAPI(param).then(res => {
        //     if (res.status === 200) {
        //         if (res.data.result !== undefined)
        //             this.setState({ row_emp: res.data.result })
        //         else
        //             this.setState({ row_emp: [] })
        //     }
        // }).catch(err => {
        //     console.log(err)
        //     this.setState({ persons: [] })
        // })
        if(search_member.length === 0){
            this.setState({ search_key: search_member });
            this.setState({ tmp_emp: [] });
        }else{
            this.setState({ search_key: search_member });
            const datas = this.state.row_emp.filter((e) => {
                return e.employee_id.includes(search_member)|| e.first_name.includes(search_member) || e.last_name.includes(search_member)
            })
            this.setState({ tmp_emp: datas });
        }
        
    }

    uploadFileemp = () => {
        document.getElementById("fileemp").click()
    }

    removePeople(e) {
        const { search_key } = this.state
        var array = search_key.length === 0 ? [...this.state.row_emp] : [...this.state.tmp_emp]// make a separate copy of the array
        var index = array.indexOf(e)
        if (index !== -1) {
          array.splice(index, 1);
          search_key.length === 0 ? this.setState({row_emp: array}) : this.setState({tmp_emp: array})
        }
    }

    deleteUser =(user)=>{
        console.log(user)
         const { app, reference_role_resource_id } = this.props.location.state
         Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันลบข้อมูลพนักงาน?",
            icon: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                let params = {
                    "work": "ADMIN_REMOVE_USER_FROM_ROLE",
                    "employee_id": user.employee_id,
                    "resource_id": user.resource_id,
                    "application_id": app._id,
                    "reference_role_resource_id": reference_role_resource_id
                }
                GroupService.requestAPI(params).then(res => {
                    if (res.status === 200) {
                        this.removePeople(user)
                        this.getNumberUser()
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
        })
         
    }

    renderPerson() {
        const { row_emp, tmp_emp, search_key } = this.state
        var arr = []
        if(search_key.length === 0){
            for (let index = 0; index < row_emp.length; index++) {
                const user = row_emp;
                if(index >= 100){
                    break
                }
                // const { user } = element
                arr.push(
                    <li key={index} className="list-group-item border-0 px-0">
                        <div className="d-flex justify-content-between border-bottom pb-2">
                            <div>
                                <h5 className="mb-0">{(user[index].first_name === undefined) ? '' : user[index].first_name} {(user[index].last_name === undefined) ? '' : user[index].last_name}</h5>
                                <p className="mb-1">{user[index].employee_id}</p>
                            </div>
                            <Button  variant="outline-primary" onClick={this.deleteUser.bind(this, user[index])} className="border float-right">Delete</Button>
                            {/* <div>
                                <ConfirmEmp user={user[index]} Confirm={this.handleConfirmEmpClick.bind(this, user[index])}></ConfirmEmp>
                            </div> */}
                        </div>
                    </li>
    
                )
            }
        }else{
            for (let index = 0; index < tmp_emp.length; index++) {
                const user = tmp_emp;
                if(index >= 100){
                    break
                }
                // const { user } = element
                arr.push(
                    <li key={index} className="list-group-item border-0 px-0">
                        <div className="d-flex justify-content-between border-bottom pb-2">
                            <div>
                                <h5 className="mb-0">{(user[index].first_name === undefined) ? '' : user[index].first_name} {(user[index].last_name === undefined) ? '' : user[index].last_name}</h5>
                                <p className="mb-1">{user[index].employee_id}</p>
                            </div>
                            <Button  variant="outline-primary" onClick={this.deleteUser.bind(this, user[index])} className="border float-right">Delete</Button>
                            {/* <div>
                                <ConfirmEmp user={user[index]} Confirm={this.handleConfirmEmpClick.bind(this, user[index])}></ConfirmEmp>
                            </div> */}
                        </div>
                    </li>
    
                )
            }
        }
        
        return arr
    }
    render() {
        const { row_emp, num_row_emp, role, backPath } = this.state
        // console.log(row_emp)
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Button variant="link" className="pl-0">
                                <Link
                                    to={backPath}
                                >
                                    <p className="text-muted pl-0 m-0" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p>
                                </Link>
                            </Button >
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">All Employees</h4>

                        </div>
                        <div>
                            {/* <Button variant="outline-secondary">Add Manual</Button> */}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-lg-8">
                            <div className="card mb-2">
                                <div className="card-body" >
                                    <div className="input-group mb-2 mr-3" style={{ marginTop: 20, marginBottom: 30 }}>
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-white"><Icon>search</Icon></div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Search Members" 
                                        onChange={e => this.setState({ search_key: e.target.value }, this.searchChangeHandler(e.target.value))}/>
                                    </div>
                                    <ul className="list-group">
                                        {this.renderPerson()}
                                    </ul>
                                </div>
                            </div>
                            {/* <div className="card">
                                <div className="card-body" >
                                    <div className="d-flex justify-content-between">
                                        <div className="media">
                                            <svg className="mt-2 mr-3" xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path d="M14 2H6c-1.1 0-1.99.9-1.99 2L4 20c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm2 16H8v-2h8v2zm0-4H8v-2h8v2zm-3-5V3.5L18.5 9H13z" /></svg>
                                            <div className="media-body">
                                                <h5 className="my-0">Import Data</h5>
                                                <p className="mb-0">12,000 Employees ID</p>
                                            </div>
                                        </div>
                                        <div className="mt-2 ml-1 upload-btn-wrapper my-2">
                                            <Button variant="outline-primary" onClick={this.uploadFileemp} size="sm" className="border float-right">
                                                Import file
                                            </Button>
                                            <input type="file" name="myfile" id="fileemp" onChange={this.fileHandler.bind(this)} accept={".xlsx, .csv"} />
                                        </div>
                                    </div>
                                    {
                                        this.state.isShow ? (
                                            <>
                                                <div className="mb-4">
                                                    <ProgressBar animated variant={this.state.progress_status} now={this.state.progress_count} label={`${this.state.progress_count}%`} />
                                                </div>

                                            </>
                                        ) : null
                                    }
                                </div>
                            </div> */}
                            <div className="d-flex flex-row-reverse" style={{ marginTop: 20, marginBottom: 20 }}>
                                {/* <button type="button" className="btn btn-primary"> Confirm</button> */}

                                {/* <Button onClick={(e) => this.props.handleConfirmEmpClick(row_emp)}

                                    className="border float-right btn btn-primary"

                                >
                                    Save
                                </Button> */}
                            </div>
                        </div>
                        <div className="col-md-4 col-lg-4">
                            <div className="card">
                                <div className="card-body">
                                    API
                                    <h5>{role}</h5>
                                    <p className="text-secondary">Total Employees</p>
                                    <div className="media">
                                        <img src={usergroup} className="mt-1 mr-1" width="18" height="18" alt="..." />
                                        <div className="media-body">
                                            <p className="mt-0 text-secondary">{num_row_emp} Employees</p>
                                        </div>
                                    </div>
                                    <ExcelFile element={<Button
                                        variant="outline-primary"
                                        className="border btn-block"
                                    >
                                        <svg className="mr-1" width="16" height="16" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                                            viewBox="0 0 512 512">
                                            <g>
                                                <g>
                                                    <polygon points="445.217,378.435 445.217,445.217 66.783,445.217 66.783,378.435 0,378.435 0,512 512,512 512,378.435 		" />
                                                </g>
                                            </g>
                                            <g>
                                                <g>
                                                    <polygon points="301.078,239.17 301.078,0 210.922,0 210.922,239.17 121.322,239.17 256,373.848 390.678,239.17 		" />
                                                </g>
                                            </g>
                                        </svg>
                                        Export file
                                    </Button>}>
                                        <ExcelSheet data={row_emp} name="employee">
                                            <ExcelColumn label="employee_id" value="employee_id" />
                                            <ExcelColumn label="first_name" value="first_name" />
                                            <ExcelColumn label="last_name" value="last_name" />
                                        </ExcelSheet>
                                    </ExcelFile>
                                </div>
                                <div className="card-body">
                                    <Button
                                        variant="outline-primary"
                                        onClick={this.handleConfirmAllClick.bind()}
                                        // size="sm" 
                                        className="border btn-block"
                                    >
                                        Delete All
                                    </Button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-8 col-lg-8">
                        <Link to={{
                                        pathname: this.props.location.state.back,
                                        state: {
                                            role: this.props.location.state.role,
                                            name: this.props.location.state.name,
                                            display_url: this.props.location.state.display_url,
                                            num_row_emp: this.state.num_row_emp,   
                                            new_row_emp: []
                                        }
                                    }}>
                                <Button
                                    className="border float-right btn btn-primary"
                                >
                                    Save
                                </Button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

