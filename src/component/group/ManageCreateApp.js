import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { Form, Media, Button } from 'react-bootstrap'
import { history } from '../../store/history'
import Swal from 'sweetalert2'
import moment from 'moment'
import { GroupService } from '../../services/GroupService'
import { Col } from 'react-bootstrap'
import { trackPromise } from 'react-promise-tracker'

import defaultimage from '../../images/default-image.jpg';

import LoadingIndicator from '../LoadingIndicator'



import ColorPicker from '../ColorPicker'


export default class ManageCreateApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            display_url: '',
            name: '',
            icon_url: '',
            image: '',
            app_type: '',
            app_types: [],
            is_api_role: true,
            is_api_role_name: 'No',
            api_role: '',
            role: '',
            bgcolor: "#fff",
            txtcolor: "#fff",
            bar_style: 'dark-content',
            row_emp: [],
            progress_count: 0,
            isShow: false,
            num_row_emp: 0,
            progress_status: 'success',
            add_empoyee: false,
            pageNum: 0,
            vender_id: null,
        }
        this.handleAddEmpClick = this.handleAddEmpClick.bind(this);
        this.handleViewEmpClick = this.handleViewEmpClick.bind(this);
        this.handleConfirmEmpClick = this.handleConfirmEmpClick.bind(this);
    }
    handleAddEmpClick(page) {
        history.push(
            {
                pathname:'/add-empoyee',
                state: {
                    name: this.state.name,
                    display_url: this.state.display_url,
                    app: this.state.app,
                    reference_role_resource_id: this.state.row_emp,
                    role: this.state.is_api_role_name,
                    back: this.props.location.pathname
                }
            }
            )
        this.setState({
            pageNum: page
        });
    }
    handleViewEmpClick() {
        this.setState({
            pageNum: 2
        });
    }
    handleConfirmEmpClick(employee) {
        const { row_emp } = this.state
        if (row_emp.length === 0) {
            this.setState({
                row_emp: employee
            });
        } else {
            var array = row_emp;
            for (var i = 0; i < employee.length; i++) {
                console.log(employee[i])
                array.push(employee[i]);
                // super_array.push(sub_array.slice(0));
            }
            this.setState({
                row_emp: array
            });
        }        
    }
    handleRemoveEmpClick(param) {
        let params = {
            "work": "ADMIN_REMOVE_USER_FROM_ROLE",
            "employee_id": param.employee_id,
            "resource_id": param.resource_id,
            "application_id": param.application_id,
            "reference_role_resource_id": param.reference_role_resource_id
        }
        GroupService.requestAPI(params).then(res => {

            if (res.status === 200) {
            }
        }).catch(err => {
            console.log(err)
        })

    }

    componentDidMount() {
        let params = {
            "work": "GET_APPLICATION_TYPES"
        }
        if (this.props.location.state !== undefined) {
            if(this.props.location.state.app !== undefined){
                this.setState({
                    app: this.props.location.state.app
                });
            }
            if(this.props.location.state.role !== undefined){
                this.setState({
                    is_api_role_name: this.props.location.state.role,
                });
            }
            if(this.props.location.state.reference_role_resource_id !== undefined){
                this.setState({
                    row_emp: this.props.location.state.reference_role_resource_id
                });
            }
            if(this.props.location.state.display_url !== undefined){
                this.setState({
                    display_url: this.props.location.state.display_url,
                });
            }
            if(this.props.location.state.name !== undefined){
                this.setState({
                    name: this.props.location.state.name,
                });
            }
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                if (res.data.status === true) {
                    this.setState({ app_types: res.data.result })
                    this.setState({ app_type: res.data.result[0]._id })
                }
            }
        }).catch(err => {
            console.log(err)
        })
        // if (this.props.location.state !== undefined) {
        //     console.log(this.props.location.state)
        //     this.setState({
        //         apps: this.props.location.state.app,
        //         reference_role_resource_id: this.props.location.state.reference_role_resource_id,
        //         // row_emp: this.props.location.state.emp,
        //         is_api_role_name: this.props.location.state.role,
        //         name: this.props.location.state.name,
        //         display_url: this.props.location.state.display_url,
        //         num_row_emp: this.props.location.state.num_row_emp,
        //         row_emp: this.props.location.state.emp
        //     });
        //     this.handleConfirmEmpClick(this.props.location.state.emp)
        // }

        //this.setState({selected_apps:JSON.parse(sessionStorage.getItem('default_apps'))})
    }

    uploadFileImg = () => {
        document.getElementById("fileimg").click()
    }
    uploadImage = async (base64Data) => {
        var m = moment().utcOffset("+07:00");
        var refer = m.format("YYYYMMDDx");
        const extFilename = base64Data.split(';')[0].split('/')[1];

        const work = {
            work: "RECEIVE_STRING_BASE64_FILE",
            category: "news",
            file_name: `${refer}.${extFilename}`,
            base64: base64Data
        }


        let res = await GroupService.uploadImage(work)
        const { status, result: { image_path } } = res.data
        return status ? `${image_path}` : ''
    }

    onConfirm = async () => {
        const { app_type, name, icon_url, image, display_url, bgcolor, txtcolor, bar_style, row_emp, is_api_role_name, role, vender_id } = this.state

        if (name === '') {
            Swal.fire({
                position: 'top',
                type: 'warning',
                title: 'กรุณาระบุชื่อแอพพลิเคชั่น',
                showConfirmButton: false,
                timer: 2000
            })
        } else if (display_url === '') {
            Swal.fire({
                position: 'top',
                type: 'warning',
                title: 'กรุณาระบุ url แอพพลิเคชั่น',
                showConfirmButton: false,
                timer: 2000
            })
        } else if (icon_url === '') {
            Swal.fire({
                position: 'top',
                type: 'warning',
                title: 'กรุณาเลือกไอคอนแอพพลิเคชั่น',
                showConfirmButton: false,
                timer: 2000
            })
        } else if (app_type === '') {
            Swal.fire({
                position: 'top',
                type: 'warning',
                title: 'กรุณาเลือกประเภทแอพพลิเคชั่น',
                showConfirmButton: false,
                timer: 2000
            })
        } else if ((is_api_role_name === 'API (IT)') && (role === '')) {

            Swal.fire({
                position: 'top',
                type: 'warning',
                title: 'กรุณาระบุชื่อกฎ',
                showConfirmButton: false,
                timer: 2000
            })
        }else {

            let img_url = ''
            if (icon_url !== '') {
                img_url = await this.uploadImage(image);
            }

        let roleTmp = null
        if (is_api_role_name === 'API (IT)') {
            roleTmp = {
                "name": role,
                // ถ้าระบุ Role เป็นของ Think หรือ asThink = true
                // ให้เพิ่ม field: resources พร้อมกับ employee_id object
                // อย่างน้อย 1 id
                "asThink": false,
                "reference_resource_id": null

            }
        } else if (is_api_role_name === 'API (APP)') {
            roleTmp = {
                "name": role,
                // ถ้าระบุ Role เป็นของ Think หรือ asThink = true
                // ให้เพิ่ม field: resources พร้อมกับ employee_id object
                // อย่างน้อย 1 id

                //  UPDATE: 07 07 2564
                // role.asThink = true และ กรณีเป็นการเพิ่ม 
                // User เข้า Role ที่มีอยู่แล้ว
                // ให้ระบุ reference_resource_id ที่ถูกแนบมาใน 
                // <application-object>.role.reference_resource_id
                // ตาม field ด้านล่าง กรณีเปลี่ยน Role เป็น Type อื่นให้ใส่
                // reference_resource_id = null/undefined
                "asThink": true,
                "resources": row_emp,
            }
        }

            const param =
            // {
            //     "work": "ADMIN_CREATE_NEW_APP_V2",
            //     "application_icon_url": img_url,
            //     "application_display_url": display_url,
            //     "application_name": name,
            //     "application_type_id": app_type,
            //     "role": is_api_role ? {
            //         "name": api_role
            //     } : null,
            //     "toolbar": txtcolor === '' && bgcolor === '' && bar_style === '' ? null : {
            //         "textColor": txtcolor,
            //         "backgroundColor": bgcolor,
            //         "barStyle": bar_style
            //     }
            // }
            {
                "work": "ADMIN_CREATE_NEW_APP_V4",
                "application_icon_url": img_url,
                "application_display_url": display_url,
                "application_name": name,
                "application_type_id": app_type,
                "role": roleTmp,
                "vender_id": vender_id,
                "toolbar": txtcolor === '' && bgcolor === '' && bar_style === '' ? null : {
                    "textColor": txtcolor,
                    "backgroundColor": bgcolor,
                    "barStyle": bar_style
                }
            }
            this.btn.setAttribute("disabled", "disabled");
            console.log(param)
            trackPromise(
                GroupService.requestAPI(param).then(res => {
                    this.btn.removeAttribute("disabled");
                    if (res.status === 200) {
                        if (res.data.status === true) {
                            history.push('/manage-app')
                            Swal.fire({
                                position: 'top',
                                type: 'success',
                                title: 'สร้างแอพเรียบร้อย',
                                showConfirmButton: false,
                                timer: 2000
                            })
                        }
                    }
                })
            )
        }
    }

    handleImage = (e) => {
        var value = e.target.files
        // const name = e.target.name
        var reader = new FileReader();
        reader.readAsDataURL(value[0]);

        if (e.target.type === 'file') {
            var img = document.createElement("img");
            img.src = URL.createObjectURL(value[0])
            const scope = this
            img.onload = function () {
                if (this.width !== 125 || this.height !== 125) {
                    Swal.fire({
                        position: 'top',
                        type: 'warning',
                        title: 'โปรดเลือกรูปไอคอนขนาด 125x125 พิกเซล',
                        showConfirmButton: false,
                        timer: 3000
                    })
                } else {
                    scope.setState({
                        icon_url: URL.createObjectURL(value[0])
                    })
                    scope.setState({ image: reader.result })
                }
            }
        }
    }

    onSelectAppType = (e) => {
        this.setState({ app_type: e.target.value })
    }

    onChangeIsRoleApi = (e) => {
        this.setState({ is_api_role: e.target.checked })
        // if(e.target.value === true){

        // }
    }
    onChangeTextComplete = (color) => {
        this.setState({ txtcolor: color })
        // setDisplayBgColorPicker(true)
    };
    onChangeBgComplete = (color) => {
        this.setState({ bgcolor: color })
    };

    onChangeBarStyle = (e) => {
        if (e.target.id === 'radio1') {
            this.setState({ bar_style: 'dark-content' })
        } else if (e.target.id === 'radio2') {
            this.setState({ bar_style: 'light-content' })
        }

    }

    render() {
        const { icon_url, app_types, bgcolor, txtcolor, is_api_role_name } = this.state
        return (
            <>
                <div className="row justify-content-md-center">
                    <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                        <div className="d-flex bd-highlight mt-2">
                            <div className="mr-auto p-0 bd-highlight py-3">
                                <Link to="/manage-app"><p className="text-muted mb-1" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                                <h4 className="h4">Create New App</h4>
                            </div>
                            <div className="p-0 flex-shrink-1 bd-highlight pt-4">

                            </div>
                        </div>
                        <div className="card p-3">
                            <div className="card-body" >
                                <Form>
                                    <Form.Group controlId="Application">
                                        <Form.Label className="font-weight-bold">Application name <span className="text-danger">*</span></Form.Label>
                                        <Form.Control type="text" placeholder="ระบุชื่อแอพ" onChange={e => this.setState({ name: e.target.value })} value={this.state.name} />
                                    </Form.Group>
                                    <Form.Group controlId="Display">
                                        <Form.Label className="font-weight-bold">App URL<span className="text-danger">*</span></Form.Label>
                                        <Form.Control type="text" placeholder="URL Webview" onChange={e => this.setState({ display_url: e.target.value })} value={this.state.display_url} />
                                    </Form.Group>
                                    <Form.Group controlId="APIRole">
                                        <Form.Label className="font-weight-bold">API Role</Form.Label><br />
                                        <div className="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline1" name="customRadioInline" className="custom-control-input" onChange={e => this.setState({ is_api_role_name: 'No' })} checked={is_api_role_name === "No"} />
                                            <label className="custom-control-label" htmlFor="customRadioInline1">No</label>
                                        </div>
                                        <div className="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline3" name="customRadioInline" className="custom-control-input" onChange={e => this.setState({ is_api_role_name: 'API (APP)' })} checked={is_api_role_name === "API (APP)"} />
                                            <label className="custom-control-label" htmlFor="customRadioInline3">API (APP)</label>
                                        </div>
                                        <div className="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="customRadioInline" className="custom-control-input" onChange={e => this.setState({ is_api_role_name: 'API (IT)' })} checked={is_api_role_name === "API (IT)"} />
                                            <label className="custom-control-label" htmlFor="customRadioInline2">API (IT)</label>
                                        </div>
                                    </Form.Group>
                                    {(is_api_role_name === "API (IT)") ? (

                                        <Form.Group controlId="Application">
                                            <Form.Label className="font-weight-bold">Role name <span className="text-danger">*</span></Form.Label>
                                            <Form.Control type="text" placeholder="Name" onChange={e => this.setState({ role: e.target.value })} value={this.state.role} />
                                        </Form.Group>
                                    ) : null
                                    }

                                    {(is_api_role_name === "API (APP)") ? (
                                        <Form.Group controlId="Application">
                                            <Form.Label className="font-weight-bold">Empoyee lists</Form.Label>
                                            <div className="d-flex justify-content-between border-bottom">
                                                {/* <p class="lead float-left">h6. Bootstrap heading</p> */}
                                                <p variant="text-black-50" className="mb-0">{(this.state.row_emp.length > 0) ? this.state.row_emp.length + " empoyee ID" : "ยังไม่มีรายชื่อ"}  {(this.state.num_row_emp) ?
                                                    <Link to={{
                                                        pathname: "/all-empoyee",
                                                        state: { 
                                                            app: this.state.app, 
                                                            reference_role_resource_id: this.state.reference_resource_id, 
                                                            role: this.state.is_api_role_name,
                                                            row_emp: this.state.row_emp, 
                                                            num_row_emp: this.state.num_row_emp,
                                                            back: this.props.location.pathname
                                                        }
                                                    }}>
                                                        <Button variant="link"
                                                        // onClick={this.handleViewEmpClick}
                                                        >View  all→</Button>
                                                    </Link> : ""}

                                                </p >
                                                <div className="mt-2 ml-1 upload-btn-wrapper my-2">
                                                    {/* <Button
                                                            variant="outline-primary"
                                                            onClick={e => this.handleAddEmpClick(1)}
                                                            size="sm"
                                                            className="border float-right"
                                                        >
                                                            + Add Empoyee ID
                                                        </Button> */}
                                                    {/* <Link to={{
                                                        pathname: "/add-empoyee",
                                                        
                                                    }}> */}
                                                        <Button
                                                            variant="outline-primary"
                                                            onClick={this.handleAddEmpClick}
                                                            size="sm"
                                                            className="border float-right"
                                                        >
                                                            + Add Empoyee ID
                                                        </Button>

                                                    {/* </Link> */}
                                                </div>
                                            </div>
                                        </Form.Group>
                                    ) : null}
                                    <Form.Group controlId="formBasicBirthday">
                                        <Form.Label className="font-weight-bold">App Type<span className="text-danger">*</span></Form.Label>
                                        <Form.Control value={this.state.app_type} as="select" onChange={this.onSelectAppType} required>
                                            {
                                                app_types.map((value) => {
                                                    return <option value={value._id} key={value._id}>{value.description}</option>
                                                })
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="Application">
                                        <Form.Row>
                                            <Col>
                                                <Form.Label className="font-weight-bold">App icon<span className="text-danger">*</span></Form.Label>
                                                <Media.Body>
                                                    <div className="clearfix">
                                                        <img
                                                            width={125}
                                                            height={120}
                                                            className="mr-3 rounded-circle float-left"
                                                            src={(icon_url) ? icon_url : defaultimage}
                                                            alt=''
                                                        />
                                                        {/* < br /> */}
                                                        <p className="float-left">ขนาดรูป<br />125px * 125px</p>
                                                    </div>
                                                    <div className="mt-2 ml-1 upload-btn-wrapper">
                                                        <button type="button" onClick={this.uploadFileImg} className="btn btn-outline-dark">Upload Icon</button>
                                                        <input type="file" accept='image/*' ref="fileUploader" id="fileimg" name="myfile" onChange={this.handleImage} />
                                                    </div>
                                                </Media.Body>
                                            </Col>
                                            <Col>
                                                {/* <Form.Group controlId="formBasicCheckbox">
                                                    <Form.Check className="font-weight-bold" type="checkbox" label="API Role :" onChange={this.onChangeIsRoleApi} />
                                                    <Form.Control type="text" placeholder="(Optional)" onChange={e => this.setState({ api_role: e.target.value })} disabled={!this.state.is_api_role} />
                                                </Form.Group>
                                                <Form.Group controlId="Display">
                                                    <Form.Label className="font-weight-bold">BG Color</Form.Label>
                                                    <Form.Control type="text" placeholder="(Optional)" onChange={e => this.setState({ bgcolor: e.target.value })} />
                                                </Form.Group>
                                                <Form.Group controlId="Display">
                                                    <Form.Label className="font-weight-bold">Text Color</Form.Label>
                                                    <Form.Control type="text" placeholder="(Optional)" onChange={e => this.setState({ txtcolor: e.target.value })} />
                                                </Form.Group>
                                                <Form.Group controlId="Display">
                                                    <Form.Label className="font-weight-bold">Status Bar</Form.Label>
                                                    <Form.Check
                                                        type="radio"
                                                        label="dark"
                                                        name="statusbar"
                                                        id="radio1"
                                                        onChange={this.onChangeBarStyle}
                                                    />
                                                    <Form.Check
                                                        type="radio"
                                                        label="light"
                                                        name="statusbar"
                                                        id="radio2"
                                                        onChange={this.onChangeBarStyle}
                                                    />
                                                </Form.Group> */}
                                            </Col>
                                        </Form.Row>
                                    </Form.Group>
                                    <Form.Group as={Col} >
                                        <Form.Label className="font-weight-bold">Custom Header Web View</Form.Label>
                                        <div className="row">
                                            <Form.Group as={Col} controlId="formBGColor">
                                                <div className="d-flex">
                                                    <label htmlFor="title">BG Color</label>

                                                </div>
                                                <div className="input-group mb-3">
                                                    <ColorPicker color={bgcolor} handleChangeComplete={this.onChangeBgComplete} /> {/* placeholder="(Optional)" onChange={e => this.setState({ txtcolor: e.target.value })}  */}
                                                </div>
                                            </Form.Group>
                                            <Form.Group as={Col} controlId="formTextColor">
                                                <div className="d-flex">
                                                    <label htmlFor="formTextColor">Text Color </label>
                                                </div>
                                                <div className="input-group mb-3">
                                                    <ColorPicker color={txtcolor} handleChangeComplete={this.onChangeTextComplete} />
                                                </div>
                                            </Form.Group>
                                            <Form.Group as={Col}>
                                                <div className="d-flex">
                                                    <label htmlFor="barStyle">Status Bar</label>
                                                    <label htmlFor="barStyle" className="text-muted d-inline-flex ml-auto"></label>
                                                </div>

                                                <div className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="radio1" name={'barStyle'} className="custom-control-input"

                                                        onChange={this.onChangeBarStyle}
                                                        defaultChecked
                                                    // checked={(this.state.bar_style === 'dark-content') ? true : false}
                                                    />
                                                    <label className="custom-control-label" htmlFor={'radio1'}>dark</label>
                                                </div>
                                                <div className="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" id="radio2" name={'barStyle'} className="custom-control-input"
                                                        onChange={this.onChangeBarStyle}
                                                    // checked={(this.state.bar_style === 'light-content') ? true : false}
                                                    />
                                                    <label className="custom-control-label" htmlFor={'radio2'}>light</label>
                                                </div>
                                            </Form.Group>
                                        </div>
                                    </Form.Group>

                                </Form>
                            </div>
                        </div>
                        <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                        <button type="button" ref={btn => { this.btn = btn; }} className="btn btn-primary" onClick={this.onConfirm}> Confirm <LoadingIndicator className="float-left" /></button>
                            {/* <button type="button" ref={btn => { this.btn = btn; }} className="btn btn-primary" onClick={this.onConfirm}> Confirm</button> */}
                        </div>
                    </div>
                </div>
            </>

        )
        // }

    }
}
