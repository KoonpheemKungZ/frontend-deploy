import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Icon from '@material-ui/core/Icon'
import { Button, Media } from 'react-bootstrap'
import { history } from '../../store/history'
import Swal from 'sweetalert2'
import moment from 'moment'
import { GroupService } from '../../services/GroupService'
import { Form } from 'react-bootstrap'
import { Col } from 'react-bootstrap'
import { trackPromise } from 'react-promise-tracker'

import LoadingIndicator from '../LoadingIndicator'
import ColorPicker from '../ColorPicker'

export default class ManageSettingApp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            app: {},
            name: '',
            icon_url: '',
            app_url: '',
            app_types: [],
            app_type: '',
            image: '',
            can_delete: false,
            is_api_role_name: 'Allow All',
            api_role: '',
            role: '',
            bgcolor: '',
            txtcolor: '',
            bar_style: '',
            type: undefined,
            row_emp: [],
            num_row_emp: 0,
            new_row_emp: [],
            pageNum: 0,
            reference_resource_id: null,
            vender_id: null,
        }
    }

    uploadFileImg = () => {
        document.getElementById("fileimg").click()
    }

    getNumberUser() {
        const { app, reference_resource_id } = this.state
        let params = {
            "work": "ADMIN_GET_NUMBER_OF_USERS_IN_ROLE",
            "application_id": app._id,
            "reference_role_resource_id": reference_resource_id,
        }
        GroupService.requestAPI(params).then(res => {
            console.log(res)
            if (res.data.status === true) {
                this.setState({ num_row_emp: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
    }
    getApplocationType() {
        let params = {
            "work": "GET_APPLICATION_TYPES"
        }
        GroupService.requestAPI(params).then(res => {
            console.log(res)
            if (res.status === 200) {
                if (res.data.status === true) {
                    this.setState({ app_types: res.data.result })
                }
            }
        }).catch(err => {
            console.log(err)
        })
    }
    // getSearchUser(){
    //     const params = {
    //         "work": "ADMIN_SEARCH_USER_IN_ROLE",
    //         "search_value": "",
    //         "application_id": id,
    //         "reference_role_resource_id": res.data.result.role.reference_resource_id,
    //         "item_per_request": 100,
    //         "current_item_amount": 0
    //     }

    //     GroupService.requestAPI(params).then(res => {
    //         if (res.data.status === true) {
    //             if (res.data.result !== undefined) {
    //                 this.setState({ row_emp: res.data.result })
    //             }

    //         }
    //     }).catch(err => {
    //         console.log(err)
    //     })
    // }

    // Date picker
    onChangeTextComplete = (color) => {
        this.setState({ txtcolor: color })
        // setDisplayBgColorPicker(true)
    };
    onChangeBgComplete = (color) => {
        this.setState({ bgcolor: color })
    };
    componentDidMount() {
        this.requestAppData()
        //this.setState({selected_apps:JSON.parse(sessionStorage.getItem('default_apps'))})
    }

    requestAppData=()=>{
        const { id } = this.props.match.params
        let params = {
            "work": "GET_APPLICATION_BY_APPLICATION_ID",
            "app_version_number": 1000,
            "app_id": id,
            "more_info": true
        }
        GroupService.requestAPI(params).then(res => {
            console.log(res)
            if (res.status === 200) {
                this.setState({ app: res.data.result })
                this.setState({ name: res.data.result.name })
                this.setState({ app_type: res.data.result.app_key })
                this.setState({ icon_url: res.data.result.icon_url })
                this.setState({ can_delete: res.data.result.usage.can_delete })
                if ((res.data.result.type !== undefined) && (res.data.result.type !== 'react_native_app')) {
                    this.setState({ type: res.data.result.type })
                    if (res.data.result.props.display !== null && res.data.result.props.display !== undefined) {
                        this.setState({ app_url: res.data.result.props.display.value })
                    }
                }
                if (res.data.result.props.toolbar !== undefined && res.data.result.props.toolbar !== null) {
                    this.setState({ bgcolor: res.data.result.props.toolbar.backgroundColor })
                    this.setState({ txtcolor: res.data.result.props.toolbar.textColor })
                    this.setState({ bar_style: res.data.result.props.toolbar.barStyle })
                }
                // console.log(res.data.result.role.reference_resource_id)
                if (res.data.result.role !== undefined && res.data.result.role !== null) {
                    // this.setState({ is_api_role: (res.data.result.role.need).toString() })
                    if (res.data.result.role) {
                        if (res.data.result.role.asThink === true) {
                            this.setState({ is_api_role_name: 'API (APP)' })
                        } else {
                            this.setState({ is_api_role_name: 'API (IT)' })
                        }
                    } else {
                        this.setState({ is_api_role_name: 'API (IT)' })
                    }
                    // this.setState({ role: 'API (APP)' })
                    let r = (res.data.result.role.key === null) ? '' : res.data.result.role.key
                    this.setState({ role: r })
                    this.setState({ reference_resource_id: res.data.result.role.reference_resource_id })

                    this.getNumberUser()
                } else {
                    this.setState({ is_api_role_name: 'No' })

                }
                if (res.data.result.vender !== undefined && res.data.result.vender !== null) {
                    console.log(res.data.result.vender)
                    this.setState({ vender_id: res.data.result.vender._id })
                }
            }
        }).catch(err => {
            console.log(err)
        })
        if (this.props.location.state !== undefined) {
            if (this.props.location.state.emp !== undefined) {
                this.setState({
                    // num_row_emp: this.props.location.state.num_row_emp,
                    new_row_emp: this.props.location.state.emp
                });
            }
        }
    }
    // componentDidUpdate(prevProps, prevState) {
    //     // console.log(prevState)
    //     if (prevState.app !== null) {
    //         this.getNumberUser();
    //     }
    // }

    uploadImage = async (base64Data) => {
        var m = moment().utcOffset("+07:00");
        var refer = m.format("YYYYMMDDx");
        const extFilename = base64Data.split(';')[0].split('/')[1];

        const work = {
            work: "RECEIVE_STRING_BASE64_FILE",
            category: "news",
            file_name: `${refer}.${extFilename}`,
            base64: base64Data
        }


        let res = await GroupService.uploadImage(work)
        const { status, result: { image_path } } = res.data
        return status ? `${image_path}` : ''
    }

    onConfirm = async () => {
        const { app, reference_resource_id, name, icon_url, image, app_url, is_api_role_name, bgcolor, txtcolor, bar_style, new_row_emp, role, app_type, vender_id } = this.state

        let img_url = icon_url
        if (icon_url !== app.icon_url) {
            img_url = await this.uploadImage(image);
        }
        let roleTmp = null
        if (is_api_role_name === 'API (IT)') {
            roleTmp = {
                "name": role,
                // ถ้าระบุ Role เป็นของ Think หรือ asThink = true
                // ให้เพิ่ม field: resources พร้อมกับ employee_id object
                // อย่างน้อย 1 id
                "asThink": false,
                "reference_resource_id": null

            }
        } else if (is_api_role_name === 'API (APP)') {
            roleTmp = {
                "name": role,
                // ถ้าระบุ Role เป็นของ Think หรือ asThink = true
                // ให้เพิ่ม field: resources พร้อมกับ employee_id object
                // อย่างน้อย 1 id

                //  UPDATE: 07 07 2564
                // role.asThink = true และ กรณีเป็นการเพิ่ม 
                // User เข้า Role ที่มีอยู่แล้ว
                // ให้ระบุ reference_resource_id ที่ถูกแนบมาใน 
                // <application-object>.role.reference_resource_id
                // ตาม field ด้านล่าง กรณีเปลี่ยน Role เป็น Type อื่นให้ใส่
                // reference_resource_id = null/undefined

                "reference_resource_id": reference_resource_id,
                "asThink": true,
                "resources": new_row_emp,
            }
        }
        // this.btn.current.setAttribute("disabled", "disabled");
        const param =
        {
            "work": "ADMIN_CHANGE_APPLICATION_PROPS_V4",
            "application_id": app._id,
            "application_image_url": img_url,
            "application_name": name,
            "application_display_url": app_url,
            "application_type_id": app_type,
            "role": roleTmp,
            "vender_id": vender_id,
            "toolbar": txtcolor === '' && bgcolor === '' && bar_style === '' ? null : {
                "textColor": txtcolor,
                "backgroundColor": bgcolor,
                "barStyle": bar_style
            }
        }
        trackPromise(
            GroupService.requestAPI(param).then(res => {
                console.log(param)
                if (res.status === 200) {
                    if (res.data.status === true) {
                        this.getNumberUser()
                        this.setState({
                            // num_row_emp: this.props.location.state.num_row_emp,
                            new_row_emp: []
                        });
                        // history.push('/manage-app')
                        Swal.fire({
                            position: 'top',
                            type: 'success',
                            title: 'แก้ไขข้อมูลเรียบร้อย',
                            showConfirmButton: false,
                            timer: 2000
                        })
                        this.requestAppData()
                    } else {
                        // this.btn.removeAttribute("disabled");
                        Swal.fire({
                            position: 'top',
                            type: 'warning',
                            title: res.data.message,
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }
            })
        )
    }

    onDeleteApp = () => {
        Swal.fire({
            title: 'ยืนยัน?',
            text: "ยืนยันการลบแอพพลิเคชั่น?",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'ยืนยัน'
        }).then((result) => {
            if (result.value) {
                const { id } = this.props.match.params
                let params = {
                    "work": "ADMIN_DELETE_APP",
                    "app_version_number": 1000,
                    "application_id": id
                }
                GroupService.requestAPI(params).then(res => {
                    //console.log(res)
                    if (res.status === 200) {
                        if (res.data.status === true) {
                            history.push('/manage-app')
                            Swal.fire({
                                position: 'top',
                                type: 'success',
                                title: 'ลบแอพพลิเคชั่นเรียบร้อย',
                                showConfirmButton: false,
                                timer: 2000
                            })
                        } else {
                            Swal.fire({
                                position: 'top',
                                type: 'warning',
                                title: 'ไม่สามารถลบแอพพลิเคชั่นนี้ได้',
                                showConfirmButton: false,
                                timer: 2000
                            })
                        }
                    }
                }).catch(err => {
                    console.log(err)
                })
            }
        })
    }

    handleImage = (e) => {
        var value = e.target.files
        // const name = e.target.name
        var reader = new FileReader();
        reader.readAsDataURL(value[0]);

        if (e.target.type === 'file') {
            var img = document.createElement("img");
            img.src = URL.createObjectURL(value[0])
            const scope = this
            img.onload = function () {
                if (this.width !== 125 || this.height !== 125) {
                    Swal.fire({
                        position: 'top',
                        type: 'warning',
                        title: 'โปรดเลือกรูปไอคอนขนาด 125x125 พิกเซล',
                        showConfirmButton: false,
                        timer: 3000
                    })
                } else {
                    scope.setState({
                        icon_url: URL.createObjectURL(value[0])
                    })
                    scope.setState({ image: reader.result })
                }
            }
        }
    }

    onChangeIsRoleApi = (e) => {
        this.setState({ is_api_role: e.target.checked })
        // if(e.target.value === true){

        // }
    }

    onChangeBarStyle = (e) => {
        if (e.target.id === 'radio1') {
            this.setState({ bar_style: 'dark-content' })
        } else if (e.target.id === 'radio2') {
            this.setState({ bar_style: 'light-content' })
        }

    }

    render() {
        const { name, icon_url, app_url, bgcolor, txtcolor, is_api_role_name, bar_style, type } = this.state
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-6 ml-sm-10 col-lg-6">
                    {/* <div className='d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center'>

                        <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                            <Link to="/manage-app"><p className="text-muted" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                        </div>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

                        <div>
                            <h4 className="h4">Setting App</h4>
                        </div>
                    </div> */}
                    <div className="d-flex bd-highlight mt-2">
                        <div className="mr-auto p-2 bd-highlight">
                            <Link to="/manage-app"><p className="text-muted mb-1" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>
                            <h4 className="h4">Manage App</h4>
                        </div>
                        <div className="p-0 flex-shrink-1 bd-highlight pt-4">
                            <Button
                                onClick={this.onDeleteApp}
                                variant="outline-secondary"
                                hidden={this.state.can_delete !== true}
                                className="clearfix">
                                <span className="far fa-trash-alt float-left mr-2 pt-1"></span>
                                <span className="float-right">Delete App</span>
                            </Button>
                        </div>
                    </div>
                    <div className="card p-3">
                        {/* <div className="card-header bg-white border-0">
                            <h5>Setting Apps</h5>
                        </div> */}
                        <div className="card-body" >
                            <label>App Name</label>
                            <div className="input-group mb-2 mr-3">
                                <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Name" value={name} onChange={e => this.setState({ name: e.target.value })} />
                            </div>
                            <label hidden={type === undefined || type === 'react_native_app'}>App URL</label>
                            <div className="input-group mb-2 mr-3" hidden={type === undefined || app_url === 'react_native_app'}>
                                <input type="text" className="form-control" id="inlineFormInputGroup1" placeholder="Name" value={app_url} onChange={e => this.setState({ app_url: e.target.value })} />
                            </div>
                            <Form.Group controlId="Application">
                                <Form.Label className="font-weight-bold">API Role</Form.Label><br />
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline1" name="customRadioInline" className="custom-control-input" onChange={e => this.setState({ is_api_role_name: 'No' })} checked={is_api_role_name === "No"} />
                                    <label className="custom-control-label" htmlFor="customRadioInline1">No</label>
                                </div>
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline3" name="customRadioInline" className="custom-control-input" onChange={e => this.setState({ is_api_role_name: 'API (APP)' })} checked={is_api_role_name === "API (APP)"} />
                                    <label className="custom-control-label" htmlFor="customRadioInline3">API (APP)</label>
                                </div>
                                <div className="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadioInline2" name="customRadioInline" className="custom-control-input" onChange={e => this.setState({ is_api_role_name: 'API (IT)' })} checked={is_api_role_name === "API (IT)"} />
                                    <label className="custom-control-label" htmlFor="customRadioInline2">API (IT)</label>
                                </div>
                            </Form.Group>
                            {(is_api_role_name === "API (IT)") ? (

                                <Form.Group controlId="Application">
                                    <Form.Label className="font-weight-bold">Role name <span className="text-danger">*</span></Form.Label>
                                    <Form.Control type="text" placeholder="Name" onChange={e => this.setState({ role: e.target.value })} value={this.state.role} />
                                </Form.Group>
                            ) : null
                            }
                            {(is_api_role_name === "API (APP)") ? (
                                <Form.Group controlId="Application">
                                    <Form.Label className="font-weight-bold">Empoyee lists</Form.Label>
                                    <div className="d-flex justify-content-between border-bottom">
                                        {/* <p class="lead float-left">h6. Bootstrap heading</p> */}
                                        <p variant="text-black-50" className="mb-0">{(this.state.num_row_emp + this.state.new_row_emp.length) ? this.state.num_row_emp + this.state.new_row_emp.length + " empoyee ID" : "ยังไม่มีรายชื่อ"}  {(this.state.num_row_emp) ?
                                            <Link to={{
                                                pathname: "/all-empoyee",
                                                state: { app: this.state.app, reference_role_resource_id: this.state.reference_resource_id, role: this.state.is_api_role_name, back: this.props.location.pathname }
                                            }}>
                                                <Button variant="link"
                                                // onClick={this.handleViewEmpClick}
                                                >View  all→</Button>
                                            </Link> : ""}

                                        </p >
                                        <div className="mt-2 ml-1 upload-btn-wrapper my-2">
                                            {/* <Button 
                                                    variant="outline-primary" 
                                                    onClick={this.gotoAddEmp.bind(this,)} 
                                                    size="sm" 
                                                    className="border float-right"
                                                >
                                                    + Add Empoyee ID
                                                </Button> */}
                                            <Link to={{
                                                pathname: "/add-empoyee",
                                                state: { app: this.state.app, reference_role_resource_id: this.state.reference_resource_id, role: this.state.is_api_role_name, back: this.props.location.pathname }
                                            }}>
                                                <Button
                                                    variant="outline-primary"
                                                    // onClick={this.handleAddEmpClick}
                                                    size="sm"
                                                    className="border float-right"
                                                >
                                                    + Add Empoyee ID
                                                </Button>

                                            </Link>
                                            {/* <input type="file" name="myfile" id="fileemp" onChange={this.fileHandler.bind(this)} accept={".xlsx, .csv"} /> */}
                                        </div>
                                    </div>
                                </Form.Group>
                            ) : null
                            }
                            {/* <Form.Group controlId="formBasicBirthday">
                                    <Form.Label className="font-weight-bold">App Type<span className="text-danger">*</span></Form.Label>
                                    <Form.Control value={this.state.app_type} defaultValue={this.state.app_type} as="select" onChange={this.onSelectAppType} required>
                                        {
                                            app_types.map((value) => {
                                                return <option value={value._id} key={value._id}>{value.description}</option>
                                            })
                                        }
                                    </Form.Control>
                                </Form.Group> */}
                            <Form.Group controlId="AppType">
                                <Form.Label className="font-weight-bold">App Type : {this.state.type}</Form.Label><br />
                            </Form.Group>
                            <Form.Group controlId="Application">
                                <Form.Row>
                                    {/* <Col>
                                        <Media>
                                            <Media.Body>
                                                <img
                                                    width={125}
                                                    // height={64}
                                                    className="mr-3"
                                                    src={(icon_url) ? icon_url : ''}
                                                    alt=''
                                                />
                                                < br />
                                                <div className="upload-btn-wrapper">

                                                    <button onClick={this.handleImage} className="btn btn-outline-dark">เปลี่ยนรูปภาพ</button>
                                                    <input type="file" accept='image/*' ref="fileUploader" name="myfile" onChange={this.handleImage} />
                                                </div>
                                            </Media.Body>
                                            <p>ขนาดรูป<br />
                                    125px * 125px</p>
                                            <Media.Body>

                                            </Media.Body>

                                        </Media>
                                    </Col> */}
                                    <Col>
                                        <Form.Label className="font-weight-bold">App icon<span className="text-danger">*</span></Form.Label>
                                        <Media.Body>
                                            <div className="clearfix">
                                                <img
                                                    width={125}
                                                    height={120}
                                                    className="mr-3 rounded-circle float-left"
                                                    src={(icon_url) ? icon_url : ''}
                                                    alt=''
                                                />
                                                {/* < br /> */}
                                                <p className="float-left">ขนาดรูป<br />125px * 125px</p>
                                            </div>
                                            <div className="mt-2 ml-1 upload-btn-wrapper">
                                                <button type="button" onClick={this.uploadFileImg} className="btn btn-outline-dark">Upload Icon</button>
                                                <input type="file" accept='image/*' ref="fileUploader" id="fileimg" name="myfile" onChange={this.handleImage} />
                                            </div>
                                        </Media.Body>
                                    </Col>
                                    <Col>
                                        {/* <Form.Group controlId="formBasicCheckbox">
                                            <Form.Check className="font-weight-bold" type="checkbox" label="API Role :" checked={is_api_role} onChange={this.onChangeIsRoleApi} />
                                            <Form.Control type="text" placeholder="(Optional)" value={api_role} onChange={e => this.setState({ api_role: e.target.value })} disabled={!this.state.is_api_role} />
                                        </Form.Group>
                                        <Form.Group controlId="Display">
                                            <Form.Label className="font-weight-bold">BG Color</Form.Label>
                                            <Form.Control type="text" placeholder="(Optional)" value={bgcolor} onChange={e => this.setState({ bgcolor: e.target.value })} />
                                        </Form.Group>
                                        <Form.Group controlId="Display">
                                            <Form.Label className="font-weight-bold">Text Color</Form.Label>
                                            <Form.Control type="text" placeholder="(Optional)" value={txtcolor} onChange={e => this.setState({ txtcolor: e.target.value })} />
                                        </Form.Group>
                                        <Form.Group controlId="Display">
                                            <Form.Label className="font-weight-bold">Status Bar</Form.Label>
                                            <Form.Check
                                                type="radio"
                                                label="dark"
                                                name="statusbar"
                                                id="radio1"
                                                checked={bar_style === 'dark-content'}
                                                onChange={this.onChangeBarStyle}
                                            />
                                            <Form.Check
                                                type="radio"
                                                label="light"
                                                name="statusbar"
                                                id="radio2"
                                                checked={bar_style === 'light-content'}
                                                onChange={this.onChangeBarStyle}
                                            />
                                        </Form.Group> */}
                                    </Col>
                                </Form.Row>
                            </Form.Group>
                            <Form.Group as={Col} >
                                <Form.Label className="font-weight-bold">Custom Header Web View</Form.Label>
                                <div className="row">
                                    <Form.Group as={Col} controlId="formBGColor">
                                        <div className="d-flex">
                                            <label htmlFor="title">BG Color</label>

                                        </div>
                                        <div className="input-group mb-3">
                                            <ColorPicker color={bgcolor} handleChangeComplete={this.onChangeBgComplete} /> {/* placeholder="(Optional)" onChange={e => this.setState({ txtcolor: e.target.value })}  */}
                                        </div>
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="formTextColor">
                                        <div className="d-flex">
                                            <label htmlFor="formTextColor">Text Color </label>
                                        </div>
                                        <div className="input-group mb-3">
                                            <ColorPicker color={txtcolor} handleChangeComplete={this.onChangeTextComplete} />
                                        </div>
                                    </Form.Group>
                                    <Form.Group as={Col}>
                                        <div className="d-flex">
                                            <label htmlFor="barStyle">Status Bar</label>
                                            <label htmlFor="barStyle" className="text-muted d-inline-flex ml-auto"></label>
                                        </div>

                                        <div className="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio1" name={'barStyle'} className="custom-control-input" value={'dark-content'}

                                                onChange={this.onChangeBarStyle}
                                                checked={bar_style === 'dark-content'}
                                            />
                                            <label className="custom-control-label" htmlFor={'radio1'}>dark</label>
                                        </div>
                                        <div className="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="radio2" name={'barStyle'} className="custom-control-input" value={'light-content'}
                                                onChange={this.onChangeBarStyle}
                                                checked={bar_style === 'light-content'}
                                            />
                                            <label className="custom-control-label" htmlFor={'radio2'}>light</label>
                                        </div>
                                    </Form.Group>
                                </div>
                            </Form.Group>
                            <Form.Group>
                            </Form.Group>
                        </div>
                    </div>
                    <div className="row justify-content-md-center" style={{ marginTop: 20, marginBottom: 20 }}>
                        <button type="button" ref={btn => { this.btn = btn; }} className="btn btn-primary" onClick={this.onConfirm}> Confirm <LoadingIndicator className="float-left" /></button>
                    </div>
                </div>
            </div>
        )


    }
}
