import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'

function ConfirmRemove(props) {
    // Declare a new state variable, which we'll call "count"
    const user = props.user
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleConfirmEmpClick = ()=> {
            
        handleClose()
        props.Confirm()
    }
    return (
        <>
            <Button onClick={handleShow}
                variant="outline-primary"
                // onClick={this.gotoEdit.bind(this,)} 
                // size="sm" 
                className="border float-right"
            >
                Remove
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Remove Employee ID</Modal.Title>
                </Modal.Header>
                <Modal.Body className="text-center py-5">
                    {/* <div class="d-flex justify-content-center"> */}
                        {/* <div className="text-center"> */}
                            <h5 className="text-danger mb-3">Confirm Remove!</h5>
                            <h6 className="mb-1">{user.first_name} {user.last_name}</h6>
                            <p>{user.employee_id}</p>
                        {/* </div> */}

                    {/* </div> */}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-light" className="border text-dark" onClick={handleClose}>Cancel</Button>
                    <Button variant="primary" onClick={handleConfirmEmpClick}>
                        Confirm
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default ConfirmRemove