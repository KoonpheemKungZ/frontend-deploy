/* eslint-disable array-callback-return */
import React from 'react';
import '../Home.css';
import { Card, Media, Button } from 'react-bootstrap'
// import iconfilter from '../../images/filter-results-button.png';
import Icon from '@material-ui/core/Icon';
import './group.css';
import { GroupService } from '../../services/GroupService';
import AddEditAccess from './AddEditAccess';
import AddMember from './AddMemberModal';
import EditGroupName from './EditGroupName';
import Form from 'react-bootstrap/Form'
import InputGroup from 'react-bootstrap/InputGroup'
import Col from 'react-bootstrap/Col'
import Row from 'react-bootstrap/Row'
import ExportExcel from './ExportExcel';

import { history } from '../../store/history';
// import { CardFooter } from 'react-bootstrap/Card';
// import { NewsService } from '../services/NewsService';

class GroupDetail extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            group: {},
            datasource: [],
            current_page: 1,
            per_page: 20,
            start_item_index: 0,
            max_page: 0,
            all_items: 0,
            status: "",
            status_id: "",
            categories: [],
            search_member: '',
            persons: [],
            category_name: '',
            category_key: '',
            apps: [],
        };
    }

    componentDidMount() {
        this.requestGroups()
        let params = {
            "work": "GET_APPS",
            "app_version_number": 1000
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                this.setState({ apps: res.data.result })
            }
        }).catch(err => {
            console.log(err)
        })
        
    }
   
    requestGroups = async () => {
        const { id } = this.props.match.params
        const param = {
            "work": "DRAWER-GROUP:INFO.GET",
            "drawer_group_id": id,
            "group_detail": true
        }

        GroupService.getGroupDetail(param).then(res => {
            // console.log(res)
            if (res.status === 200) {
                this.setState({ group: res.data.result })
                this.setState({ all_items: res.data.result.member.amount }, () => {
                    this.getMember()
                })
            }
        }).catch(err => {
            console.log(err)
        })
        const params = {
            "work": "GET_THAIBEV_COMPANY_GROUPS"
        }
        GroupService.requestAPI(params).then(res => {
            if (res.status === 200) {
                this.setState({ categories: res.data.result })
                this.setState({ category_key: res.data.result[0].key })
                this.setState({ category_name: res.data.result[0].display_name })
            }
        }).catch(err => {
            console.log(err)
        })
    }

    request(id) {
        const param = {
            "work": "DRAWER-GROUP:INFO.GET",
            "drawer_group_id": id,
            "group_detail": true
        }
        // console.log(param)
        GroupService.getGroupDetail(param).then(res => {
            // console.log(res)
            if (res.status === 200) {
                this.setState({ group: res.data.result })
                this.getMember()
            }
        }).catch(err => {
            console.log(err)
        })
    }
 
    getMember = () => {
        const { id } = this.props.match.params
        const { search_member } = this.state
        const param = {
            work: "SEARCH_MEMBER_IN_DRAWER_GROUP",
            user_id: "5cff95c2ab4eee1afc097367",
            drawer_group_id: id,
            search_key: search_member,
            current_item_amount: 0,
            item_per_request: 100,
            language: "en"
        }
        GroupService.requestAPI(param).then(res => {
            if (res.status === 200) {
                // this.setState({ ...g.amount , res.data.result.member.amount })
                if (res.data.result.member.persons !== undefined) {
                    this.setState({ persons: res.data.result.member.persons })
                    // this.setState({ all_items: res.data.result.member.amount })
                    // const g = this.state.group
                    // g.member.amount = res.data.result.member.amount
                    // this.setState({ group: g })
                    // console.log(g)
                }
            }
        }).catch(err => {
            console.log(err)
        })

    }

    serchChangeHandler = (event) => {
        const search_member = event.target.value
        const { id } = this.props.match.params

        // const param = {
        //     work: "SEARCH_MEMBER_IN_DRAWER_GROUP",
        //     user_id: "5cff95c2ab4eee1afc097367",
        //     drawer_group_id: id,
        //     search_key: search_member,
        //     current_item_amount: 0,
        //     item_per_request: 100,
        //     language: "en"
        // }
        const param = {
            "work": "DRAWER-GROUP:MEMBER.SEARCH",
            "user_id": "5cff95c2ab4eee1afc097367",
            "drawer_group_id": id,
            "search_key": search_member,
            "current_item_amount": 0,
            "item_per_request": 100,
            "language": "th",
            "excepted": false
        }

        GroupService.MemberSearch(param).then(res => {
            // console.log(res)
            if (res.status === 200) {
                this.setState({ persons: res.data.result.member.persons })
            }
        }).catch(err => {
            console.log(err)
        })
        this.setState({ search_member: event.target.value });
    }

    renderApps(apps) {
        var arr = []
        apps.map((app, index) => {
            this.state.apps.map(item => {
                if (app.app_id === item._id) {
                    arr.push(
                        <div className="col-md-3" id="p1" style={{ marginBlock: 10 }} key={item._id + index}>
                            {/* <Image src={app.icon.active} fluid/> */}
                            <Card style={{ border: 'none' }}>
                                <Card.Img variant="top" src={item.icon_url} fluid='true' />
                            </Card>
                            <br />
                        </div>
                    )
                }
            })

        })
        return arr
    }

    refresh = () => {
        this.requestGroups()
        // this.getMember()
    }

    renderPerson() {
        const { persons, group } = this.state

        var arr = []
        for (let index = 0; index < persons.length; index++) {
            const element = persons[index];
            // <div className="list-group-item col-md-12 d-flex justify-content-between align-items-center" style={{ margin: 5 }} key={element.user_id}>
            arr.push(
                <div className="list-group-item d-flex justify-content-between align-items-center" key={element.user_id}>
                    {/* <ListGroup.Item className="d-flex justify-content-between align-items-center"> */}
                    <Media>
                        <img
                            width={40}
                            height={40}
                            className="mr-2 rounded-circle"
                            src={element.info.profile_url}
                            alt=""
                            onError={(e) => { e.target.onerror = null; e.target.src = "https://yt3.ggpht.com/a/AGF-l7_KcMkAm641_17l3LYRN9jbx8kA6SyZONwang=s48-c-k-c0xffffffff-no-rj-mo" }}
                        />
                        <Media.Body>
                            <h6 className="mb-2">{element.info.first_name} {element.info.last_name} ,{element.info.ref_id}</h6>
                            <p className='mb-0'>{group.title}</p>
                        </Media.Body>
                    </Media>
                    <Button variant="outline-secondary" onClick={() => this.props.history.push({
                        pathname: '/managegroup/' + element.user_id, state: {
                            user: element.info
                        }
                    })}>Manage</Button>
                    {/* </ListGroup.Item> */}
                    {/* <Media>
                        <img
                            width={50}
                            height={50}
                            className="mr-2 rounded-circle"
                            src={element.info.profile_url}
                            alt=""
                            onError={(e) => { e.target.onerror = null; e.target.src = "https://yt3.ggpht.com/a/AGF-l7_KcMkAm641_17l3LYRN9jbx8kA6SyZONwang=s48-c-k-c0xffffffff-no-rj-mo" }}

                        />
                        <Media.Body>
                            <h6 className="mb-2">{element.info.first_name} {element.info.last_name}</h6>
                            <p>{group.title}</p>
                        </Media.Body>
                    </Media>
                    <Button variant="outline-secondary" onClick={() => this.props.history.push({ pathname : '/managegroup/' + element.user._id, state : {
                        user : element.user
                    }})}>Manage</Button> */}
                </div>
            )
        }
        return arr
    }

    render() {
        const { group, all_items, persons } = this.state
        const { id } = this.props.match.params
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-9 ml-sm-10 col-lg-10">
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <div>
                            <button 
                                className='btn p-0'
                                // to="/groups"
                                onClick={() => history.push({
                                    pathname: '/groups/',
                                    state: {
                                        group: this.state.group
                                    }
                                })}
                            >
                                <p className="text-muted" style={{ fontSize: "14px" }} >
                                <Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon> Home</p>
                            </button>
                            {/* state: {
                                group: data
                            } */}
                            <h4>Member Group</h4>
                        </div>
                        <ExportExcel group_id={id}></ExportExcel>
                    </div>
                    <div className="row">
                        <div className="col-md-9">
                            <Card>
                                <Card.Header className="bg-white border-0">
                                    <Row>
                                        <Col xs="9">
                                            <Form>
                                                <Form.Group as={Col} md="12" className="p-0" controlId="validationCustomUsername">
                                                    {/* <Form.Label>Username</Form.Label> */}
                                                    <InputGroup>
                                                        <InputGroup.Prepend>
                                                            <InputGroup.Text id="inputGroupPrepend"><Icon>search</Icon></InputGroup.Text>
                                                        </InputGroup.Prepend>
                                                        <Form.Control
                                                            type="text"
                                                            placeholder="search"
                                                            aria-describedby="inputGroupPrepend"
                                                            ref={(input) => { this.search_member = input; }}
                                                            onChange={this.serchChangeHandler}
                                                        />
                                                    </InputGroup>
                                                </Form.Group>
                                            </Form>
                                        </Col>
                                        {/* <Col xs="3">
                                            <Dropdown>
                                                <Dropdown.Toggle variant="light" id="dropdown-basic"><img className="mb-1" width="20" height="19" src={iconfilter} alt="" />
                                                    Filter</Dropdown.Toggle>: {category_name}

                                                <Dropdown.Menu>
                                                    {
                                                        categories.map((item) => {
                                                            return (
                                                                <Dropdown.Item key={item.key}>{item.display_name}</Dropdown.Item>
                                                            )

                                                        })
                                                    }

                                                </Dropdown.Menu>
                                            </Dropdown>
                                            
                                        </Col> */}
                                    </Row>
                                </Card.Header>
                                <Card.Body>

                                    {persons.length === 0 ? <ul className="list-group">
                                        <li className="list-group-item">User not found</li>

                                    </ul> : <div className="list-group">
                                        {this.renderPerson()}
                                    </div>}


                                </Card.Body>
                            </Card>
                        </div>
                        <div className="col-md-3">
                            <Card className="mb-2">
                                <Card.Header className="bg-white clearfix">
                                    <div className="float-left">{group.title}</div>
                                    <div className="float-right">
                                        <EditGroupName editSuccess={this.requestGroups} group_id={id} group_name={group.title} />
                                    </div>
                                </Card.Header>
                                <Card.Body>
                                    <Card.Text>App Access</Card.Text>
                                    <div className="row">
                                        {group.visible_apps ? this.renderApps(group.visible_apps) : ''}
                                    </div>
                                    <AddEditAccess apps={group.visible_apps} group_id={id} reloadDetail={this.request.bind(this)} />
                                    {/* <Button variant="outline-primary">Add/Edit Access</Button> */}
                                </Card.Body>
                                <Card.Footer className="bg-white border-0">
                                    <Media>
                                        <Icon className="fa fa-users mr-1" style={{ "fontSize": "1.25rem", color: "#888888", "width": "30px" }} />
                                        <Media.Body>
                                            <p>{all_items} Members</p>
                                        </Media.Body>
                                    </Media>
                                    <AddMember group_id={id} addMemberSuccess={this.refresh} />
                                    {/* <Button variant="outline-primary">Add Member</Button> */}
                                </Card.Footer>
                            </Card>
                            {/* <div className="d-flex flex-row-reverse">
                                <Button variant="primary">Save</Button>
                                <Button variant="outline-primary" className="mr-2">Discard</Button>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default GroupDetail;

