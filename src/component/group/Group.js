/* eslint-disable array-callback-return */
import React from 'react';
import '../Home.css';
import { Link } from 'react-router-dom'
import { Card, Media, Form } from 'react-bootstrap'
import iconcreatenews from '../../images/icon-create-news.svg';
import Icon from '@material-ui/core/Icon';
import { Cookies } from "react-cookie"
import './group.css';
import { GroupService, authSign } from '../../services/GroupService';

import { history } from '../../store/history';
import Swal from 'sweetalert2';

import defaultimage from '../../images/default-image.jpg';
// import { CardFooter } from 'react-bootstrap/Card';
// import { NewsService } from '../services/NewsService';

class Group extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            default_apps: [],
            datasource: [],
            current_page: 1,
            per_page: 20,
            start_item_index: 0,
            max_page: 0,
            all_items: 0,
            status: "",
            status_id: "",
            categories: [],
            isLoading: false
        };
    }

    componentDidMount() {
        this.requestGroups()
        // this.memberAmount()
    }
    // componentDidUpdate() {
    //     console.log('update')
    //     this.requestGroups()
    // }

    requestGroups(){
        const param = {
            "work": "DRAWER-GROUP.FEED",
            "user_id": "5cff95c2ab4eee1afc097367"
        }
        this.setState({ isLoading: true })
        authSign().then(res => {
            if (res.data.token !== undefined) {
                const cookies = new Cookies()
                cookies.set('token', res.data.token)
                cookies.set('tmp', res.data.tmp)

                GroupService.getGroups(param).then(res => {
                    // console.log(res)
                    if (res.status === 200) {
                        this.setState({ isLoading: false })
                        let arrayGroups = res.data.result.groups
                        if (this.props.location.state !== undefined) {
                            const { group } = this.props.location.state
                         
                            arrayGroups[arrayGroups.findIndex((x) => x._id === group._id)].member = group.member;
                        }

                        this.setState({ datasource: arrayGroups })


                        // this.setState({ datasource: res.data.result.user_group_roles })
                        res.data.result.groups.map(data => {
                            if (data.default === true) {
                                this.setState({ default_apps: data.apps })
                                sessionStorage.setItem('default_apps', JSON.stringify(data.apps))

                            }
                        })
                    }
                }).catch(err => {
                    console.log(err)
                    this.setState({ isLoading: false })
                })
            }
        }).catch(err => {
            this.setState({ isLoading: false })
        })
    }
    

    deleteGroup = (id) => {
        Swal.fire({
            title: 'ยืนยันการลบ?',
            text: "",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                const param = {
                    work: 'ADMIN_DELETE_DRAWER_GROUP',
                    user_id: '5cff95c2ab4eee1afc097367',
                    drawer_group_id: id
                }
                GroupService.requestAPI(param).then(res => {
                    if (res.status === 200) {
                        Swal.fire(
                            '',
                            'ลบกลุ่มสำเร็จ',
                            'success'
                        )
                        this.requestGroups()
                    }
                }).catch(err => {
                    console.log(err)
                })

            }
        })

    }
    serchChangeHandler = (event) => {
        const search_member = event.target.value
        // console.log(event.target.value)
        // this.setState({ search_member: event.target.value });
        this.props.history.push('/searchmember/' + search_member)
    }
    onError = () => {
        if (!this.state.errored) {
            this.setState({
                src: this.props.fallbackSrc,
                errored: true,
            });
        }
    }

    renderApps(apps, index) {
        var arr = []
        let hiddenStyle;
        for (let j = 0; j < 8; j++) {
            if (apps[j]?.icon?.active === undefined) {
                hiddenStyle = { "height": "52px", "width": "52px", "opacity": 0 }
            } else {
                hiddenStyle = { "height": "52px", "width": "52px" }
            }
            if (j === 7 && apps[j] !== undefined) {
                arr.push(
                    <div className="col-3 p-1" id="p1" style={{ marginBlock: 5 }} key={(index * 100) + j}>
                        {/* <Image src={app.icon.active} fluid/> */}
                        {/* <>{console.log(index * 100 + j)}</> */}
                        <Card style={{ border: 'none', "height": "52px", "width": "52px", }} className='position-relative rounded-circle bg-dark'>

                            <Card.Img
                                variant="top"
                                src={apps[j]?.icon?.active}
                                fluid='true'
                                // style={{ 'opacity': '50%' }}
                                onError={(e) =>
                                    e.target.src = defaultimage
                                }
                                className='rounded-circle'
                                width={'52'}
                                height={'52'}
                                style={{ 'opacity': '50%' }}
                            />
                            <div className='position-absolute  text-white text-center' style={{ "height": "52px", "width": "52px", paddingTop: "8px" }}>
                                <span className='align-middle'>More</span>
                            </div>
                            {/* <span className='position-absolute text-white' style={{
                                'top': '30%',
                                'left': '20%'
                            }}>More</span> */}
                        </Card>
                        <br />
                    </div>
                )
            } else {
                arr.push(
                    <div className="col-3 p-1" id="p1" style={{ marginBlock: 5 }} key={apps[j] !== undefined ? apps[j].id : 200 + j}>
                        {/* <Image src={app.icon.active} fluid/> */}
                        {/* <>{console.log(index * 100 + j)}</> */}
                        <Card style={{ border: 'none' }} className='position-relative rounded-circle'>
                            <Card.Img
                                variant="top"
                                src={apps[j]?.icon?.active}
                                fluid='true'
                                onError={(e) =>
                                    e.target.src = defaultimage
                                }
                                className='rounded-circle'
                                style={hiddenStyle}
                            />

                        </Card>
                        <br />
                    </div>
                )
            }

            // if (j === 7 && apps[j] !== undefined) {
            //     arr.push(
            //         <div className="col-3 p-1" id="p1" style={{ marginBlock: 5 }} key={(index * 100) + j}>
            //             {/* <Image src={app.icon.active} fluid/> */}
            //             {/* <>{console.log(index * 100 + j)}</> */}
            //             <Card style={{ border: 'none', "height": "52px", "width": "52px", }} className='position-relative rounded-circle bg-dark'>

            //                 <Card.Img
            //                     variant="top"
            //                     src={apps[j].icon.active}
            //                     fluid='true'
            //                     // style={{ 'opacity': '50%' }}
            //                     onError={(e) =>
            //                         e.target.src = defaultimage
            //                     }
            //                     className='rounded-circle'
            //                     width={'52'}
            //                     height={'52'}
            //                     style={{ 'opacity': '50%' }}
            //                 />
            //                 <div className='position-absolute  text-white text-center' style={{ "height": "52px", "width": "52px", paddingTop: "8px" }}>
            //                     <span className='align-middle'>More</span>
            //                 </div>
            //                 {/* <span className='position-absolute text-white' style={{
            //                     'top': '30%',
            //                     'left': '20%'
            //                 }}>More</span> */}
            //             </Card>
            //             <br />
            //         </div>
            //     )
            // }

        }
        // apps.map((app, j) => {
        //     if (j < 7) {
        //         arr.push(
        //             <div className="col-3 p-1" id="p1" style={{ marginBlock: 5 }} key={(index * 100) + j}>
        //                 {/* <Image src={app.icon.active} fluid/> */}
        //                 <Card style={{ border: 'none' }} className='position-relative rounded-circle'>
        //                     <Card.Img
        //                         variant="top"
        //                         src={app.icon.active}
        //                         fluid='true'
        //                         onError={(e) =>
        //                             e.target.src = defaultimage
        //                         }
        //                         className='rounded-circle'
        //                         style={{ "height": "52px", "width": "52px"}}
        //                     />

        //                 </Card>
        //                 <br />
        //             </div>
        //         )
        //     } else if (j === 12) {
        //         arr.push(
        //             <div className="col-3 p-1" id="p1" style={{ marginBlock: 5 }} key={(index * 100) + j}>
        //                 {/* <Image src={app.icon.active} fluid/> */}
        //                 <Card style={{ border: 'none', "height": "52px", "width": "52px", }} className='position-relative rounded-circle bg-dark'>

        //                     <Card.Img
        //                         variant="top"
        //                         src={app.icon.active}
        //                         fluid='true'
        //                         // style={{ 'opacity': '50%' }}
        //                         onError={(e) =>
        //                             e.target.src = defaultimage
        //                         }
        //                         className='rounded-circle'
        //                         width={'52'}
        //                         height={'52'}
        //                         style={{ 'opacity': '50%' }}
        //                     />
        //                     <div className='position-absolute  text-white text-center' style={{ "height": "52px", "width": "52px", paddingTop: "8px" }}>
        //                         <span className='align-middle'>More</span>
        //                     </div>
        //                     {/* <span className='position-absolute text-white' style={{
        //                         'top': '30%',
        //                         'left': '20%'
        //                     }}>More</span> */}
        //                 </Card>
        //                 <br />
        //             </div>
        //         )
        //     }

        // })
        return arr
    }

    renderGroups(filter) {
        let arr = []
        let v2_style = {}
        const { datasource } = this.state
        if (filter) {
            v2_style = { borderTopColor: "#FA6400", borderTopWidth: "5px", color: "#FA6400" }
        }
        datasource.filter((data) => {
            return data["default:v2"] === filter
        }).map((data, index) => {
            arr.push(
                <div className="col-sm-2 col-md-4 col-lg-3" key={index}>
                    <Card className='shadow-sm' style={v2_style}>
                        <Card.Header>
                            <div className="float-left">{data?.title}</div>
                            {/* <div className="float-right"><img className="mb-1" src={iconpassword} alt="" /></div> */}
                            <div className="float-right" style={{ cursor: 'pointer' }} hidden={data?.member?.amount !== 0}><Icon style={{ height: 20 }} className="fa fa-trash" fluid='true' onClick={this.deleteGroup.bind(this, data._id)} /></div>
                            {/* <button className="float-right btn"><Icon className="fa fa-trash" /></button> */}
                        </Card.Header>
                        <Card.Body style={{ cursor: 'pointer' }} className="bg-white clearfix py-0" onClick={() => history.push({
                            pathname: '/groupdetail/' + data._id,
                            state: {
                                group: data
                            }
                        })}>
                            <Card.Text className='mt-2 text-secondary'>App Access</Card.Text>
                            <div className="row">
                                {this.renderApps(data.apps, index)}
                            </div>
                        </Card.Body>
                        <Card.Footer className="bg-white border-0">
                            <Media style={{ cursor: 'pointer' }} onClick={() => this.props.history.push('/addmember/' + data._id)}>
                                <Icon className="fa fa-users mr-1" style={{ "fontSize": "1.25rem", color: "#888888", "width": "30px" }} />
                                <Media.Body>
                                    <p className='mb-0 text-secondary'>{data?.member?.amount.toLocaleString()} Members</p>
                                </Media.Body>
                            </Media>
                        </Card.Footer>
                    </Card>
                    <br />
                </div>
            )
        })
        return arr
    }

    render() {
        return (
            <div className="row justify-content-md-center">
                <div role="main" className="col-md-12 ml-sm-12 col-lg-11">
                    <div className="mr-auto mt-3 p-0 bd-highlight">
                        <Link to="/"><p className="text-muted mb-1" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>Back</p></Link>

                    </div>
                    <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                        <div>
                            <h4 className="h4">{(this.state.status === undefined || this.state.status === null || this.state.status === "") ? "ThaiBev Affiliates" : this.state.status}</h4>
                            <p>ระบบบริหารจัดการการเข้าถึง Application  Beverest Life ของบริษัทในเครือไทยเบฟเวอเรจ</p>
                        </div>

                        <div className="btn-toolbar mb-2 mb-md-0">
                            <Form inline>
                                <div className="btn-group mr-2 mb   -2">
                                    <div className="input-group mr-2">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-white"><Icon>search</Icon></div>
                                        </div>
                                        <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Search Members" onChange={this.serchChangeHandler} />
                                    </div>
                                </div>
                                <button type="button" className="btn btn-primary" onClick={() => history.push({
                                    pathname: '/create-group',
                                    state: {
                                        apps: this.state.default_apps
                                    }
                                })}><img className="mb-1" src={iconcreatenews} alt="" />
                                    <span className='ml-2'>Create Group</span>
                                </button>
                            </Form>
                        </div>

                    </div>
                    {this.state.isLoading ? "กำลังโหลดข้อมูล..." : (
                        <>
                            <div className="row">
                                {this.renderGroups(Boolean(1))}
                            </div>
                            <div className='border mt-3 mb-5'></div>
                            <div className="row">
                                {this.renderGroups(Boolean(0))}
                            </div>
                        </>
                    )}

                </div>
            </div>
        )
    }
}

export default Group;

