

import { useState, useEffect } from "react";
import { Card, Media, Form } from 'react-bootstrap'
import Icon from '@material-ui/core/Icon';
import { history } from '../../store/history';
import { Cookies } from "react-cookie"
import { GroupService, authSign } from '../../services/GroupService';

function ManagementUser() {

    const [member_amount, setMemberAmount] = useState([])
    const [datasource] = useState([
        { title: "ThaiBev Affiliates", member: member_amount, discription: "User Group ที่อยู่ภายใต้ธุรกิจของ ThaiBev Group", path: "/groups", active: true },
        { title: "Other Groups", member: member_amount, discription: "User Group ทั่วไปที่สามารถเข้าถึง App ของ Beverege Life ที่ใช้ Authen อื่น เช่น Open ID, Facebook, Gmail", path: "#", active: false }
    ]);
    useEffect(() => {
        let mounted = true;
        // authSign().then(res => {
        //     if (mounted) {
        //         if (res.data.token !== undefined) {
        //             const cookies = new Cookies()
        //             cookies.set('token', res.data.token)
        //             cookies.set('tmp', res.data.tmp)
        //             const param = {
        //                 "work": "DRAWER-GROUP:OVERVIEW.GET"
        //             }
        //             GroupService.getOverviewgroup(param).then(res_2 => {
        //                 if (res.status === 200) {
        //                     setMemberAmount(res_2.data.result)
        //                 }
        //             }).catch(err => {
        //                 console.log(err)
        //             })
        //         }
        //         // setDatasource(items)
        //     }
        // })
        return () => mounted = false;
    }, [])

    // requestGroups = () => {
    //     const param = {
    //         "work": "DRAWER-GROUP.FEED",
    //         "user_id": "5cff95c2ab4eee1afc097367"
    //     }
    //     authSign().then(res => {
    //         if (res.data.token !== undefined) {
    //             const cookies = new Cookies()
    //             cookies.set('token', res.data.token)
    //             cookies.set('tmp', res.data.tmp)

    // GroupService.getGroups(param).then(res => {
    //     console.log(res)
    //     if (res.status === 200) {
    //         // this.setState({ datasource: res.data.result.groups })


    //         // this.setState({ datasource: res.data.result.user_group_roles })
    //         res.data.result.groups.map(data => {
    //             if (data.default === true) {
    //                 this.setState({ default_apps: data.apps })
    //                 sessionStorage.setItem('default_apps', JSON.stringify(data.apps))

    //             }
    //         })
    //     }
    // }).catch(err => {
    //     console.log(err)
    // })
    //         }
    //     }).catch(err => {

    //     })

    // }
    const renderGroups = (amount) => {
        var arr = []
        // const { datasource } = this.state
        datasource.map((data, index) => (
            arr.push(
                <button className="btn card col-sm-2 col-md-4 col-lg-3" key={index} disabled={!data.active}  onClick={() => history.push({
                    pathname: data.path,
                })}>
                    {/* <Card> */}
                    <Card.Header className='w-100 bg-white'>
                        <div className="float-left text-primary font-weight-bolder">{data.title}</div>
                    </Card.Header>
                    <Card.Body
                        // style={{ cursor: 'pointer', minHeight: '120px' }}
                        className="bg-white text-muted"
                       
                    >
                        <Card.Text className='mb-0'>{data.discription}</Card.Text>
                        <div className="row">
                            {/* {this.renderApps(data.apps, index)} */}
                        </div>
                    </Card.Body>
                    <Card.Footer className="my-2 w-100 py-0 bg-white border-0 d-flex align-items-center">
                        <Media className='d-flex align-items-center' style={{ cursor: 'pointer' }}
                        // onClick={() => this.props.history.push('/addmember/' + data._id)}
                        >
                            <Icon className="fa fa-users mr-1" style={{ "fontSize": "1.25rem", color: "#888888", "width": "30px" }} />

                            <Media.Body className='d-flex align-items-center text-muted'>
                                {index === 0 ? `${amount?.thaibev_affiliate?.["member:amount"].toLocaleString('en-US')} `: null}
                                {index === 1 ? `${amount?.consumer?.["member:amount"].toLocaleString('en-US')} ` : null}
                                Members  
                            </Media.Body>

                        </Media>

                    </Card.Footer>
                    {/* </Card> */}
                    {/* <br /> */}
                </button>
            )
        ))
        return arr
    }

    return (
        <div className="row justify-content-md-center">
            <div role="main" className="col-md-9 ml-sm-10 col-lg-10">
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
                    <div>
                        <h4 className="h4">
                            ThaiBev Mangement User Groups

                        </h4>
                        <p>App Permission Setting</p>
                    </div>

                    <div className="btn-toolbar mb-2 mb-md-0">
                        <Form inline>
                            <div className="btn-group mr-2">
                                <div className="input-group mr-2">
                                    {/* <div className="input-group-prepend">
                                        <div className="input-group-text bg-white"><Icon>search</Icon></div>
                                    </div>
                                    <input type="text" className="form-control" id="inlineFormInputGroup" placeholder="Search Members" /> */}
                                </div>
                            </div>

                        </Form>
                    </div>

                </div>
                <div className="card-deck row">
                    {renderGroups(member_amount)}
                </div>
            </div>
        </div>
    )
}
export default ManagementUser