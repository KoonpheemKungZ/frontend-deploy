import React from 'react';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom'
import imageUpload from '../../images/icon-upload.svg'
import $ from 'jquery'
import './CreateNews.css';
import axios from 'axios'
import { utils } from '../../utils/utils'
import Swal from 'sweetalert2'
import { API_URL } from '../../utils/constants'
import moment from 'moment'
import highlight from '../../images/ic_pin_article_active.svg';
import unHighlight from '../../images/ic_pin_article_inactive.svg';
import DatePicker from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css";

class EditNews extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            select_category: "",
            categories: undefined,
            title: "",
            description: "",
            file: "",
            category: "",
            status: "",
            is_public: true,
            image0: "",
            image1: "",
            image2: "",
            caption0: "",
            caption1: "",
            caption2: "",
            is_change_cover: false,
            is_change_image0: false,
            is_change_image1: false,
            is_change_image2: false,
            highlight: false,
            scheduleDate: moment().toDate()
        };
    }

    componentDidMount() {
        jqueryScript();
        this.loadNews();
        this.loadCategory();
    }
    loadNews() {
        let id = this.props.match.params.id;
        var params = {
            "work": "CMS_GET_NEWS_BY_NEWS_ID",
            "news_id": id,
            "reference_fields": ["file", "description", "title", "image"]

        }
        axios({
            method: "post",
            url: API_URL,
            data: params
        })
            .then(response => {
                if (response.data.status === true) {
                    //console.log(response.data);
                    var news = response.data.result;
                    this.setState({ status: news.status.desc })
                    this.setState({ is_public: news.status.is_publishing })
                    this.setState({ title: news.contents.title })
                    this.setState({ file: news.contents.file });
                    this.setState({ description: news.contents.description })
                    this.setState({ select_category: news.category._id })
                    this.setState({ highlight: news.highlight ? news.highlight : false })
                    this.setState({ scheduleDate: news.status.schedule ? moment.unix(news.status.schedule.publish_timestamp).toDate() : moment().toDate() })
                    for (let i = 0; i < news.contents.images.length; i++) {
                        let cap = news.contents.images[i];

                        if (i === 0) {
                            this.setState({ image0: cap.image_url })
                            this.setState({ caption0: cap.caption })
                        } else if (i === 1) {
                            this.setState({ image1: cap.image_url })
                            this.setState({ caption1: cap.caption })
                        } if (i === 2) {
                            this.setState({ image2: cap.image_url })
                            this.setState({ caption2: cap.caption })
                        }
                    }
                }

            }).catch(error => {
                console.log(error);
            });
    }
    loadCategory() {
        var params = {
            "work": "GET_NEWS_CATEGORIES"
        }
        axios({
            method: "post",
            url: API_URL,
            data: params
        })
            .then(response => {
                if (response.data.status === true) {
                    this.setState({ categories: response.data.result })
                }

            }).catch(error => {
                console.log(error);
            });
    }

    sendDeleteNews = () => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be delete news?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'OK'
        }).then((result) => {
            if (result.value) {
                let uid = localStorage.getItem("uid");
                let id = this.props.match.params.id;
                var params = {
                    work: "CMS_UPDATE_NEWS",
                    news_id: id,
                    updater_id: uid + ".firebase",
                    status_key: utils.getStatusKey("Hidden")
                }
                console.log(params)
                axios({
                    method: "post",
                    url: API_URL,
                    data: params
                })
                    .then(response => {
                        console.log(response.data)
                        if (response.data.status === true) {
                            Swal.fire({
                                type: "success",
                                title: "News has been deleted!",
                                showConfirmButton: false,
                                timer: 1500
                            }).then(() => {
                                this.props.history.push('/home/All')
                            });
                        }

                    }).catch(error => {
                        console.log(error)
                        Swal.fire({
                            type: 'error',
                            title: 'Error!',
                            text: 'Cannot be delete news!'
                        })
                    });
            }
        })
    }

    sendUpdateNews = async (e) => {
        let id = this.props.match.params.id;
        let uid = localStorage.getItem("uid");
        var file = this.state.file;
        var image0 = this.state.image0;
        var image1 = this.state.image1;
        var image2 = this.state.image2;
        if (this.state.is_change_cover) {
            file = await this.uploadImageCover(this.state.file)
        }
        if (this.state.is_change_image0) {
            image0 = await this.uploadImage(this.state.image0)
        }
        if (this.state.is_change_image1) {
            image1 = await this.uploadImage(this.state.image1)
        }
        if (this.state.is_change_image2) {
            image2 = await this.uploadImage(this.state.image2)
        }
        var params = {
            work: "CMS_UPDATE_NEWS",
            news_id: id,
            updater_id: uid + ".firebase",
            status_key: utils.getStatusKey(this.state.status),
            title: this.state.title,
            file: file,
            description: this.state.description,
            category_key: this.state.select_category,
            images: [{
                file: image0,
                caption: this.state.caption0
            },
            {
                file: image1,
                caption: this.state.caption1
            },
            {
                file: image2,
                caption: this.state.caption2
            }],
            highlight: this.state.highlight ? true : false
        }

        if (this.state.status === 'Schedule') {
            params.schedule = {
                publish_timestamp: Number(utils.convertDateToTimeStamp(this.state.scheduleDate))
            }
        }

        // console.log(params)
        axios({
            method: "post",
            url: API_URL,
            data: params
        })
            .then(response => {
                console.log(response.data);
                if (response.data.status === true) {
                    Swal.fire({
                        type: "success",
                        title: "News has been update!",
                        showConfirmButton: false,
                        timer: 1500
                    }).then(() => {
                        this.props.history.push('/home/All')
                    });
                }

            }).catch(error => {
                Swal.fire({
                    type: 'error',
                    title: 'Error!',
                    text: 'Cannot be update news!'
                })
            });
    }

    updateNews = (e) => {
        e.preventDefault();

        if (this.validate()) {
            this.sendUpdateNews()
        }
    }

    addImagePlaceholderSrc(e) {
        e.target.src = imageUpload
    }

    validate() {
        var check = true;
        if (this.state.title.length === 0) {
            check = false;
            Swal.fire({
                type: 'warning',
                text: 'Please select some category'
            })
        } else if (this.state.description.length === 0) {
            check = false;
            Swal.fire({
                type: 'warning',
                title: 'Warning!',
                text: 'Description text is require'
            })
        } else if (this.state.file.length === 0) {
            check = false;
            Swal.fire({
                type: 'warning',
                title: 'Warning!',
                text: 'Please cover image is require'
            })
        }
        return check;
    }

    renderOptionCategory(b) {
        if (b === undefined || b.length === 0) return [];
        const arr = [];
        for (var i = 0; i < b.length; i++) {
            arr.push(
                <option value={b[i]._id} key={i} defaultValue={this.state.select_category}>
                    {b[i].name}
                </option>
            );
        }
        return arr;
    }

    handleCoverChange = (e) => {
        var value = e.target.files
        // const name = e.target.name

        if (e.target.type === 'file') {
            var reader = new FileReader();
            reader.readAsDataURL(value[0]);
            reader.onload = (upload) => {
                this.setState({ is_change_cover: true })
                this.setState({ file: upload.target.result })
                return;
            }
        }
    }

    uploadImage = async (base64Data) => {
        var m = moment().utcOffset("+07:00");
        var refer = m.format("YYYYMMDDx");
        const extFilename = base64Data.split(';')[0].split('/')[1];

        const work = {
            work: "RECEIVE_STRING_BASE64_FILE",
            category: "news",
            file_name: `${refer}.${extFilename}`,
            base64: base64Data
        }

        try {
            let apiServer = process.env.REACT_APP_API_URL_UPLOAD;
            const res = await axios.post(`${apiServer}`, work, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            const { status, message, result: { image_path } } = res.data
            console.log(message)
            return status ? `${image_path}` : ''

        } catch (err) {
            console.log(err)
        }
    }

    uploadImageCover = async (base64Data) => {
        var m = moment().utcOffset("+07:00");
        var refer = m.format("YYYYMMDDx");
        const extFilename = base64Data.split(';')[0].split('/')[1];

        const work = {
            work: "RECEIVE_STRING_BASE64_FILE",
            category: "news",
            file_name: `${refer}.${extFilename}`,
            base64: base64Data
        }

        try {
            let apiServer = process.env.REACT_APP_API_URL_UPLOAD;
            const res = await axios.post(`${apiServer}`, work, {
                headers: {
                    'Content-Type': 'application/json'
                }
            })

            const { status, message, result: { image_path } } = res.data
            console.log(message)
            return status ? `${image_path}` : ''

        } catch (err) {
            console.log(err)
        }
    }

    handleImage0 = (e) => {
        var value = e.target.files
        // const name = e.target.name

        if (e.target.type === 'file') {
            var reader = new FileReader();
            reader.readAsDataURL(value[0]);
            reader.onload = (upload) => {
                this.setState({ is_change_image0: true })
                this.setState({ image0: upload.target.result })
                return;
            }
        }
    }

    handleImage1 = (e) => {
        var value = e.target.files
        // const name = e.target.name

        if (e.target.type === 'file') {
            var reader = new FileReader();
            reader.readAsDataURL(value[0]);
            reader.onload = (upload) => {
                this.setState({ is_change_image1: true })
                this.setState({ image1: upload.target.result })
                return;
            }
        }
    }

    handleImage2 = (e) => {
        var value = e.target.files
        // const name = e.target.name

        if (e.target.type === 'file') {
            var reader = new FileReader();
            reader.readAsDataURL(value[0]);
            reader.onload = (upload) => {
                this.setState({ is_change_image2: true })
                this.setState({ image2: upload.target.result })
                return;
            }
        }
    }

    render() {
        const temState = JSON.parse(JSON.stringify(this.state))
        delete temState.categories

        return (
            <div role="main" className="col-md-9 ml-sm-auto col-lg-10">

                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3">
                    <Link to="/home/All"><p className="text-muted" style={{ fontSize: "14px" }} ><Icon style={{ paddingTop: "6px" }}>keyboard_arrow_left</Icon>All news</p></Link>
                </div>
                <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3">
                    <h4 className="h4">Edit article</h4>
                </div>
                <form id='my-form' onSubmit={this.updateNews}>
                    <div className="row">
                        <div className="col-8">
                            <div className="card mb-3">
                                <div className="card-body">
                                    <div className="form-group">
                                        <div className="d-flex">
                                            <label htmlFor="exampleInputEmail1">Title <span className="text-danger">*</span></label>
                                            <label htmlFor="exampleInputEmail1" className="text-muted d-inline-flex ml-auto">Max 250 characters</label>
                                        </div>
                                        <input type="text" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Title news" value={this.state.title} onChange={e => this.setState({ title: e.target.value })} />
                                    </div>
                                    <div className="form-group">
                                        <div className="d-flex">
                                            <label htmlFor="exampleInputEmail1">Category <span className="text-danger">*</span></label>
                                        </div>
                                        <div className="input-group">
                                            <select className="custom-select" id="inputGroupSelect04" onChange={e => this.setState({ select_category: e.target.value })} value={this.state.select_category}>
                                                {this.renderOptionCategory(this.state.categories)}
                                            </select>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label htmlFor="exampleFormControlTextarea1" style={{ display: 'block' }}>Feature image <span className="text-danger">*</span></label>
                                        <div>
                                            <input type="file" className="form-control upload-img" id="upload-img" data-id="4" style={{ display: 'none' }} onChange={this.handleCoverChange} />
                                            <img src={this.state.file} onError={this.addImagePlaceholderSrc} alt="" className="img-thumbnail upload-img" id="upload-img" style={{ cursor: 'pointer', display: 'inline-block' }} />
                                            <label id="emailHelp" className="text-muted mt-2" style={{ display: 'inline-block', verticalAlign: 'top', marginLeft: '21px' }}>ขนาดรูป<br />900px * 450px</label>
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <div className="d-flex">
                                            <label htmlFor="exampleFormControlTextarea1">Description <span className="text-danger">*</span></label>
                                            <label htmlFor="exampleInputEmail1" className="text-muted d-inline-flex ml-auto">Max 500 characters</label>
                                        </div>
                                        <textarea className="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Detail" style={{ resize: "none" }} value={this.state.description} onChange={e => this.setState({ description: e.target.value })}></textarea>
                                    </div>
                                </div>
                            </div>
                            <h5>Optional Content</h5>
                            {/* Optional 1 */}
                            <div className="card mb-3">
                                <div className="card-body">

                                    <div className="row">
                                        <div className="col-auto">
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Image</label>
                                                <input type="file" className="form-control upload-img0" id="upload-img0" placeholder="Caption" data-id="0" style={{ display: 'none' }} onChange={this.handleImage0} />
                                                <img src={this.state.image0} onError={this.addImagePlaceholderSrc} alt="" className="img-thumbnail upload-img0" id="upload-img0" style={{ cursor: 'pointer', display: 'block' }} />
                                                <label id="emailHelp" className="form-text text-muted">ขนาดรูป<br />900px * 450px</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlTextarea1">Content</label>
                                                <textarea className="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Detail" style={{ resize: "none" }} value={this.state.caption0} onChange={(e) => this.setState({ caption0: e.target.value })}></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/* Optional 2 */}
                            <div className="card mb-3">
                                <div className="card-body">

                                    <div className="row">
                                        <div className="col-auto">
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Image</label>
                                                <input type="file" className="form-control upload-img1" id="upload-img1" placeholder="Caption" data-id="1" style={{ display: 'none' }} onChange={this.handleImage1} />
                                                <img src={this.state.image1} onError={this.addImagePlaceholderSrc} alt="" className="img-thumbnail upload-img1" data-id="1" id="upload-img1" style={{ cursor: 'pointer', display: 'block' }} />
                                                <label id="emailHelp" className="form-text text-muted">ขนาดรูป<br />900px * 450px</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlTextarea1">Content</label>
                                                <textarea className="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Detail" style={{ resize: "none" }} value={this.state.caption1} onChange={(e) => this.setState({ caption1: e.target.value })}></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            {/* Optional 3 */}
                            <div className="card mb-3">
                                <div className="card-body">

                                    <div className="row">
                                        <div className="col-auto">
                                            <div className="form-group">
                                                <label htmlFor="exampleInputEmail1">Image</label>
                                                <input type="file" className="form-control upload-img2" id="upload-img2" placeholder="Caption" data-id="2" style={{ display: 'none' }} onChange={this.handleImage2} />
                                                <img src={this.state.image2} onError={this.addImagePlaceholderSrc} alt="" className="img-thumbnail upload-img2" data-id="2" id="upload-img2" style={{ cursor: 'pointer', display: 'block' }} />
                                                <label id="emailHelp" className="form-text text-muted">ขนาดรูป<br />900px * 450px</label>
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div className="form-group">
                                                <label htmlFor="exampleFormControlTextarea1">Content</label>
                                                <textarea className="form-control" id="exampleFormControlTextarea1" rows="6" placeholder="Detail" style={{ resize: "none" }} value={this.state.caption2} onChange={(e) => this.setState({ caption2: e.target.value })}></textarea>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <button type="button" className="btn btn-danger float-right delete-news" onClick={this.sendDeleteNews}>Delete</button>
                        </div>
                        <div className="col-4">
                            <div className="card">
                                <div className="card-body">
                                    <div>
                                        <h5 className="card-title">Setting publish</h5>
                                        <div className="form-check">
                                            <input className="form-check-input" type="radio" name="eexampleRadios" id="published" value="Publish" checked={(this.state.status === "Publish" ? true : false)} onChange={e => this.setState({ status: e.currentTarget.value })} />
                                            <label className="form-check-label" htmlFor="published">Published</label>
                                        </div>
                                        <div className="form-check mt-2">
                                            <input className="form-check-input" type="radio" name="eexampleRadios" id="unpublished" value="Unpublish" checked={(this.state.status === "Unpublish" ? true : false)} onChange={e => this.setState({ status: e.currentTarget.value })} />
                                            <label className="form-check-label" htmlFor="unpublished">Unpublished</label>
                                        </div>
                                        <div className="form-check mt-2">
                                            <input className="form-check-input" type="radio" name="eexampleRadios" id="schedule" value="Schedule" checked={(this.state.status === "Schedule" ? true : false)} onChange={e => this.setState({ status: e.currentTarget.value })} />
                                            <label className="form-check-label" htmlFor="schedule">Schedule</label>
                                        </div>
                                    </div>

                                    <div className="mt-3">
                                        <h6 className="card-title">Publishing Schedule</h6>
                                        <DatePicker
                                            name="startEvent"
                                            className="form-control"
                                            selected={this.state.scheduleDate}
                                            dateFormat="dd MMM yyyy"
                                            onChange={date => this.setState({ scheduleDate: date })}
                                            placeholderText={moment().format('DD/MM/YYYY')}
                                            minDate={new Date()}
                                            disabled={!(this.state.status === 'Schedule')}
                                        />
                                    </div>

                                    <div className="mt-3">
                                        <h6 className="card-title">Setting Highlight News</h6>
                                        <span style={{ cursor: 'pointer' }} onClick={() => this.setState({ highlight: !this.state.highlight })}>{this.state.highlight ? <img src={highlight} alt=''/> : <img src={unHighlight} alt=''/>} Highlight</span>
                                        <div className="d-block mt-2">
                                            <span style={{ color: 'red' }}>Note </span><span>: บทความข่าวจะแสดง Highlight ได้หนึ่งบทความเท่านั้น</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                {/* <div style={{ width: '300px', height: '300px', border: '1px solid red', position: 'fixed', bottom: '0', right: '0', overflowY: 'scroll' }}>
                    <pre>{JSON.stringify(temState, null, 2)}</pre>
                </div> */}
            </div>
        )
    }
}

function jqueryScript() {
    $("img#upload-img").click(function (e) {
        var id = $(this).attr('data-id')
        console.log('upload image click', id)
        $("input.upload-img").trigger('click');
    });
    $("img#upload-img0").click(function (e) {
        var id = $(this).attr('data-id')
        console.log('upload image click', id)
        $("input.upload-img0").trigger('click');
    });
    $("img#upload-img1").click(function (e) {
        var id = $(this).attr('data-id')
        console.log('upload image click', id)
        $("input.upload-img1").trigger('click');
    });
    $("img#upload-img2").click(function (e) {
        var id = $(this).attr('data-id')
        console.log('upload image click', id)
        $("input.upload-img2").trigger('click');
    });

    function readURL(input, id) {
        //console.log('readURL', id, input)
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //console.log(e)
                switch (id) {
                    case '0':
                        //this.setState({image0:e.target.result})
                        $('img#upload-img0').attr('src', e.target.result);
                        break
                    case '1':
                        $('img#upload-img1').attr('src', e.target.result);
                        //this.setState({image1:e.target.result})
                        break
                    case '2':
                        $('img#upload-img2').attr('src', e.target.result);
                        //this.setState({image2:e.target.result})
                        break
                    default:
                        //this.setState({file:e.target.result})
                        $('img#upload-img').attr('src', e.target.result);
                }

            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("input#upload-img").change(function () {
        var id = $(this).attr('data-id')
        console.log('image change', id)
        readURL(this, id)
    });
    $("input#upload-img0").change(function () {
        var id = $(this).attr('data-id')
        console.log('image change', id)
        readURL(this, id)
        // readURL(this);
    });
    $("input#upload-img1").change(function () {
        var id = $(this).attr('data-id')
        console.log('image change', id)
        readURL(this, id)
    });
    $("input#upload-img2").change(function () {
        var id = $(this).attr('data-id')
        console.log('image change', id)
        readURL(this, id)
    });
}

export default EditNews;