import React, { useState } from 'react';
import { CustomPicker, SketchPicker } from 'react-color';
// import { SketchPicker } from 'react-color';

// class ColorPicker extends React.Component {
const ColorPicker = (props) => {
    const { color, handleChangeComplete } = props
    const [displayBgColorPicker, setDisplayBgColorPicker] = useState(false)
    // const [textColor, setTextColor] = useState('#ffffff')
    
    const ChangeComplete = (color) => {
        handleChangeComplete(color.hex)
        // setDisplayColorPicker(false)
    };
    const handleColorClose = () => {
        setDisplayBgColorPicker(false)
    };
    const handleBgColorClick = () => {
        setDisplayBgColorPicker(true)
    };
    return <div className="input-group mb-3">
        {displayBgColorPicker ? <div style={{
                position: 'absolute',
                zIndex: '2',
            }}>
            <div style={{
                position: 'fixed',
                top: '0px',
                right: '0px',
                bottom: '0px',
                left: '0px',
            }} onClick={handleColorClose} />
            <SketchPicker
                color={color}
                onChangeComplete={ChangeComplete}
            />
            {/* <ColorPicker color={backgroundColor} handleChangeComplete={setBackgroundColor} /> */}

        </div> : null}
        <input type="text" readOnly className={`form-control border-right-0 bg-white`} defaultValue={color}  placeholder="(Optional)" aria-label="Recipient's username" aria-describedby="basic-addon2" />
        <div className="input-group-append" onClick={handleBgColorClick}>
            <span className="input-group-text bg-white" id="basic-addon1">
                <div>
                    <div className="border" style={{

                        width: '24px',
                        height: '24px',
                        borderRadius: '2px',
                        background: `${color}`,

                    }} />
                </div>
            </span>

        </div>
    </div>

    // return <SketchPicker
    //     color={color}
    //     onChangeComplete={ChangeComplete}
    // />;

}

export default CustomPicker(ColorPicker);