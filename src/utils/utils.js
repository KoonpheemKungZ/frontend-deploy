import moment from "moment";

export class utils {
    static getStatusKey(status){
        if(status === "Publish"){
            return "5d453e2a1e79ab8688d1bd1b";
        }else if(status === "Unpublish"){
            return "5d453e2f1e79ab8688d1bd1c";
        }else if(status === "Hidden"){
            return "5d453e351e79ab8688d1bd1d";
        }else if(status === "Schedule"){
            return "5dde316e786e6b352f361107";
        }else{
            return "";
        }
    }

    static convertDateToTimeStamp(date) {
        const dateFormat = moment(date).format()
        return moment(dateFormat).format('X')
    }
}