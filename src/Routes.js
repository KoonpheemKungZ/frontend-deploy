import React from 'react';
import { Route, Switch } from 'react-router-dom'
import RouteLayout from './layout/RouteLayout';

// import Login from './component/Login';
import PageNotFound from './component/PageNotFound';
import Home from './component/Home';
import Page1 from './component/Page1';

// import CreateNews from './component/news/CreateNews'
import EditNews from './component/news/EditNews'
import { GroupList } from './component/group';
import { OtherGroup } from './component/group';
import ManagementUser  from './component/group/MangementUserGroups';
import AddMember from './component/group/AddMember';
import AddEmpoyee from './component/group/AddEmpoyee';
import AllEmpoyee from './component/group/AllEmpoyee';
import { GroupDetail } from './component/group';
import CreateGroup from './component/group/CreateGroup';
import SearchMember from './component/group/SearchMember';
import ManageGroup from './component/group/ManageGroup';
import ManageApp from './component/group/ManageApp';
import ManageSettingApp from './component/group/ManageSettingApp';
import ManageCreateApp from './component/group/ManageCreateApp';

// eslint-disable-next-line import/no-anonymous-default-export
export default ({ childProps }) =>
  <Switch>
    {/* <Route exact path="/"><Redirect to="/groups" /></Route> */}
    <RouteLayout path="/" exact component={ManagementUser} props={childProps} />

    {/* <RouteLayout path="/login" exact component={Login} props={childProps}  /> */}
    <RouteLayout path="/addmember/:id" exact component={AddMember} props={childProps} />
    <RouteLayout path="/all-empoyee" exact component={AllEmpoyee} props={childProps} />
    <RouteLayout path="/add-empoyee" exact component={AddEmpoyee} props={childProps} />
    <RouteLayout path="/searchmember/:value" exact component={SearchMember} props={childProps} />
    <RouteLayout path="/managegroup/:id" exact component={ManageGroup} props={childProps} />
    <RouteLayout path="/home/:status" exact component={Home} props={childProps} />
    <RouteLayout path="/page1" exact component={Page1} props={childProps} />
    {/* <RouteLayout path="/createnews" exact component={CreateNews} props={childProps} /> */}
    <RouteLayout path="/editnews/:id" exact component={EditNews} props={childProps} />
    <RouteLayout path="/groups" exact component={GroupList} props={childProps} />
    <RouteLayout path="/other-groups" exact component={OtherGroup} props={childProps} />
    <RouteLayout path="/create-group" exact component={CreateGroup} props={childProps} />
    <RouteLayout path="/groupdetail/:id" exact component={GroupDetail} props={childProps} />
    <RouteLayout path="/managegroup/:id" exact component={ManageGroup} props={childProps} />
    <RouteLayout path="/manage-app" exact component={ManageApp} props={childProps} />
    <RouteLayout path="/manage-setting-app/:id" exact component={ManageSettingApp} props={childProps} />
    <RouteLayout path="/manage-create-app" exact component={ManageCreateApp} props={childProps} />
    { /* Finally, catch all unmatched routes */}
    <Route component={PageNotFound} />
  </Switch>
