import React from 'react';
import Routes from './Routes';
import { Router, HashRouter } from 'react-router-dom';
import { history } from './store/history';
import Footer from './component/Footer';

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      isAuthenticated: false,
      isCreateNews: false,
    };
  }

  userHasAuthenticated = authenticated => {
    this.setState({ isAuthenticated: authenticated }, () => {
      //this.gotoHomePage()
    });
    console.log(this.state.isAuthenticated)
  }

  userCreateNews = createnews => {
    this.setState({ isCreateNews: createnews }, () => {
      //this.gotoHomePage()
    });
  }

  render() {

    const childProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated,
      isCreateNews: this.userCreateNews
    };

    return (
      <HashRouter>
        <Router history={history}>
          <div className="App mb-5">
            <Routes childProps={childProps}/>
            {/* <Switch>
              <Route exact path="/"><Redirect to="/login" /></Route>
              <RouteLayout path="/login" exact component={Login} props={childProps} />
              <RouteLayout path="/addmember/:id" exact component={AddMember} props={childProps} />
              <RouteLayout path="/searchmember/:value" exact component={SearchMember} props={childProps} />
              <RouteLayout path="/managegroup/:id" exact component={ManageGroup} props={childProps} />
              <RouteLayout path="/home/:status" exact component={Home} props={childProps} />
              <RouteLayout path="/page1" exact component={Page1} props={childProps} />
              <RouteLayout path="/createnews" exact component={CreateNews} props={childProps} />
              <RouteLayout path="/editnews/:id" exact component={EditNews} props={childProps} />
              <RouteLayout path="/groups" exact component={GroupList} props={childProps} />
              <RouteLayout path="/create-group" exact component={CreateGroup} props={childProps} />
              <RouteLayout path="/groupdetail/:id" exact component={GroupDetail} props={childProps} />
              <Route component={PageNotFound} />
            </Switch> */}

          </div>
          <Footer/>
        </Router>
      </HashRouter>
    );
  }
}


export default App;
