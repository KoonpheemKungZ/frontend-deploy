import React from 'react';
import Header from "../component/Header";
import { withRouter } from 'react-router-dom';

const HomeLayout = ({ children, ...rest }) => {
    return (
        <div>
            <Header />
            <div className="container-fluid">
                {/* <div className="row"> */}
                    {/*<nav className="col-md-2 d-none d-md-block bg-light sidebar">
                         <div className="sidebar-sticky-top">
                            <ul className="nav flex-column mb-2">
                                <li className="nav-item">
                                    <NavLink  activeclassname="active" className="nav-link" to="/home/All"><img className="nav-link-icon mb-1" src={iconnews} alt="" />All News<span className="sr-only">(current)</span></NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink  activeclassname="active" className="nav-link" to="/home/Publish"><img className="nav-link-icon mb-1" src={iconnews} alt="" />Publish</NavLink>
                                </li>
                                <li className="nav-item">
                                    <NavLink activeclassname="active" className="nav-link" to="/home/Unpublish"><img className="nav-link-icon mb-1" src={iconnews} alt="" />Unpublish</NavLink>
                                </li>
                            </ul>
                            <ul className="logout_sidebar_button navbar-nav mr-auto">
                                <li className="nav-item">
                                <Link className="nav-link" to="#" onClick={e => ( Swal.fire({
                                    title: 'Are you sure?',
                                    text: "You won't be sign out?",
                                    type: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'OK'
                                }).then((result) => {
                                    if (result.value) {
                                        auth.signOut()
                                        .then(response=>{
                                            children.props.userHasAuthenticated(false);
                                            // Swal.fire(
                                            //     'Sigh Out',
                                            //     'Your sign out success',
                                            //     'success',
                                            // )
                                            children.props.history.push("/login");
                                        })
                                    }
                                }))} style={{ color: "494E52", fontFamily: "Arial", fontSize: 15, marginLeft: 16 }}>Logout
                                <img src={imageLogout} alt="" style={{ marginLeft: 11 }} /></Link>
                            </li>                               
                            </ul>
                            

                        </div>
                    </nav> */}
                    {/* <nav className="navbar fixed-bottom navbar-light bg-light">
                        <ul className="navbar-nav mr-auto">
                            
                        </ul>
                    </nav> */}
                    {children}
                {/* </div > */}
            </div >
            {/* <Footer /> */}
        </div>
    )
}

export default withRouter(HomeLayout);