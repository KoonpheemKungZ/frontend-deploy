/* eslint-disable import/no-anonymous-default-export */
import React from 'react';
import { Route } from 'react-router-dom'
import HomeLayout from './HomeLayout'
// import Login from '../component/Login';
import { Cookies } from 'react-cookie'

const cookies = new Cookies()

export default ({ component: C, props: cProps, ...rest }) =>
  <Route
  {...rest} render={props =>
    <HomeLayout><C {...props} {...cProps} /></HomeLayout>
    //     ((cookies.get('user') === undefined || cookies.get('user') === null || cookies.get('user') === '') ? (window.location = process.env.REACT_APP_AUTH_URL) : <HomeLayout><C {...props} {...cProps} /></HomeLayout>)
  }
  />;

//((cookies.get('user') === undefined || cookies.get('user') === null || cookies.get('user') === '') ? (window.location = process.env.REACT_APP_AUTH_URL) : <C {...props} {...cProps} />)

