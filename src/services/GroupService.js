import { API_URL, REACT_APP_API_URL_V3 } from "../utils/constants"
import Axios from "axios"
import { Cookies } from "react-cookie"

export const GroupService = {
    highlight,
    requestAPI,
    getGroups,
    createGroup,
    getGroupDetail,
    addAppToGroup,
    removeAppToGroup,
    uploadImage,
    MemberSearch,
    getOverviewgroup
}
export const requestBeverestlife = (data, path) => {
    const cookies = new Cookies()
    const token = cookies.get('token')

    const tmp = cookies.get('tmp')
    const headers = {
        'Content-Type': 'application/json',
        'Authorization': token,
        'tmp': tmp
    }
    return new Promise((resolve, reject) => {
        Axios({
            method: 'POST',
            url: `${REACT_APP_API_URL_V3}${path}`,
            headers: headers,
            // url:'https://beverestlifeapiuat.thaibevapp.com/work/v3/portal-management',
            data: data
        }).then(function (response) {
            // console.log(response)
            if (response.status === 200) {
                resolve(response)
            } else if (response.status === 401) {
                authSign().then(res => {
                    if (res.data.token !== undefined) {
                        const cookies = new Cookies()
                        cookies.set('token', res.data.token)
                        cookies.set('tmp', res.data.tmp)
                        requestBeverestlife(data, path)
                    }})
                // reject(response)
            } else {
                return reject(Error(response.data.msg))
            }
        }).catch(function (error) {
            if (error.response.status === 401) {
                authSign().then(res => {
                    if (res.data.token !== undefined) {
                        const cookies = new Cookies()
                        cookies.set('token', res.data.token)
                        cookies.set('tmp', res.data.tmp)
                        requestBeverestlife(data, path)
                    }})
                // reject(response)
            }else{
                reject(error)
                console.log(error.message)
                console.log(error.response)
            }
        })
    })
}
export const authSign = () => {
    const data = {
        "work": "AUTH_SIGN",
        "app_key": "ElbGQNRz20Sgqf0RkHoo1A8htVTZh6shrIwDlbYz8HqJ5fYJLavPagRNaIkkASR6vnoQBQd42M6x3okp"
    }
    // /portal-management-v2/drawer-group
    // return Axios({
    //     method: 'POST',
    //     url: `${API_URL}`,
    //     data: data
    // }).then(response => {
    //     return response
    // }).catch(error => {
    //     return error.response
    // })
    return requestBeverestlife(data, '/portal-management-v2').then(res => {
        return res
    }).catch(err => {
        return err
    })
}
async function highlight(data) {
    return Axios({
        method: 'POST',
        url: `${API_URL}`,
        data: data
    }).then(response => {
        return response
    }).catch(error => {
        return error.response
    })
}

function requestAPI(data) {
    return Axios({
        method: 'POST',
        url: `${API_URL}`,
        data: data
    }).then(response => {
        return response
    }).catch(error => {
        return error.response
    })
}
function getOverviewgroup(data) {
    return requestBeverestlife(data, '/portal-management-v2/drawer-group').then(res => {
        return res
    }).catch(err => {
        return err
    })
}
function getGroups(data) {
    // const data ={
    //     "work": "AUTH.SIGN",
    //     "rkey": "628dfa89836857402b02b50a",
    //     "issuer": "beverestlife-uat",
    //     "payload": {
    //         "org_const:key": "639f1e7e26c08956bbe67cf4",
    //         "org:key":"636689479c45db02c447866e"
    //     }
    // }

    return requestBeverestlife(data, '/portal-management-v2/drawer-group').then(res => {
        return res
    }).catch(err => {
        return err
    })


    // console.log(REACT_APP_API_URL_V3)
    // return Axios({
    //     method: 'POST',
    //     url: `${API_URL}`,
    //     data: data
    // }).then(response => {
    //     console.log(response)
    //     return response
    // }).catch(error => {
    //     console.log(error.response)
    //     return error.response
    // })
}

function getGroupDetail(data) {
    return requestBeverestlife(data, '/portal-management-v2/drawer-group').then(res => {
        return res
    }).catch(err => {
        return err.response
    })
    // return Axios({
    //     method: 'POST',
    //     url: `${API_URL}`,
    //     data: data
    // }).then(response => {
    //     return response
    // }).catch(error => {
    //     return error.response
    // })
}

function createGroup(data) {
    return requestBeverestlife(data, '/portal-management-v2/drawer-group').then(res => {
        return res
    }).catch(err => {
        return err.response
    })
    // return Axios({
    //     method: 'POST',
    //     url: `${API_URL}`,
    //     data: data
    // }).then(response => {
    //     return response
    // }).catch(error => {
    //     return error.response
    // })
}
function MemberSearch(data) {
    console.log(data)
    return requestBeverestlife(data, '/portal-management-v2/drawer-group').then(res => {
        return res
    }).catch(err => {
        return err.response
    })
    // return Axios({
    //     method: 'POST',
    //     url: `${API_URL}`,
    //     data: data
    // }).then(response => {
    //     return response
    // }).catch(error => {
    //     return error.response
    // })
}

function addAppToGroup(data) {
    return Axios({
        method: 'POST',
        url: `${API_URL}`,
        data: data
    }).then(response => {
        return response
    }).catch(error => {
        return error.response
    })
}

function removeAppToGroup(data) {
    return Axios({
        method: 'POST',
        url: `${API_URL}`,
        data: data
    }).then(response => {
        return response
    }).catch(error => {
        return error.response
    })
}

function uploadImage(data) {
    try {
        let apiServer = process.env.REACT_APP_API_URL_UPLOAD;
        const res = Axios.post(`${apiServer}`, data, {
            headers: {
                'Content-Type': 'application/json'
            }
        })
        return res
    } catch (err) {
        console.log(err)
    }
}