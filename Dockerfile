# 1st Stage: Build แอปพลิเคชัน
FROM node:latest AS builder
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json .
COPY yarn.lock .
RUN yarn install --ignore-platform
COPY . .
RUN yarn build

# 2nd Stage: Serve แอปพลิเคชันโดยใช้ Express.js
FROM node:latest
WORKDIR /usr/src/app
COPY --from=builder /usr/src/app/build/ ./build
COPY server.js .
RUN npm install express
EXPOSE 3000
CMD ["node", "server.js"]
