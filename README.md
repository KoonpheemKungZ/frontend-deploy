# ThaiBev Git Control

First should be add remote tb to ThaiBev Repo.
```
git remote add tb https://gitlab.thaibevapp.com/think-wfh/docker-images/admin-tool.git
```
If want to promote to UAT
```
sh deploy_uat.sh
```

If want to promote to PRD
```
sh deploy_prd.sh
```