#!/bin/bash

GIT_COMMIT=master-$(git rev-parse --short=8 HEAD)
DOCKER_IMAGE=registryii.thaibevapp.com/bevlife/admin-tool:$GIT_COMMIT


git push tb --force develop:master

echo $GIT_COMMIT

# remove from local
docker rmi $DOCKER_IMAGE

## deploy