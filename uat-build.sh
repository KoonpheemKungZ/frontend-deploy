#!/bin/bash
# first run this command plase create remote tb
# git remote add tb https://gitlab.thaibevapp.com/think-wfh/beverest-life-auten.git

GIT_COMMIT=master-$(git rev-parse --short=8 HEAD)
#create version file
find public/ -type f -name 'master-*.txt' -delete
touch public/$GIT_COMMIT.txt

#build dist
npm run build:uat

DOCKER_IMAGE=registryii.thaibevapp.com/bevlife/admin-tool:$GIT_COMMIT

echo $DOCKER_IMAGE

docker image build -t $DOCKER_IMAGE --platform=linux/amd64 -f Dockerfile .

docker push $DOCKER_IMAGE;

