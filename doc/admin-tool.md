# Admin-tool

## Introduction
* This project is to build a Fronend with React 

## Requirements
* Nodejs (v21.4.0)
* npm (10.2.5) / yarn (1.22.21)

## Installation the Nodejs
* ### Macos
```
brew install node
```
```
brew install npm
```
```
brew install yarn
```

## Installation the Admin-tool Services
* ### Get package Node
> ---
>```
> yarn install 
>```
> * if cant work
>```
> npm i --force
>```
> ---
* ### Build optimize
```
yarn build
```

## Installation the Docker Admin-tool Container
* ### Build Docker image
```
docker build -t admin-portal .
```

## Run the Admin-tool Services / Docker Container
* ### Run Local
```
yarn start
```
* ### Run Docker Container
> ---
>```
>docker run -p 3004:80 -d --name admin-tool admin-tool
>```
>> ### Keyword Commands
>> * `--name` container name
>> * `second admin-tool` image name
>> * `-p` port {host}:{docker network protocol}
> ---
